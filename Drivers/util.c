/*
 * adc_internal.c
 *
 *  Created on: July 2, 2020
 *      Author: Nathan
 */

#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/cfg/global.h>
#include <ti/sysbios/BIOS.h>

#include <stdbool.h>
#include <stdint.h>

#include "util.h"
#include "globals.h"
#include "adc_internal.h"
#include "adc.h"
#include "fuse.h"
#include "crc.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/timer.h"
#include "driverlib/eeprom.h"

uint8_t eeprom_crc_error_count;
uint8_t eeprom_write_error_count;
uint8_t eeprom_read_error_count;


void util_init_leds_rgb() {
    // Setup LED pins
    GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |
                                           GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);
    util_led_rgb_1(LED_OFF);
    util_led_rgb_2(LED_OFF);
}

void util_led_rgb_1(uint8_t led_set) {
    GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2, led_set);
}

void util_led_rgb_2(uint8_t led_set) {
    GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5, led_set<<3);
}

void util_rgb_led_1_eflt_isr() {
    ROM_TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

    led_rgb_1_state = GPIOPinRead(GPIO_PORTL_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2);

    // if(LED is off) {turn on}, else{turn off}
    if(led_rgb_1_state == 7) {
        util_led_rgb_1(LED_RED);
    } else {
        util_led_rgb_1(LED_OFF);
    }
}

void util_rgb_led_2_eflt_isr() {
    ROM_TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

    led_rgb_2_state = GPIOPinRead(GPIO_PORTL_BASE, GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);

    // if(LED is off) {turn on}, else{turn off}
    if(led_rgb_2_state == 7 << 3) {
        util_led_rgb_2(LED_RED);
    } else {
        util_led_rgb_2(LED_OFF);
    }
}

void util_init_mp_pwr() {
    GPIOPinTypeGPIOOutput(GPIO_PORTQ_BASE, GPIO_PIN_2 | GPIO_PIN_3);

    /**********************************************************/
    /******* RGB 1 Corresponds to MP 2 and Vice Versa *********/
    /**********************************************************/

    util_mp_pwr_ch1_enable();
    //util_led_rgb_2(LED_YEL);

    // Delay to help with inrush characteristics
    // This method of creating a delay is SAD, should probably fix when there isn't a time-crunch
    int i;
    for(i=0; i<200000; i++);

    util_mp_pwr_ch2_enable(); // Channel 2 is powered on, but the eFuse will default to OFF
    //util_led_rgb_1(LED_RED);
    //mp_channel_2_is_off = true;
}

void util_mp_pwr_ch1_enable() {
    GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_2, 0);
}

void util_mp_pwr_ch1_disable() {
    GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_2, 1<<2);
}

void util_mp_pwr_ch2_enable() {
    GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_3, 0);
}

void util_mp_pwr_ch2_disable() {
    GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_3, 1<<3);
}

void util_mp_rx_status_update_isr(void) {
    ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    adc_internal_24vsns_update();

    // These checks run only at startup events
    if(vsns_24v_init) {
        vsns_24v_init = false;
        vsns_24v_prev = vsns_24v;

        if(vsns_24v < VIN_THRESH) {
            //util_mp_pwr_ch2_disable();
            fuse_ch2_disable();
            util_led_rgb_1(LED_RED);
            mp_channel_2_is_off = true;
        } else if (vsns_24v > VIN_THRESH) {
            //util_mp_pwr_ch2_enable();
            fuse_ch2_enable();
            util_led_rgb_1(LED_BLU);
            mp_channel_2_is_off = false;
        }
    }

    if(adc1_ma_data_init) {
        adc1_ma_data_init = false;
        previous_adc1_data_ma = current_adc1_data_ma;
        if(current_adc1_data_ma < MA_LOOP_THRESH) {
            util_led_rgb_2(LED_BLU);
        } else {
            util_led_rgb_2(LED_GRN);
        }
    }

    if(adc2_ma_data_init && !mp_channel_2_is_off) {
        adc2_ma_data_init = false;
        previous_adc2_data_ma = current_adc2_data_ma;
        if(current_adc2_data_ma < MA_LOOP_THRESH) {
            util_led_rgb_1(LED_BLU);
        } else {
            util_led_rgb_1(LED_GRN);
        }
    }

    // Check Loop 1 Current and Update LED if Necessary
    if(current_adc1_data_ma < MA_LOOP_THRESH) {
        if(previous_adc1_data_ma > MA_LOOP_THRESH) {
            util_led_rgb_2(LED_BLU);
        }
    } else {
        if(previous_adc1_data_ma < MA_LOOP_THRESH) {
            util_led_rgb_2(LED_GRN);
        }
    }
    previous_adc1_data_ma = current_adc1_data_ma;

    // Check Loop 2 Current and Update LED if Necessary
    if(current_adc2_data_ma < MA_LOOP_THRESH && !mp_channel_2_is_off) {
        if(previous_adc2_data_ma > MA_LOOP_THRESH) {
            util_led_rgb_1(LED_BLU);
        }
    } else if(!mp_channel_2_is_off) {
        if(previous_adc2_data_ma < MA_LOOP_THRESH) {
            util_led_rgb_1(LED_GRN);
        }
    }
    previous_adc2_data_ma = current_adc2_data_ma;

    // Check for Presence of External Power Adapter, Turn-off Channel Two and Update LED if Necessary
    if(vsns_24v < VIN_THRESH) {
        // Only need to update stuff in transitions from acceptable voltage to under-voltage
        if(vsns_24v_prev > VIN_THRESH) {

            //util_mp_pwr_ch2_disable();
            fuse_ch2_disable();
            if(led_rgb_1_is_blinking) {
                //Timer_stop(util_rgb_led_1_eflt);
                util_led_rgb_1(LED_BLU);
                led_rgb_1_is_blinking = false;
                adc2_ma_data_init = true;
            }
            util_led_rgb_1(LED_RED);
            mp_channel_2_is_off = true;
        }
        vsns_24v_prev = vsns_24v;
    } else if (vsns_24v > VIN_THRESH) {
        // Only need to update stuff in transitions from undervoltage to acceptable voltage
        if(vsns_24v_prev < VIN_THRESH) {

            // Reinitialize Channel 2 Parameters
            //util_mp_pwr_ch2_enable();
            fuse_ch2_enable();
            util_led_rgb_1(LED_BLU);
            mp_channel_2_is_off = false;
            adc2_ma_data_init = true;
            //ADCInit();
        }
    }
    vsns_24v_prev = vsns_24v;
}

void util_init_eeprom() {
    // Enable EEPROM module

    SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);

    // Wait for EEPROM module to be ready
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_EEPROM0)) {};

    //Wait for EEPROM initialization to complete
    uint32_t ui32EEPROMInit;
    ui32EEPROMInit = EEPROMInit();
    if(ui32EEPROMInit != EEPROM_INIT_OK) {
        while(1) {};
    }

}

bool util_eeprom_write_block(uint8_t block_num, char *block_data)
{
    if (block_num < EEPROM_BLOCK_COUNT)
    {
        int ii;
        char block_buffer[EEPROM_BYTES_PER_BLOCK + 4]; // Each block has 2 bytes for 16-bit CRC data.
        int eeprom_ret;

        crc16_t calculated_crc = crc16_usb((const uint8_t *) block_data, EEPROM_BYTES_PER_BLOCK);

        for (ii = 0; ii < EEPROM_BYTES_PER_BLOCK; ii++)
        {
            block_buffer[ii] = block_data[ii];
        }

        block_buffer[EEPROM_BYTES_PER_BLOCK] = (calculated_crc >> 8);
        block_buffer[EEPROM_BYTES_PER_BLOCK + 1] = calculated_crc & 0xFF;

        uint32_t block_address = (block_num * (EEPROM_BYTES_PER_BLOCK + 4)); // Each block has 2 bytes for 16-bit CRC data.

        eeprom_ret = EEPROMProgram((uint32_t*) block_buffer, block_address, (EEPROM_BYTES_PER_BLOCK + 4));

        if (eeprom_ret == 0)
        {
            return true;
        }
        else
        {
            eeprom_write_error_count++;
            return false;
        }
    }
    else
    {
        eeprom_write_error_count++;
        return false;
    }
}

bool util_eeprom_read_block(uint8_t block_num, char *block_data)
{
    if (block_num < EEPROM_BLOCK_COUNT)
    {
        int ii;
        char block_buffer[EEPROM_BYTES_PER_BLOCK + 4];
        crc16_t retrieved_crc;
        crc16_t calculated_crc;

        uint32_t block_address = (block_num * (EEPROM_BYTES_PER_BLOCK + 4)); // Each block has 2 bytes for 16-bit CRC data.
        EEPROMRead((uint32_t*) block_buffer, block_address, (EEPROM_BYTES_PER_BLOCK + 4));

        retrieved_crc = (block_buffer[EEPROM_BYTES_PER_BLOCK] << 8) | (block_buffer[EEPROM_BYTES_PER_BLOCK + 1]);
        calculated_crc = crc16_usb((const uint8_t *) block_buffer, EEPROM_BYTES_PER_BLOCK);

        if (retrieved_crc == calculated_crc)
        {
            for (ii = 0; ii < EEPROM_BYTES_PER_BLOCK; ii++)
            {
                block_data[ii] = block_buffer[ii];
            }
            return true;
        }
        else
        {
            eeprom_read_error_count++;
            eeprom_crc_error_count++;
            return false;
        }
    }
    else
    {
        eeprom_read_error_count++;
        return false;
    }
}
