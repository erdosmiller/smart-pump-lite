
///
/// Erdos Miller CONFIDENTIAL
/// Unpublished Copyright (c) 2015 Erdos Miller, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Erdos Miller. The intellectual and technical concepts contained
/// herein are proprietary to Erdos Miller and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Erdos Miller.  Access to the source code contained herein is hereby forbidden to anyone except current Erdos Miller employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of Erdos Miller.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

/******************************************
 *******************************************
 **  BEGIN ERDOS MILLER'S BACKGROUND IP  ***
 *******************************************
 ******************************************/

#include "emlib.h"

#include <string.h>
#include <math.h>

Uint16 crc_error_inject = 0;

Uint16 starts_with(const char *str, const char *pre)
{
    Uint16 lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}

Uint16 crc_16_update(Uint16 crc, Uint16 data) {
    Uint16 ii;
    data &= 0xFFFF;
    crc ^= data;
    for(ii = 0; ii < 16; ++ii) {
        if(crc & 1) {
            crc = (crc >> 1) ^ 0xA001;
        } else {
            crc = (crc >> 1);
        }
    }
    if (crc_error_inject)
    {
        crc = crc + crc_error_inject;
        crc_error_inject = (crc_error_inject)?crc_error_inject--:0;
    }
    return crc;
}
float32 apply_temperature_cal(em_calibration_t *cal_struct, Uint16 matrix, Uint16 idx, Uint16 idy, float32 temp)
{
    // Xxx = @ + bt + gt^2 + dt^3
    switch(matrix)
    {
        case THREE_X_THREE:
            return cal_struct->cal[0][idx][idy]   + cal_struct->cal[1][idx][idy]*temp + cal_struct->cal[2][idx][idy]*temp*temp + cal_struct->cal[3][idx][idy]*temp*temp*temp;
        case ONE_X_THREE:
            return cal_struct->xyz_offset[0][idx] + cal_struct->xyz_offset[1][idx]*temp + cal_struct->xyz_offset[2][idx]*temp*temp + cal_struct->xyz_offset[3][idx]*temp*temp*temp;
        case DIAGONAL_MX:
            return cal_struct->diag[0][idx]       + cal_struct->diag[1][idx]*temp + cal_struct->diag[2][idx]*temp*temp + cal_struct->diag[3][idx]*temp*temp*temp;
        default:
            return -1;
    }
}

void em_lib_reverse_string(char* str, int len)
{
    int i = 0;
    int j = len-1;
    char temp = '\0';
    while (i < j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}

int em_lib_ltoa_space(long n, char* buf, int width, int forced_neg)
{
    int pos = 0;
    int is_neg = forced_neg;

    if (n<0)
    {
        n = -n;
        is_neg = true;
    }

    if (n == 0)
        buf[pos++] = '0';
    else
    {
        while (n > 0)
        {
            buf[pos++] = n % 10 + '0';
            n /= 10;
        }
    }
    if (is_neg)
        buf[pos++] = '-';

    while (pos < width)
        buf[pos++] = ' ';
    em_lib_reverse_string(buf, pos);

    buf[pos] = '\0';
    return pos;
}

int em_lib_ltoa_space_with_forced_sign(long n, char* buf, int width, int forced_neg)
{
    int pos = 0;
    int is_neg = forced_neg;

    if (n<0)
    {
        n = -n;
        is_neg = true;
    }

    if (n == 0)
        buf[pos++] = '0';
    else
    {
        while (n > 0)
        {
            buf[pos++] = n % 10 + '0';
            n /= 10;
        }
    }
    if (is_neg)
        buf[pos++] = '-';
    else
        buf[pos++] = '+';

    while (pos < width)
        buf[pos++] = ' ';
    em_lib_reverse_string(buf, pos);

    buf[pos] = '\0';
    return pos;
}

int em_lib_ultoa_space(unsigned long n, char* buf, int width)
{
    int pos = 0;

    if (n == 0)
        buf[pos++] = '0';
    else
    {
        while (n > 0)
        {
            buf[pos++] = n % 10 + '0';
            n /= 10;
        }
    }

    while (pos < width)
        buf[pos++] = ' ';
    em_lib_reverse_string(buf, pos);

    buf[pos] = '\0';
    return pos;
}

int em_lib_lltoa_space(long long n, char* buf, int width, int forced_neg)
{
    int pos = 0;
    int is_neg = forced_neg;

    if (n<0)
    {
        n = -n;
        is_neg = true;
    }

    if (n == 0)
        buf[pos++] = '0';
    else
    {
        while (n > 0)
        {
            buf[pos++] = n % 10 + '0';
            n /= 10;
        }
    }
    if (is_neg)
        buf[pos++] = '-';

    while (pos < width)
        buf[pos++] = ' ';
    em_lib_reverse_string(buf, pos);

    buf[pos] = '\0';
    return pos;
}

int em_lib_lltoa_space_with_forced_sign(long long n, char* buf, int width, int forced_neg)
{
    int pos = 0;
    int is_neg = forced_neg;

    if (n<0)
    {
        n = -n;
        is_neg = true;
    }

    if (n == 0)
        buf[pos++] = '0';
    else
    {
        while (n > 0)
        {
            buf[pos++] = n % 10 + '0';
            n /= 10;
        }
    }
    if (is_neg)
        buf[pos++] = '-';
    else
        buf[pos++] = '+';

    while (pos < width)
        buf[pos++] = ' ';
    em_lib_reverse_string(buf, pos);

    buf[pos] = '\0';
    return pos;
}

int em_lib_ulltoa_space(unsigned long long n, char* buf, int width)
{
    int pos = 0;

    if (n == 0)
        buf[pos++] = '0';
    else
    {
        while (n > 0)
        {
            buf[pos++] = n % 10 + '0';
            n /= 10;
        }
    }

    while (pos < width)
        buf[pos++] = ' ';
    em_lib_reverse_string(buf, pos);

    buf[pos] = '\0';
    return pos;
}

int em_lib_ftoa_space_digit(float f, char* buf, int before_point_space, int after_point_digits)
{
    int pos = 0;
    long int_part = 0;
    int is_neg = false;

    if (f > 1E+20)
        return -1;
    if (f < -1E+20)
        return -1;

    if (f<0)
    {
        f = -f;
        is_neg = true;
    }

    if(after_point_digits > 0)
    {
        float adj45 = 0.5;
        int ii = 0;
        for(ii = 0; ii < after_point_digits; ii++)
            adj45 /= 10;
        f += adj45;
    }

    int_part = (long)(f);
    f -= int_part;

    pos = em_lib_ltoa_space(int_part, buf, before_point_space, is_neg);

    int dig = 0;
    buf[pos++] = '.';
    while (after_point_digits > 0)
    {
        dig = (int)(f * 10);
        f = f * 10 - dig;
        if (dig < 0) dig = 0;
        if (dig > 9) dig = 9;
        buf[pos++] = dig + '0';
        after_point_digits--;
    }

    buf[pos] = '\0';
    return pos;
}

int em_lib_ftoa_space_digit_with_forced_sign(float f, char* buf, int before_point_space, int after_point_digits)
{
    int pos = 0;
    long int_part = 0;
    int is_neg = false;

    if (f > 1E+20)
        return -1;
    if (f < -1E+20)
        return -1;

    if (f<0)
    {
        f = -f;
        is_neg = true;
    }

    if(after_point_digits > 0)
    {
        float adj45 = 0.5;
        int ii = 0;
        for(ii = 0; ii < after_point_digits; ii++)
            adj45 /= 10;
        f += adj45;
    }

    int_part = (long)(f);
    f -= int_part;

    pos = em_lib_ltoa_space_with_forced_sign(int_part, buf, before_point_space, is_neg);

    int dig = 0;
    buf[pos++] = '.';
    while (after_point_digits > 0)
    {
        dig = (int)(f * 10);
        f = f * 10 - dig;
        if (dig < 0) dig = 0;
        if (dig > 9) dig = 9;
        buf[pos++] = dig + '0';
        after_point_digits--;
    }

    buf[pos] = '\0';
    return pos;
}

int em_lib_ftoa_digit(float f, char* buf, int significant_digits)
{
    int pos = 0;
    int space_left = significant_digits;
    long int_part = 0;
    int is_neg = false;

    if (f > 1E+8 || f < -1E+8)
    {
        buf[0] = '\0';
        return 0;
    }

    if (f<0)
    {
        buf[pos++] = '-';
        f = -f;
        is_neg = true;
    }

    int_part = (long)(f);
    f -= int_part;

    if (int_part == 0)
        buf[pos++] = '0';
    else
    {
        while (int_part > 0)
        {
            buf[pos++] = int_part % 10 + '0';
            int_part /= 10;
        }

        if (is_neg)
            em_lib_reverse_string(buf + 1, pos - 1);
        else
            em_lib_reverse_string(buf, pos);
        space_left -= pos;
    }
    buf[pos] = '\0';

    int dig = 0;
    if (f != 0)
    {
        buf[pos++] = '.';
        while (space_left > 0)
        {
            dig = (int)(f * 10);
            f = f * 10 - dig;
            if (dig < 0) dig = 0;
            if (dig > 9) dig = 9;
            buf[pos++] = dig + '0';
            if((space_left != significant_digits)||(dig!=0))
                space_left--;
            if(pos > significant_digits + 4)
                break;
        }
    }

    buf[pos] = '\0';
    return pos;
}

int em_lib_etoa_space_digit_digit(float f, char* buf, int before_point_space, int after_point_digits, int ext_digits)
{
    long double df = f;
    int ext = 0;

    int pos = 0;
    int space_left = after_point_digits;

    while (pos < before_point_space - 2)
        buf[pos++] = ' ';

    if (df<0)
    {
        buf[pos++] = '-';
        df = -df;
    }
    else
        buf[pos++] = ' ';

    if(df == 0)
        ext = 0;
    else if(df < 1E-37)
        ext = 0;
    else if (df >= 1)
    {
        while (df >= 10)
        {
            df /= 10.0f;
            ext++;
        }
    }
    else
    {
        while (df < 1)
        {
            df *= 10.0f;
            ext--;
        }
    }

    if(after_point_digits > 0)
    {
        long double adj45 = 0.5;
        int ii = 0;
        for(ii = 0; ii < after_point_digits; ii++)
            adj45 /= 10;
        df += adj45;
        if(df >= 10)
        {
            df /= 10.0f;
            ext++;
        }
    }

    int dig = 0;

    dig = (int)(df);
    df -= (long double)dig;
    if (dig < 0) dig = 0;
    if (dig > 9) dig = 9;
    buf[pos++] = dig + '0';
    buf[pos++] = '.';

    while (space_left > 0)
    {
        dig = (int)(df * 10);
        df = df * 10 - dig;
        if (dig < 0) dig = 0;
        if (dig > 9) dig = 9;
        buf[pos++] = dig + '0';
        space_left--;
    }

    buf[pos++] = 'E';

    if (ext < 0)
    {
        buf[pos++] = '-';
        ext = -ext;
    }
    else
        buf[pos++] = '+';

    int ext_pos = pos;

    while (ext > 0)
    {
        buf[pos++] = ext % 10 + '0';
        ext /= 10;
    }

    while (pos - ext_pos < ext_digits)
        buf[pos++] = '0';

    em_lib_reverse_string(&(buf[ext_pos]), pos - ext_pos);
    buf[pos] = '\0';

    return pos;
}

int em_lib_ftoa_free_style(float f, char* buf)
{
    if (f > 1e7 || f < -1e7)
        return em_lib_etoa_space_digit_digit(f, buf, 0, 6, 2);
    if (f < 1e-4 && f > -1e-4)
        return em_lib_etoa_space_digit_digit(f, buf, 0, 6, 2);
    return em_lib_ftoa_digit(f, buf, 7);
}

Uint16 crc_table[256] =
{
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
    0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
    0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
    0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
    0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
    0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
    0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
    0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
    0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
    0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
    0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
    0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
    0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
    0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
    0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
    0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
    0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040 };

Uint16 em_lib_crc_16_update_by_byte(Uint16 crc, char data)
{
    data &= 0x00FF;
    Uint16 x = (Uint16)(crc ^ data);
    return (Uint16)((crc >> 8) ^ crc_table[x & 0x00FF]);;
}

Uint16 em_lib_crc_16_update_by_Uint16(Uint16 crc, Uint16 data)
{
    crc = em_lib_crc_16_update_by_byte(crc, (char) ((data >> 8) & 0x00FF));
    crc = em_lib_crc_16_update_by_byte(crc, (char) (data & 0x00FF));
    return crc;
}

Uint16 em_lib_crc_16_update_by_Uint32(Uint16 crc, Uint32 data)
{
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 24) & 0xFF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 16) & 0xFF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 8) & 0xFF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) (data & 0xFF));
    return crc;
}

Uint16 em_lib_crc_16_update_by_Uint64(Uint16 crc, Uint64 data)
{
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 56) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 48) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 40) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 32) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 24) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 16) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) ((data >> 8) & 0x000000FF));
    crc = em_lib_crc_16_update_by_byte(crc,  (char) (data & 0xFF));
    return crc;
}

Uint16 em_lib_crc_16_update_by_float32(Uint16 crc, float32 data)
{
    Uint16 ii;
    union FloatorUint32 value = {.fl = data};
    for (ii = 0; ii < 4; ii++)
        crc = em_lib_crc_16_update_by_byte(crc, (char) ((value.ui >> ((3-ii)*8)) & 0xFF));
    return crc;
}

Uint16 em_lib_crc_16_update_by_string(Uint16 crc, char* data)
{
    while((*data) != '\0')
        crc = em_lib_crc_16_update_by_byte(crc, *(data++));
    return crc;
}

int em_lib_strlen_in_word_buf(Uint16 * buf)
{
    int pos = 0;
    char c = 0;
    while(true)
    {
        CH_GET(buf, pos, c);
        if(c == 0)
            break;
        pos ++;
    }
    return pos;
}

void em_lib_buf_byte_to_word(Uint16 * word_buf, int start_pos, char * byte_buf, int len)
{
    int ii = 0;
    for(ii = 0; ii < len; ii++)
    { CH_PUT(word_buf, start_pos + ii, byte_buf[ii]); }
}

void em_lib_buf_word_to_word(Uint16 * dst_buf, const Uint16 dst_start_pos, Uint16 * src_buf, Uint16 src_start_pos, Uint16 len)
{
    if((dst_start_pos & 1)||(src_start_pos & 1))
    {
        char c = 0;
        int dst_pos = dst_start_pos;
        int src_pos = src_start_pos;
        int ii = 0;

        for(ii = 0; ii < len; ii++)
        {
            CH_GET(src_buf, src_pos, c);
            CH_PUT(dst_buf, dst_pos, c);
            dst_pos++;
            src_pos++;
        }
    }
    else
    {
        memcpy(dst_buf + dst_start_pos / 2, src_buf + src_start_pos / 2, (len + 1) / 2);
    }
}

int em_lib_buf_word_to_word_string(Uint16 * dst_buf, Uint16 dst_start_pos, const Uint16 * src_buf, Uint16 src_start_pos)
{
    char c = 0;
    Uint16 dst_pos = dst_start_pos;
    Uint16 src_pos = src_start_pos;
    while(true)
    {
        CH_GET(src_buf, src_pos, c);
        CH_PUT(dst_buf, dst_pos, c);
        dst_pos++;
        src_pos++;
        if(c == 0)
            break;
    }

    return(dst_pos - dst_start_pos);
}

int em_lib_buf_string_to_word(Uint16 * word_buf, int start_pos, const char * string_buf)
{
    int pos = start_pos;
    while((*string_buf)!= 0)
    {
        CH_PUT(word_buf, pos, *string_buf);
        pos ++;
        string_buf ++;
    }
    CH_PUT(word_buf, pos, 0);
    return pos - start_pos;
}

int em_lib_buf_string_to_byte(char* byte_buf, int start_pos, const char * string_buf)
{
    int pos = start_pos;
    while((*string_buf)!= 0)
    {
        byte_buf[pos] = *string_buf;
        pos ++;
        string_buf ++;
    }
    byte_buf[pos] = '\0';
    return pos - start_pos;
}

void em_lib_buf_word_to_byte(char * dst_buf, const Uint16 dst_start_pos, Uint16 * src_buf, Uint16 src_start_pos, Uint16 len)
{
    int ii = 0;
    for(ii = 0; ii < len; ii++)
    { CH_GET(src_buf, src_start_pos + ii, dst_buf[dst_start_pos + ii]); }
}

int em_lib_buf_word_to_string(char * dst_buf, Uint16 dst_start_pos, const Uint16 * src_buf, Uint16 src_start_pos)
{
    int pos = 0;
    while(true)
    {
        char c = 0;
        CH_GET(src_buf, src_start_pos + pos, c);
        dst_buf[dst_start_pos + pos] = c;
        if(c == 0)
            break;
    }

    return pos;
}

unsigned char em_lib_load_byte_from_word_buffer(unsigned int * buf, unsigned int offset)
{
    unsigned char result = 0;
    CH_GET(buf, offset, result);
    return result;
}

unsigned int em_lib_load_word_from_word_buffer(unsigned int * buf, unsigned int offset)
{
    unsigned char c0, c1;
    CH_GET(buf, offset, c0);
    CH_GET(buf, offset + 1, c1);
    return (unsigned int)(((unsigned int)(c1) << 8) | ((unsigned int)(c0)));
}

unsigned long em_lib_load_dword_from_word_buffer(unsigned int * buf, unsigned int offset)
{
    unsigned char c0, c1, c2, c3;
    CH_GET(buf, offset, c0);
    CH_GET(buf, offset + 1, c1);
    CH_GET(buf, offset + 2, c2);
    CH_GET(buf, offset + 3, c3);
    return (unsigned long)(
            ((unsigned long)(c3) << 24)
            | ((unsigned long)(c2) << 16)
            | ((unsigned long)(c1) << 8)
            | ((unsigned long)(c0)));
}

void em_lib_set_byte_to_word_buffer(unsigned int * buf, unsigned int offset, unsigned char val)
{
    CH_PUT(buf, offset, val);
}

void em_lib_set_word_to_word_buffer(unsigned int * buf, unsigned int offset, unsigned int val)
{
    CH_PUT(buf, offset, val & 0x00FF);
    CH_PUT(buf, offset + 1, (val>>8) & 0x00FF);
}

void em_lib_set_dword_to_word_buffer(unsigned int * buf, unsigned int offset, unsigned long val)
{
    CH_PUT(buf, offset, val & 0x000000FF);
    CH_PUT(buf, offset + 1, (val>>8) & 0x000000FF);
    CH_PUT(buf, offset + 2, (val>>16) & 0x000000FF);
    CH_PUT(buf, offset + 3, (val>>24) & 0x000000FF);
}

void swap32(char* buf){
    char b[4];
    b[0] = buf[3];
    b[1] = buf[2];
    b[2] = buf[1];
    b[3] = buf[0];
    memcpy(buf, b, 4);
}


/******************************************
 *******************************************
 **   END ERDOS MILLER'S BACKGROUND IP   ***
 *******************************************
 ******************************************/

