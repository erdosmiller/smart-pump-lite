/*
 * pinout.c
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#include "pinout.h"

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_usb.h"
#include "inc/hw_ints.h"
#include "inc/hw_can.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/can.h"

#include "driverlib/adc.h"

void PinoutSet(bool bEthernet, bool bUSB) {
  //
  // Enable all the GPIO peripherals.
  //
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOH);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);

  // PM0 - PM3 are hardware version pins
  ROM_GPIOPinTypeGPIOInput(GPIO_PORTM_BASE, GPIO_PIN_0 | GPIO_PIN_1 |
                                            GPIO_PIN_2 | GPIO_PIN_3);

  MAP_GPIOPadConfigSet(GPIO_PORTM_BASE, GPIO_PIN_0 | GPIO_PIN_1 |
                                        GPIO_PIN_2 | GPIO_PIN_3,
                       GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD_WPU);

  // ADC 1 and 2 nRESET
  GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_0);
  GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_1);
  GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_0, GPIO_PIN_0);
  GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_1, GPIO_PIN_1);

  // PN0 ADC 1 and 2 nDRDY
  ROM_GPIOPinTypeGPIOInput(GPIO_PORTN_BASE, GPIO_PIN_0 | GPIO_PIN_1);

  // Enable nDRDY ready signal for MP1 and MP2
  GPIOPinTypeGPIOInput(GPIO_PORTN_BASE, GPIO_PIN_0);
  GPIOPinTypeGPIOInput(GPIO_PORTN_BASE, GPIO_PIN_1);
  GPIOIntTypeSet(GPIO_PORTN_BASE, GPIO_PIN_0, GPIO_FALLING_EDGE);
  GPIOIntTypeSet(GPIO_PORTN_BASE, GPIO_PIN_1, GPIO_FALLING_EDGE);
  GPIOIntEnable(GPIO_PORTN_BASE, GPIO_INT_PIN_0 | GPIO_INT_PIN_1);
  GPIOIntClear(GPIO_PORTN_BASE, GPIO_INT_PIN_0 | GPIO_INT_PIN_1);

  // Set up ADC Chipselects - ADC-nCS
  GPIOPinTypeGPIOOutput(GPIO_PORTK_BASE, GPIO_PIN_6 | GPIO_PIN_7);
  GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_6 | GPIO_PIN_7);


  // Setup LED pins
  /*GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |
                                         GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);
  GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |
                                GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5,
                                GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |
                                GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5); */

  // Setup ADC-nSYNC signal to low
  GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_0);
  GPIOPinWrite(GPIO_PORTG_BASE, GPIO_PIN_0, 0x0);


  // Turn on Power Supplies
  //GPIOPinTypeGPIOOutput(GPIO_PORTQ_BASE, GPIO_PIN_2 | GPIO_PIN_3);
  //GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_2 | GPIO_PIN_3, 0x8); // Channel 2 OFF by default

  //Set USB Up RST to LOW
  GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_7);
  GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_7, 0x0);
  //Setup USB CFG_SEL[0..1] = 0 // default config
  //CFG_SEL1
  GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_6);
  GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_6, 0x0);
  //CFG_SEL0
  GPIOPinTypeGPIOOutput(GPIO_PORTK_BASE, GPIO_PIN_4);
  GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_4, 0x0);


  //eF-nFLT1 input - fuse 1 state
  GPIOPinTypeGPIOInput(GPIO_PORTN_BASE, GPIO_PIN_2);
  GPIOIntTypeSet(GPIO_PORTN_BASE, GPIO_PIN_2, GPIO_BOTH_EDGES);
  //eF-nFLT2 input - fuse 2 state
  GPIOPinTypeGPIOInput(GPIO_PORTN_BASE, GPIO_PIN_4);
  GPIOIntTypeSet(GPIO_PORTN_BASE, GPIO_PIN_4, GPIO_BOTH_EDGES);

//  GPIOIntEnable(GPIO_PORTN_BASE, GPIO_INT_PIN_2 | GPIO_INT_PIN_4);
//  GPIOIntClear(GPIO_PORTN_BASE,  GPIO_INT_PIN_2 | GPIO_INT_PIN_4);

  //eF-nSHDN1 - fuse 1 reset
  GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_3);
  GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_3, 0x0);
  //eF-nSHDN2 - fuse 2 reset
  GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_5);
  GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_5, 0x0);


//  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
//  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)){}

  //Setup eBus 24V enable
  GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_4 | GPIO_PIN_5 |
                                         GPIO_PIN_6 | GPIO_PIN_7);
  GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_4 | GPIO_PIN_5 |
                                GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_6);

  // 3 is set to high
//  GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_6, 0x1);
  // 4 is set to low since this is the tool channel - 24V is not connected
//  GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_7, 0x0);

  //
  // PB0-1/PD6/PL6-7 are used for USB.
  // PQ4 can be used as a power fault detect on this board but it is not
  // the hardware peripheral power fault input pin.
  //
  if(bUSB)
  {
//    HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
//    HWREG(GPIO_PORTD_BASE + GPIO_O_CR) = 0xff;
//    ROM_GPIOPinConfigure(GPIO_PD6_USB0EPEN);
//    ROM_GPIOPinTypeUSBAnalog(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
//    ROM_GPIOPinTypeUSBDigital(GPIO_PORTD_BASE, 0x0);
//    ROM_GPIOPinTypeUSBDigital(GPIO_PORTD_BASE, GPIO_PIN_6);
    ROM_GPIOPinTypeUSBAnalog(GPIO_PORTL_BASE, GPIO_PIN_6 | GPIO_PIN_7);
    ROM_GPIOPinTypeGPIOInput(GPIO_PORTQ_BASE, GPIO_PIN_4);
  }

  //
  // PF0/PF4 are used for Ethernet LEDs.
  //
  if(bEthernet)
  {
    //
    // this app wants to configure for ethernet LED function.
    //
    ROM_GPIOPinConfigure(GPIO_PF0_EN0LED0);
    ROM_GPIOPinConfigure(GPIO_PF4_EN0LED1);

    GPIOPinTypeEthernetLED(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4);

  }
  else
  {
    //
    // This app does not want Ethernet LED function so configure as
    // standard outputs for LED driving.
    //
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4);

    //
    // Default the LEDs to OFF.
    //
    ROM_GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4, 0);
    MAP_GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4,
                         GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD);
  }
}

  void
LEDWrite(uint32_t ui32LEDMask, uint32_t ui32LEDValue)
{

  //
  // Check the mask and set or clear the LED as directed.
  //
  if(ui32LEDMask & CLP_D1)
  {
    if(ui32LEDValue & CLP_D1)
    {
      GPIOPinWrite(CLP_D1_PORT, CLP_D1_PIN, CLP_D1_PIN);
    }
    else
    {
      GPIOPinWrite(CLP_D1_PORT, CLP_D1_PIN, 0);
    }
  }

  if(ui32LEDMask & CLP_D2)
  {
    if(ui32LEDValue & CLP_D2)
    {
      GPIOPinWrite(CLP_D2_PORT, CLP_D2_PIN, CLP_D2_PIN);
    }
    else
    {
      GPIOPinWrite(CLP_D2_PORT, CLP_D2_PIN, 0);
    }
  }

  if(ui32LEDMask & CLP_D3)
  {
    if(ui32LEDValue & CLP_D3)
    {
      GPIOPinWrite(CLP_D3_PORT, CLP_D3_PIN, CLP_D3_PIN);
    }
    else
    {
      GPIOPinWrite(CLP_D3_PORT, CLP_D3_PIN, 0);
    }
  }

  if(ui32LEDMask & CLP_D4)
  {
    if(ui32LEDValue & CLP_D4)
    {
      GPIOPinWrite(CLP_D4_PORT, CLP_D4_PIN, CLP_D4_PIN);
    }
    else
    {
      GPIOPinWrite(CLP_D4_PORT, CLP_D4_PIN, 0);
    }
  }
}

void LEDRead(uint32_t *pui32LEDValue)
{
  *pui32LEDValue = 0;

  //
  // Read the pin state and set the variable bit if needed.
  //
  if(GPIOPinRead(CLP_D4_PORT, CLP_D4_PIN))
  {
    *pui32LEDValue |= CLP_D4;
  }

  //
  // Read the pin state and set the variable bit if needed.
  //
  if(GPIOPinRead(CLP_D3_PORT, CLP_D3_PIN))
  {
    *pui32LEDValue |= CLP_D3;
  }

  //
  // Read the pin state and set the variable bit if needed.
  //
  if(GPIOPinRead(CLP_D2_PORT, CLP_D2_PIN))
  {
    *pui32LEDValue |= CLP_D2;
  }

  //
  // Read the pin state and set the variable bit if needed.
  //
  if(GPIOPinRead(CLP_D1_PORT, CLP_D1_PIN))
  {
    *pui32LEDValue |= CLP_D1;
  }
}

