/*
 * pwm.c
 *
 *  Created on: Jul 25, 2021
 *      Author: Yifang.Yang
 */

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* TI-RTOS Header files */
#include <inc/hw_memmap.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include <driverlib/pwm.h>
#include <driverlib/gpio.h>
#include <driverlib/pin_map.h>
#include <driverlib/sysctl.h>

/* Smart Pump Header files */
#include "pwm.h"

/*
 *  =============================== PWM ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_SECTION(PWM_config, ".const:PWM_config")
#pragma DATA_SECTION(pwmTivaHWAttrs, ".const:pwmTivaHWAttrs")
#endif

#include <ti/drivers/PWM.h>
#include <ti/drivers/pwm/PWMTiva.h>

PWMTiva_Object pwmTivaObjects[M0_PWM2];

const PWMTiva_HWAttrs pwmTivaHWAttrs[M0_PWM2] = {
    {
        .baseAddr = PWM0_BASE,
        .pwmOutput = PWM_OUT_1,
        .pwmGenOpts = PWM_GEN_MODE_DOWN | PWM_GEN_MODE_DBG_RUN | PWM_TR_CNT_ZERO
    },
    {
        .baseAddr = PWM0_BASE,
        .pwmOutput = PWM_OUT_2,
        .pwmGenOpts = PWM_GEN_MODE_DOWN | PWM_GEN_MODE_DBG_RUN | PWM_TR_CNT_ZERO
    }
};

const PWM_Config PWM_config[] = {
    {
        .fxnTablePtr = &PWMTiva_fxnTable,
        .object = &pwmTivaObjects[0],
        .hwAttrs = &pwmTivaHWAttrs[0]
    },
    {
        .fxnTablePtr = &PWMTiva_fxnTable,
        .object = &pwmTivaObjects[1],
        .hwAttrs = &pwmTivaHWAttrs[1]
    },
     {NULL, NULL}
};

/*
 *  ======== PWM_M0_Init ========
 *  Sets up the Configuration structure needed initialize PWM0
 */
void PWM_M0_Init(void)
{
    /* Enable PWM peripherals */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);

    /*
     * Enable PWM output on GPIO pins.
     */
    GPIOPinConfigure(GPIO_PF1_M0PWM1);
    GPIOPinConfigure(GPIO_PF2_M0PWM2);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 );

    PWM_init();
}

void PWM_Init(void)
{
    PWM_Handle pwm1;
    PWM_Handle pwm2;
    PWM_Params params1, params2;
    uint16_t   ad7124_pwmPeriod = 20;      // Period and duty in time counts. 50 mHz sysclk to 2 mHz PWM period for AD7124 ADC
    uint16_t   ads131_pwmPeriod = 10;      // Period and duty in time counts. 50 mHz sysclk to 4 mHz PWM period for ADS131M08 ADC
    uint16_t   duty = 50;                   // Simulating clock cycles which needs 50% duty cycle only

    PWM_M0_Init();

    PWM_Params_init(&params1);
    PWM_Params_init(&params2);
    params1.period = ad7124_pwmPeriod;
    params1.dutyMode = PWM_DUTY_COUNTS;
    pwm1 = PWM_open(M0_PWM1, &params1);
    params2.period = ads131_pwmPeriod;
    params2.dutyMode = PWM_DUTY_COUNTS;
    pwm2 = PWM_open(M0_PWM2, &params2);

    PWM_setDuty(pwm1, duty);    //Sets duty cycle to 50%
    PWM_setDuty(pwm2, duty);
}
