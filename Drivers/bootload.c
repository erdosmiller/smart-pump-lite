/*
 * bootload.c
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#include "bootload.h"
#include "globals.h"
#include "usb.h"

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/usb.h"

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_ssi.h"
#include "inc/hw_timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "driverlib/interrupt.h"
#include "driverlib/pwm.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/timer.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/usb.h"
#include "usblib/usblib.h"
#include "usblib/usb-ids.h"
#include "usblib/device/usbdevice.h"

void JumpToBootLoader(){
  uint32_t ui32SysClock_DFU;

  /* disable all interrupts */
  ROM_IntMasterDisable();

  /* disable SysTick and its interrupt */
  ROM_SysTickIntDisable();
  ROM_SysTickDisable();

  /*  Disable all processor interrupts. Instead of disabling them
   *  one at a time, a direct write to NVIC is done to disable all
   *  peripheral interrupts.
   */
  HWREG(NVIC_DIS0) = 0xffffffff;
  HWREG(NVIC_DIS1) = 0xffffffff;
  HWREG(NVIC_DIS2) = 0xffffffff;
  HWREG(NVIC_DIS3) = 0xffffffff;
  HWREG(NVIC_DIS4) = 0xffffffff;

  /* run from the pll at 120 MHz */
  ui32SysClock_DFU = MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN
        | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), 120000000);

  /* enable and reset the usb peripheral */
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_USB0);
  ROM_SysCtlPeripheralReset(SYSCTL_PERIPH_USB0);
  ROM_USBClockEnable(USB0_BASE, 4, USB_CLOCK_INTERNAL);

  /* wait for about a second */
  ROM_SysCtlDelay(ui32SysClock_DFU / 3);

  /* re-enable interrupts at the NVIC level */
  ROM_IntMasterEnable();

  /* call the usb boot loader */
  ROM_UpdateUSB(0);

  /* should never get here */
  while (true);
}
