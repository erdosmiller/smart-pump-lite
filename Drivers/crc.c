/*
 * crc.c
 *
 *  Created on: September 10, 2020
 *      Author: Nathan
 */

#include <stdbool.h>
#include <stdint.h>

#include "crc.h"

void crc16_usb_init() {
    adc1_crc = 0;
    adc2_crc = 0;

    crc16_t remainder;
    int dividend;
    uint8_t bit;
    for(dividend = 0; dividend < 256; ++dividend) {
        remainder = dividend << (WIDTH - 8);

        for(bit = 8; bit > 0; --bit) {
            if(remainder & TOPBIT) {
                remainder = (remainder << 1) ^ CRC_USB_POLY;
            }
            else {
                remainder = (remainder << 1);
            }
        }
        CRC16_USB_TABLE[dividend] = remainder;
    }
}

crc16_t crc16_usb(uint8_t const message[], int nBytes) {
    uint8_t data;
    crc16_t crc = 0;

    int byte;
    for(byte = 0; byte < nBytes; ++byte) {
        data = message[byte] ^ (crc >> (WIDTH - 8));
        crc = CRC16_USB_TABLE[data] ^ (crc << 8);
    }

    return crc;
}
