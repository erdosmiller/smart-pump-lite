/*
 * globals.c
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#include "globals.h"

volatile bool g_bUSBConfigured = false;

uint32_t ui32PLLRate = 0;
//uint32_t ui32SysClock = 120000000;
uint32_t ui32SysClock = 50000000;

volatile uint32_t g_ui32Flags = 0;

char fw_message[CMD_BASE_LENGTH + 16] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_FW_VERSION >> 8, CMD_FW_VERSION,  // Command
        0x00, 0x10,                   // Length
        CMD_STX,
        FW_MAJOR, FW_MINOR, FW_PATCH, // Body
        0x00,                         // Reserved
        0x00, 0x00, 0x00, 0x00,       // Reserved
        0x00, 0x00, 0x00, 0x00,       // Reserved
        0x00, 0x00, 0x00, 0x00,       // Reserved
        CMD_FOOTER
    };

char hw_message[CMD_BASE_LENGTH + 16] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_HW_VERSION >> 8, CMD_HW_VERSION,  // Command
        0x00, 0x10,                   // Length
        CMD_STX,
        HW_MAJOR, HW_MINOR,           // Body
        0x00, 0x00,                   // Reserved
        0x00, 0x00, 0x00, 0x00,       // Reserved
        0x00, 0x00, 0x00, 0x00,       // Reserved
        0x00, 0x00, 0x00, 0x00,       // Reserved
        CMD_FOOTER
    };

char mode_message[CMD_BASE_LENGTH + 1] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_DEVICE_MODE >> 8, CMD_DEVICE_MODE, // Command
        0x00, 0x01,                    // Length
        CMD_STX,
        0x00,                          // Body
        CMD_FOOTER
    };

char adc1_header[16] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_ADC1_DATA >> 8, CMD_ADC1_DATA,                            // Command
        ADC1_DATA_BODY_LENGTH >> 8, ADC1_DATA_BODY_LENGTH,      // Length
        CMD_STX,
        0x00, 0x00, 0x00, 0x00, ADC_SAMPLES >> 8, ADC_SAMPLES // Body
    };

char adc2_header[16] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_ADC2_DATA >> 8, CMD_ADC2_DATA,                            // Command
        ADC_DATA_BODY_LENGTH >> 8, ADC_DATA_BODY_LENGTH,      // Length
        CMD_STX,
        0x00, 0x00, 0x00, 0x00, ADC_SAMPLES >> 8, ADC_SAMPLES // Body
    };

char can_xdcr1_header[16] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_CAN_XDCR1_DATA >> 8, CMD_CAN_XDCR1_DATA,          // Command
        CAN_XDCR_DATA_BODY_LENGTH >> 8,
        CAN_XDCR_DATA_BODY_LENGTH,                            // Length
        CMD_STX,
        0x00, 0x00, 0x00, 0x00, ADC_SAMPLES >> 8, ADC_SAMPLES // Body
    };

char can_xdcr2_header[16] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_CAN_XDCR2_DATA >> 8, CMD_CAN_XDCR2_DATA,          // Command
        CAN_XDCR_DATA_BODY_LENGTH >> 8,
        CAN_XDCR_DATA_BODY_LENGTH,                            // Length
        CMD_STX,
        0x00, 0x00, 0x00, 0x00, ADC_SAMPLES >> 8, ADC_SAMPLES // Body
    };

char error_flags_response_message[CMD_BASE_LENGTH + ERROR_FLAGS_RESP_BODY_LENGTH] = {
        CMD_HEADER, CMD_RESERVED,
        ERROR_FLAGS_RESP >> 8, ERROR_FLAGS_RESP,
        ERROR_FLAGS_RESP_BODY_LENGTH >> 8,
        ERROR_FLAGS_RESP_BODY_LENGTH,
        CMD_STX,

        0x00,      // adc_err_flags (uint32_t) 1 of 4 bytes
        0x00,      // adc_err_flags (uint32_t) 2 of 4 bytes
        0x00,      // adc_err_flags (uint32_t) 3 of 4 bytes
        0x00,      // adc_err_flags (uint32_t) 4 of 4 bytes

        0x00,      // adc_err_state_count (uint32_t) 1 of 4 bytes
        0x00,      // adc_err_state_count (uint32_t) 2 of 4 bytes
        0x00,      // adc_err_state_count (uint32_t) 3 of 4 bytes
        0x00,      // adc_err_state_count (uint32_t) 4 of 4 bytes

        0x00,      // adc_err_crc1_count (uint32_t) 1 of 4 bytes
        0x00,      // adc_err_crc1_count (uint32_t) 2 of 4 bytes
        0x00,      // adc_err_crc1_count (uint32_t) 3 of 4 bytes
        0x00,      // adc_err_crc1_count (uint32_t) 4 of 4 bytes

        0x00,      // adc_err_crc2_count (uint32_t) 1 of 4 bytes
        0x00,      // adc_err_crc2_count (uint32_t) 2 of 4 bytes
        0x00,      // adc_err_crc2_count (uint32_t) 3 of 4 bytes
        0x00,      // adc_err_crc2_count (uint32_t) 4 of 4 bytes

        0x00,      // adc_err_sync  (uint32_t) 1 of 4 bytes
        0x00,      // adc_err_sync  (uint32_t) 2 of 4 bytes
        0x00,      // adc_err_sync  (uint32_t) 3 of 4 bytes
        0x00,      // adc_err_sync  (uint32_t) 4 of 4 bytes

        0x00,      // _usb_err_flags (uint32_t) 1 of 4 bytes
        0x00,      // _usb_err_flags (uint32_t) 2 of 4 bytes
        0x00,      // _usb_err_flags (uint32_t) 3 of 4 bytes
        0x00,      // _usb_err_flags (uint32_t) 4 of 4 bytes

        0x00,      // eeprom_crc_error_count (uint8_t)
        0x00,      // eeprom_read_error_count (uint8_t)
        0x00,      // eeprom_write_error_count (uint8_t)

        0x00,      // Reserved
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Reserved
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Reserved
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Reserved

        CMD_FOOTER
    };

char fuse_reset_counts_message[CMD_BASE_LENGTH + 2] = {
        CMD_HEADER, CMD_RESERVED,
        FUSE_RESET_COUNTS >> 8, FUSE_RESET_COUNTS, // Command
        0x00, 0x02,
        CMD_STX,
        0x00,                                      // Channel 1 fuse reset count
        0x00,                                      // Channel 2 fuse reset count
        CMD_FOOTER
    };

char fuse_states_message[CMD_BASE_LENGTH + 2] = {
        CMD_HEADER, CMD_RESERVED,
        FUSE_STATES >> 8, FUSE_STATES, // Command
        0x00, 0x02,                    // Length
        CMD_STX,
        0x00,                          // Channel 1 fuse state
        0x00,                          // Channel 2 fuse state
        CMD_FOOTER
    };

char fuse_state_changed_message[CMD_BASE_LENGTH + 2] = {
        CMD_HEADER, CMD_RESERVED,
        FUSE_STATE_CHANGED >> 8, FUSE_STATE_CHANGED, // Command
        0x00, 0x02,                                  // Length
        CMD_STX,
        0x00,                                        // Fuse id
        0x00,                                        // Fuse state
        CMD_FOOTER
    };

char notify_24v_disconnected[CMD_BASE_LENGTH] = {
        CMD_HEADER, CMD_RESERVED,
        NOTIFY_24V_DISCONNECTED >> 8, NOTIFY_24V_DISCONNECTED, // Command
        0x00, 0x00,                                  // Length
        CMD_STX,
        CMD_FOOTER
};
char notify_24v_connected[CMD_BASE_LENGTH] = {
        CMD_HEADER, CMD_RESERVED,
        NOTIFY_24V_CONNECTED >> 8, NOTIFY_24V_CONNECTED, // Command
        0x00, 0x00,                                  // Length
        CMD_STX,
        CMD_FOOTER
};

char id_block_response[CMD_BASE_LENGTH + 120] = {
        CMD_HEADER, CMD_RESERVED,
        ID_BLOCK_RESPONSE >> 8, ID_BLOCK_RESPONSE, // Command
        0x00, 0x78,                                  // Length
        CMD_STX,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 16
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 8
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 8
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 8

        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 16
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 8
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 8

        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 16
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 16
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 16
        CMD_FOOTER
};

char eeprom_write_block_resp_message[CMD_BASE_LENGTH + EEPROM_WRITE_RESP_LENGTH] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_EEPROM_WRITE >> 8, CMD_EEPROM_WRITE,
        EEPROM_WRITE_RESP_LENGTH >> 8,
        EEPROM_WRITE_RESP_LENGTH,
        CMD_STX,
        
        0x00,  // Status (Possible Values: 0xB6: Good, 0x00: EEPROM Write Limit of Power-up Session Reached or Bad Data)

        CMD_FOOTER
    };

char eeprom_read_block_message[CMD_BASE_LENGTH + EEPROM_READ_RESP_LENGTH] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_EEPROM_READ >> 8, CMD_EEPROM_READ,
        EEPROM_BLOCK_MESSAGE_LENGTH >> 8,
        EEPROM_BLOCK_MESSAGE_LENGTH,
        CMD_STX,

        0x00,                                           // Success or Error byte

        0x00,                                           // EEPROM Block Number
        0x00,                                           // EEPROM Section Number

        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // EEPROM Block Data (32 bytes)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

        CMD_FOOTER
    };

char eeprom_save_response[CMD_BASE_LENGTH + EEPROM_SAVE_RESP_LENGTH] = {
        CMD_HEADER, CMD_RESERVED,
        CMD_EEPROM_SAVE >> 8, CMD_EEPROM_SAVE,
        EEPROM_SAVE_RESP_LENGTH >> 8,
        EEPROM_SAVE_RESP_LENGTH,
        CMD_STX,
        
        0x00,  // Status (1 = Successful, 0 = Error)
        CMD_FOOTER

    };

char footer_message[2] = {CMD_FOOTER};

uint8_t device_mode = CONFIG_MODE;

uint16_t new_adc1_data = 0;
uint16_t new_adc2_data = 0;
//int current_adc1_data[ADC_SAMPLES];
//int current_adc2_data[ADC_SAMPLES];
//int send_adc1_data[ADC_SAMPLES];
//int send_adc2_data[ADC_SAMPLES];

//uint8_t adc1_final_data[(sizeof(adc1_header) + (sizeof(send_adc1_data)-(sizeof(send_adc1_data)/4)) + 2/*crc*/ + sizeof(footer_message))];
//uint8_t adc2_final_data[(sizeof(adc1_header) + (sizeof(send_adc1_data)-(sizeof(send_adc1_data)/4)) + sizeof(footer_message))];
//uint32_t adc1_final_data_index = 0;
//uint32_t adc2_final_data_index = 0;

//AdcSendBufferItem adc1SendBuffer[ADC_SEND_BUFFER_SIZE] = {0};
//AdcSendBufferItem* adc1SendBufferHead = adc1SendBuffer;
//
//AdcSendBufferItem adc2SendBuffer[ADC_SEND_BUFFER_SIZE] = {0};
//AdcSendBufferItem* adc2SendBufferHead = adc2SendBuffer;

//uint32_t debug_adcSendCount = 0;
//uint32_t debug_adcSendREQEDIndex = 0;

uint8_t adc1_can_data[ADC1_CAN_DATA_SIZE];

//uint16_t current_adc1_index = 0;
//uint16_t current_adc2_index = 0;
//
//uint32_t adc1_block_index = 0;
//uint32_t adc2_block_index = 0;
//
//uint32_t adc1_usb_write_error = 0;
//uint32_t adc2_usb_write_error = 0;

uint16_t go_to_bootloader_flag = 0;

uint32_t set_ch_1_fuse_after_time = 0;
uint32_t set_ch_2_fuse_after_time = 0;

//CAN Transducer
uint16_t new_can_xdcr1_data = 0;
uint16_t new_can_xdcr2_data = 0;
int current_can_xdcr1_data[ADC_SAMPLES] = {0};
int current_can_xdcr2_data[ADC_SAMPLES] = {0};
int send_can_xdcr1_data[ADC_SAMPLES] = {0};
int send_can_xdcr2_data[ADC_SAMPLES] = {0};
//uint8_t can_xdcr1_final_data[(sizeof(adc1_header) +
//                              sizeof(send_adc1_data) +
//                              sizeof(footer_message))] = {0};
//uint8_t can_xdcr2_final_data[(sizeof(adc1_header) +
//                              sizeof(send_adc1_data) +
//                              sizeof(footer_message))] = {0};
uint32_t can_xdcr1_final_data_index = 0;
uint32_t can_xdcr2_final_data_index = 0;
uint16_t current_can_xdcr1_index = 0;
uint16_t current_can_xdcr2_index = 0;
uint32_t can_xdcr1_block_index = 0;
uint32_t can_xdcr2_block_index = 0;
float vsns_24v = 0.0;
float vsns_24v_prev = 0.0;
bool vsns_24v_init = true;
extern bool adc1_ma_data_init = true;
extern bool adc2_ma_data_init = true;
uint32_t adc0_raw[SS_FIFO_DEPTH] = {0};
extern bool sample_adc0 = false;
extern int32_t led_rgb_1_state = 0;
extern int32_t led_rgb_2_state = 0;
extern bool led_rgb_1_is_blinking = false;
extern bool led_rgb_2_is_blinking = false;
extern float current_adc1_data_ma = 0.0;
extern float current_adc2_data_ma = 0.0;
extern float previous_adc1_data_ma = 0.0;
extern float previous_adc2_data_ma = 0.0;
extern char fuse1_state = 0x01;
extern char fuse2_state = 0x01;
extern bool mp_channel_2_is_off = true;
extern bool startup = true;
