/*
 * can.c
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#include "can.h"
#include "ecana.h"
#include "cli.h"

tCANMsgObject sCANMessage;
uint8_t pui8MsgData[8] = {0};

tCANMsgObject sCANMessage_2;
uint8_t pui8MsgData_2[8] = {0};

volatile uint32_t g_ui32MsgCount = 0;
volatile bool g_bErrFlag = 0;

void EM_CANInit(){
  //CAN0
  GPIOPinConfigure(GPIO_PA0_CAN0RX);
  GPIOPinConfigure(GPIO_PA1_CAN0TX);
  GPIOPinTypeCAN(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

  SysCtlPeripheralEnable(SYSCTL_PERIPH_CAN0);

  CANInit(CAN0_BASE);

  CANBitRateSet(CAN0_BASE, ui32SysClock, 250000);


  CANIntEnable(CAN0_BASE, CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);
  CANEnable(CAN0_BASE);

  sCANMessage.pui8MsgData = pui8MsgData;
  sCANMessage.ui32MsgID = 0;
  sCANMessage.ui32MsgIDMask = 0;
  sCANMessage.ui32Flags = MSG_OBJ_RX_INT_ENABLE |
                          MSG_OBJ_TX_INT_ENABLE |
                          MSG_OBJ_USE_ID_FILTER;
  sCANMessage.ui32MsgLen = 8;

  CANMessageSet(CAN0_BASE, 1, &sCANMessage, MSG_OBJ_TYPE_RX);

  //CAN1
  GPIOPinConfigure(GPIO_PB0_CAN1RX);
  GPIOPinConfigure(GPIO_PB1_CAN1TX);
  GPIOPinTypeCAN(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

  SysCtlPeripheralEnable(SYSCTL_PERIPH_CAN1);

  CANInit(CAN1_BASE);

  CANBitRateSet(CAN1_BASE, ui32SysClock, 250000);


  CANIntEnable(CAN1_BASE, CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);
  CANEnable(CAN1_BASE);

  sCANMessage_2.pui8MsgData = pui8MsgData_2;
  sCANMessage_2.ui32MsgID = 0;
  sCANMessage_2.ui32MsgIDMask = 0;
  sCANMessage_2.ui32Flags = MSG_OBJ_RX_INT_ENABLE |
                            MSG_OBJ_TX_INT_ENABLE |
                            MSG_OBJ_USE_ID_FILTER;
  sCANMessage_2.ui32MsgLen = 8;

  CANMessageSet(CAN1_BASE, 1, &sCANMessage_2, MSG_OBJ_TYPE_RX);

}

void EchoFrame(){
  sCANMessage.pui8MsgData = pui8MsgData;
  CANMessageGet(CAN0_BASE, 1, &sCANMessage, 0);
  if(sCANMessage.ui32Flags & MSG_OBJ_DATA_LOST)
  {
    //MessageLoss Detected
  }

  CANMessageSet(CAN0_BASE, 1, &sCANMessage, MSG_OBJ_TYPE_TX); // Echo maybe
}

void EM_CANSend(Uint64 id, Uint16 len, Uint64 msg){
  sCANMessage.ui32MsgID = id;
  sCANMessage.ui32MsgLen = len;
  *(Uint64*)sCANMessage.pui8MsgData = msg;
  CANMessageSet(CAN0_BASE, 2, &sCANMessage, MSG_OBJ_TYPE_TX);
}

void CANIntHandler(void) {
  uint32_t ui32Status;

  //
  // Read the CAN interrupt status to find the cause of the interrupt
  //
  ui32Status = CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE);

  //
  // If the cause is a controller status interrupt, then get the status
  //
  if(ui32Status == CAN_INT_INTID_STATUS)
  {
    //
    // Read the controller status.  This will return a field of status
    // error bits that can indicate various errors.  Error processing
    // is not done in this example for simplicity.  Refer to the
    // API documentation for details about the error status bits.
    // The act of reading this status will clear the interrupt.
    //
    ui32Status = CANStatusGet(CAN0_BASE, CAN_STS_CONTROL);

    if(ui32Status != CAN_STATUS_RXOK | ui32Status != CAN_STATUS_TXOK){
      g_bErrFlag = 1;
    }
  }

  //
  // Check if the cause is message object 1, which what we are using for
  // receiving messages.
  //
  else if(ui32Status == 1)
  {
    //
    // Getting to this point means that the RX interrupt occurred on
    // message object 1, and the message reception is complete.  Clear the
    // message object interrupt.
    //
    CANIntClear(CAN0_BASE, 1);

    //
    // Increment a counter to keep track of how many messages have been
    // received.  In a real application this could be used to set flags to
    // indicate when a message is received.
    //
    g_ui32MsgCount++;

    //
    // Since a message was received, clear any error flags.
    //
    g_bErrFlag = 0;

    //
    // Set flag to indicate received message is pending.
    //
//    g_bRXFlag = 1;


    sCANMessage.pui8MsgData = pui8MsgData;
    CANMessageGet(CAN0_BASE, 1, &sCANMessage, 0);

    if(sCANMessage.ui32Flags & MSG_OBJ_DATA_LOST)
    {
      //MessageLoss Detected
    }

//    ecana_isr_101_service();

    if(sCANMessage.ui32MsgID == 0x01){
      if(sCANMessage.ui32MsgLen >= 4){
        union{
            float32 fval;
            UInt32 ival;
        } val;
        val.ival = 0;

        val.ival |= sCANMessage.pui8MsgData[3];
        val.ival <<= 8;
        val.ival |= sCANMessage.pui8MsgData[2];
        val.ival <<= 8;
        val.ival |= sCANMessage.pui8MsgData[1];
        val.ival <<= 8;
        val.ival |= sCANMessage.pui8MsgData[0];

//        float32 test = val.fval;

        current_can_xdcr1_data[current_can_xdcr1_index++] = val.ival;
        if(current_can_xdcr1_index >= ADC_SAMPLES){
            memcpy(send_can_xdcr1_data, current_can_xdcr1_data, sizeof(current_can_xdcr1_data));
            new_can_xdcr1_data = 1;
            current_can_xdcr1_index = 0;
        }

      }
    }


  }
  else if(ui32Status == 2)
  {
    //Clear send interrupts
    CANIntClear(CAN0_BASE, 2);
//    ecana_isr_tx_complete();
  }
  //
  // Otherwise, something unexpected caused the interrupt.  This should
  // never happen.
  //
  else
  {
    //
    // Spurious interrupt handling can go here.
    //
  }
}

void CANIntHandler_2(void) {
  uint32_t ui32Status;
  ui32Status = CANIntStatus(CAN1_BASE, CAN_INT_STS_CAUSE);

  if(ui32Status == CAN_INT_INTID_STATUS) {
    ui32Status = CANStatusGet(CAN1_BASE, CAN_STS_CONTROL);

    if(ui32Status != CAN_STATUS_RXOK | ui32Status != CAN_STATUS_TXOK){
//      g_bErrFlag = 1;
    }
  }

  else if(ui32Status == 1) {
    CANIntClear(CAN1_BASE, 1);
//    g_ui32MsgCount++;
//    g_bErrFlag = 0;

    sCANMessage_2.pui8MsgData = pui8MsgData_2;
    CANMessageGet(CAN1_BASE, 1, &sCANMessage_2, 0);

    if(sCANMessage_2.ui32Flags & MSG_OBJ_DATA_LOST)
    {
      //MessageLoss Detected
    }


    if(sCANMessage_2.ui32MsgID == 0x01){
      if(sCANMessage_2.ui32MsgLen >= 4){
        union{
            float32 fval;
            UInt32 ival;
        } val;
        val.ival = 0;

        val.ival |= sCANMessage_2.pui8MsgData[3];
        val.ival <<= 8;
        val.ival |= sCANMessage_2.pui8MsgData[2];
        val.ival <<= 8;
        val.ival |= sCANMessage_2.pui8MsgData[1];
        val.ival <<= 8;
        val.ival |= sCANMessage_2.pui8MsgData[0];

//        float32 test = val.fval;

        current_can_xdcr2_data[current_can_xdcr2_index++] = val.ival;
        if(current_can_xdcr2_index >= ADC_SAMPLES){
            memcpy(send_can_xdcr2_data, current_can_xdcr2_data, sizeof(current_can_xdcr2_data));
            new_can_xdcr2_data = 1;
            current_can_xdcr2_index = 0;
        }

      }
    }


  }
//  else if(ui32Status == 2) {
//    CANIntClear(CAN1_BASE, 2);
//  }
  else {
    //
    // Spurious interrupt handling can go here.
    //
  }
}

