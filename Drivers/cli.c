
///
/// Erdos Miller CONFIDENTIAL
/// Unpublished Copyright (c) 2015 Erdos Miller, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Erdos Miller. The intellectual and technical concepts contained
/// herein are proprietary to Erdos Miller and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Erdos Miller.  Access to the source code contained herein is hereby forbidden to anyone except current Erdos Miller employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of Erdos Miller.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

/******************************************
 *******************************************
 **  BEGIN ERDOS MILLER'S BACKGROUND IP  ***
 *******************************************
 ******************************************/

/** \file cli.c
 *  \ingroup Communication
 *  \ingroup CANProtocol
 *  \brief Command Line Interface
 *
 *  This is where all of the CAN commands get called when a can message is recieved
 *
 */


#include "version.h"
#include <ti/sysbios/knl/Clock.h>

#include "adc.h"
#include "cli.h"
#include <string.h>
#include "ecana.h"
#include "globals.h"


// TODO change all CAN stuff to use CH2GET/PUT
static char cli_tx_buf[CLI_TX_BUF_SIZE];
char cli_rx_buf[CLI_RX_BUF_SIZE] = {0};

Uint16 mp_can_in_use = false;
Uint16 mp_can_waiting_cli_caution = false;
static Uint32 cli_tx_buf_size = 0;
static Uint32 cli_tx_idx_f = 0;
static Uint32 cli_tx_idx_b = 0;

Uint16 cli_ordered_recieved = 0;
int cli_read_idx = 0;
Uint16 cli_dbg_cnt = 0;
int cli_rx_idx = 0;
int cli_rx_buf_size = 0;

Uint32 dbg_ticks = 0;

Cli_State cli_state;
Uint32 cli_state_start_time;
//GetUid uid_data;
cli_frame_data_t cli_frame;

Uint32 cli_bcast_delay = 10000;
Uint32 cli_bcast_last = 0;
Uint32 session_mode_start_time = 0;
Uint32 _dbg_var = 0;

Uint16 _stream_bytes_in_block_buf = 0;
Uint16 _stream_block_id = 0;
Uint16 _stream_crc = 0;

Uint16 cli_processing_cmd_flag = false;
Uint16 delay_following_frames = false;
Uint16 THIS_TOOL_TYPE = TOOL_TYPE_EM_DAQ;

const function_array_t *_cli_function_buf[TOOL_TYPES_SIZE] = {\
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        &_micropulse_functions, // 20
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        &_generic_functions
};

static inline _clean_up_for_command(cli_frame_data_t cli_frame);
static Uint16 _cli_wait_mframe_done(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following);
static Uint16 _cli_wait_mframe_stream_cmd_done(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following);

void cli_init(){
    Uint16 ii;
    cli_tx_buf_size = 0;
    cli_tx_idx_f = 0;
    cli_tx_idx_b = 0;

    cli_state_start_time = 0;
    cli_state = IDLE;

    cli_frame.serial = 0;
    cli_frame.reserved = 0;
    cli_frame.msg_id = 0;
    cli_frame.priority = 0;
    cli_frame.settings = 0;
    cli_frame.length = 0;
    cli_frame.rtr = 0;

    recv_frame_cq.size = 0;
    recv_frame_cq.tail = recv_frame_cq.head = 0;
    memset(recv_frame_cq.queue, 0, sizeof(recv_frame_cq.queue));


    ecana_init_rx_callback(1, _cli_enqueue_can);
    ecana_init_tx_callback(1, _cli_dequeue_can);


    //
    for (ii = 0; ii < CLI_RX_BUF_SIZE; ii++)
    {
        cli_rx_buf[ii] = 0;
    }
    clear_ordered_collected_buf();
}

Uint16 _cli_wait_for_ack(Uint16 frame_id)
{
    Uint16 temp;
    wait_ack_done(frame_id, 5000, &temp);
    return temp;
}

void cli_reset()
{
    memset(cli_rx_buf,0,sizeof(char)*CLI_RX_BUF_SIZE);
    cli_read_idx = 0;
    cli_rx_idx = 0;
    cli_rx_buf_size = 0;
}

void clear_ordered_collected_buf()
{
    memset(cli_rx_buf,0,cli_rx_idx);
    cli_rx_idx = 0;
    cli_rx_buf_size = 0;
    cli_ordered_recieved = 0;
    cli_read_idx = 0;
}

void cli_execute()
{
    if(ecan_error())
    {
        //cli_state = CERROR;
    }
    if(ecan_rml())
    {
        cli_state = CERROR;
    }
//    ServiceDog(); // Watchdog
    dbg_ticks = Clock_getTicks();
//    last_cli_active_timestamp = dbg_ticks;

    cli_processing_cmd_flag = false;

    if (mp_can_in_use) return;

    switch(cli_state)
    {
        case IDLE:
        {
            Uint16 to_process_cli_cmd = false;
            cli_frame_data_t temp_frame;

            if(!ecana_rcv_frame_queue_is_empty())
            {
                if (mp_can_waiting_cli_caution)
                {
                    temp_frame = recv_frame_cq.queue[recv_frame_cq.tail];
                    if (cli_could_handle_frame(&temp_frame))
                        to_process_cli_cmd = true;
                }
                else
                    to_process_cli_cmd = true;
            }

            if(to_process_cli_cmd)
            {
                cli_state_start_time = Clock_getTicks();
                ecana_rcv_frame_dequeue(&recv_frame_cq.val);
                memcpy(&cli_frame, &recv_frame_cq.val, sizeof(cli_frame));
                cli_state = GOT_CMD;
            }
            else {
                if ((Clock_getTicks() - cli_bcast_last) > cli_bcast_delay)
                {
                    cli_bcast_last = Clock_getTicks();
                    if ((cli_bcast_last - session_mode_start_time) > CLI_BROADCAST_TIMEOUT) // 1 min timeout
                    {
//                        if(session_mode) {
//                            log_pulse_disconnect_event();
//                            if(config.misc_config.device_mode == DEVICE_MODE_RGM && !ukf_disabled) ukf_disabled = 1; //fixme: in case Pulse does not set session mode back to zero when it is closed
//                        }
//                        session_mode = 0;
                    }
                    /* until broadcasts are fixed
                       if (session_mode)
                       {
                    //_cli_broadcast();
                    }
                    else
                    {
                    //_cli_broadcast_ticks();
                    }
                    */
                }
                //cli_dbg_cnt++;
            }
            break;
        }
        case GOT_CMD:
            // msg_id now
            if (_cli_function_buf[cli_frame.tool_type] != NULL)
            {
                if (*(_cli_function_buf[cli_frame.tool_type])->array[cli_frame.msg_id] != NULL)
                {
                    if ((Uint32)*(_cli_function_buf[cli_frame.tool_type])->array[cli_frame.msg_id] != 0xFFFFFFFF)
                    {
                        cli_processing_cmd_flag = true;
                        ecana_clear_send_frame_buffer();
                        (*_cli_function_buf[cli_frame.tool_type]->array[cli_frame.msg_id])(DEFAULT_TX_MBOX);
                        cli_processing_cmd_flag = false;
                    }
                }
            }
            cli_state_start_time = Clock_getTicks();
            cli_state = SEND_WAIT;
            break;
        case SEND_WAIT:
            if(_cli_tx_buf_size() == 0)
            {
                if(ecana_rcv_frame_queue_is_empty())
                {
                    wait_mbox_empty(DEFAULT_TX_MBOX, 500); // 500 ms
                    cli_frame.serial = 0;
                    cli_frame.reserved = 0;
                    cli_frame.msg_id = 0;
                    cli_frame.priority = 0;
                    cli_frame.settings = 0;
                    cli_frame.length = 0;
                    cli_frame.mframe_length = 0;
                    cli_frame.rtr = 0;
                }
                cli_state = IDLE;
            } else { // check for timeout
                Uint32 cli_cmd_timeout = Clock_getTicks() - cli_state_start_time;
                if(cli_cmd_timeout > 1000)
                {
                    cli_state = CERROR;
                }
            }
            break;
        case CERROR:
        default:
            _cli_empty_queue();
            // re-initalize CAN
            ecana_init(CANBAUD_500KBIT_CAN);
            cli_init();
            cli_dbg_cnt++;
            cli_state = IDLE;
            break;
    }
}

void _set_cli_frame(Uint16 priority, Uint16 msg_id)
{
    cli_frame.priority = priority;
    cli_frame.tool_type = THIS_TOOL_TYPE;
    cli_frame.msg_id = msg_id;
    cli_frame.reserved = 0;
//    cli_frame.serial = config.tool_info.uid_l & 0x00FF;
    cli_frame.serial = 12; // TODO need to make this uniqe or settable
    msg_id_broadcast(priority, msg_id);
}

void _cli_broadcast_ticks()
{
    _set_cli_frame(CAN_PRIORITY_BRCAST_SINGLE, GET_CLOCK_TICKS); // add serial support later
    get_clock_ticks(BROADCAST_TX_MBOX);

    // Add functions to bcast above
    cli_state_start_time = Clock_getTicks();
}

void _cli_broadcast()
{
    Uint16 ii;
    _set_cli_frame(CAN_PRIORITY_BRCAST_SINGLE, GET_CLOCK_TICKS); // add serial support later
    get_clock_ticks(BROADCAST_TX_MBOX);
    wait_mbox_empty(BROADCAST_TX_MBOX, 3000);

//    _set_cli_frame(CAN_PRIORITY_BRCAST_SINGLE, _GET_FLOW); // add serial support later
//    _get_flow(BROADCAST_TX_MBOX);
//    wait_mbox_empty(BROADCAST_TX_MBOX, 3000);
//
//    _set_cli_frame(CAN_PRIORITY_BRCAST_SINGLE, _GET_PULSE); // add serial support later
//    _get_pulse(BROADCAST_TX_MBOX);
//    wait_mbox_empty(BROADCAST_TX_MBOX, 3000);



    // Add functions to bcast above
    cli_state_start_time = Clock_getTicks();
    cli_state = IDLE;
}

/*
 * Cli Commands
 */

// NOTE: set functions don't use mbox but still declare it
//
// to avoid errors with the void* pointer array
//
// Information
//
void get_device_type(Uint16 mbox)
{
    ecana_pkg_uint32(THIS_TOOL_TYPE);
    _cli_load_and_send(cli_frame.priority, mbox);
}

void get_clock_ticks(Uint16 mbox)
{
    Uint32 clk_ticks = Clock_getTicks();
    ecana_pkg_uint32(clk_ticks);
    _cli_load_and_send(cli_frame.priority, mbox);
}

void get_device_unique_id(Uint16 mbox)
{
//    uid_data = get_uid();
//    ecana_pkg_uint64(uid_data.uid_val);
    ecana_pkg_uint64(32);
    _cli_load_and_send(cli_frame.priority, mbox);
}

void get_device_firmware_version(Uint16 mbox)
{
    ecana_pkg_char(FW_MAJOR);
    ecana_pkg_char(FW_MINOR);
    ecana_pkg_char(FW_PATCH);
//    ecana_pkg_uint32(get_fw_version_debug());
    ecana_pkg_uint32(0);
    _cli_load_and_send(cli_frame.priority, mbox);
}

void get_device_hardware_version(Uint16 mbox)
{
    ecana_pkg_char(HW_MAJOR);
    ecana_pkg_char(HW_MINOR);
    _cli_load_and_send(cli_frame.priority, mbox);
}

void set_device_mode(Uint16 mbox){
    char mode = _cli_deserialize_byte();

    device_mode = mode;
    switch(device_mode){
    case ADC_MODE:
    case ADC_CAN_MODE:
        ADCStart();
        break;
    case CONFIG_MODE:
    default:
        ADCStop();
        break;
    }


    ecana_pkg_char(mode);
    _cli_load_and_send(cli_frame.priority, mbox);
}

void set_adc_gain(Uint16 mbox){
    char gainSetting = _cli_deserialize_byte();

//    setGain(gainSetting);

    ecana_pkg_char(gainSetting);
    _cli_load_and_send(cli_frame.priority, mbox);
}

////////////////////////////////////////////////////////////
//                 Broadcasts                             //
////////////////////////////////////////////////////////////

void _adc_stream(char* b, Uint16 size){
    Uint16 i;
    for(i = 0; i < size; ++i){
        ecana_pkg_char(*(b + i));
    }
    ecana_pkg_char(0x00);
    ecana_pkg_char(0x00);

    ecana_pkg_char(0x00);
    ecana_pkg_char(0x01);
    ecana_pkg_char(0x02);
    ecana_pkg_char(0x03);
    cli_frame.msg_id = _ADC_STREAM;
    cli_frame.priority = CAN_PRIORITY_REALTIME_MULTI;
    _cli_load_and_send(cli_frame.priority, 3);
}

    
////////////////////////////////////////////////////////////
//                 Geneeric Can functions                 //
////////////////////////////////////////////////////////////

char _cli_dequeue_rx(){
    char c = 0;
    if(cli_rx_buf_size > 0)
    {
        c = cli_rx_buf[cli_read_idx++];
        if(cli_read_idx >= CLI_RX_BUF_SIZE)
        {
            cli_read_idx = 0;
        }
        cli_rx_buf_size--;
    }
    return c;
}

char _cli_peek_rx(){
    if(cli_rx_buf_size > 0)
    {
        return cli_rx_buf[cli_read_idx];
    }
    return 255;
}

void _cli_enqueue_rx(char c)
{
    if(cli_rx_buf_size < CLI_RX_BUF_SIZE)
    {
        cli_rx_buf[cli_rx_idx++] = c;
        if(cli_rx_idx >= CLI_RX_BUF_SIZE)
        {
            cli_rx_idx = 0;
        }
        cli_rx_buf_size++;
    }
}

void _cli_enqueue_can(Uint16 len, Uint32 msgh, Uint32 msgl)
{
    Uint16 i;
    Uint16 byte;
//    dbg_single_byte_since ++;
    for(i = 0; i < len; i++)
    {
        switch(i)
        {
            case 0: byte = (0xFF000000 & msgl)>>24; break;
            case 1: byte = (0x00FF0000 & msgl)>>16; break;
            case 2: byte = (0x0000FF00 & msgl)>>8; break;
            case 3: byte = (0x000000FF & msgl); break;
            case 4: byte = (0xFF000000 & msgh)>>24; break;
            case 5: byte = (0x00FF0000 & msgh)>>16; break;
            case 6: byte = (0x0000FF00 & msgh)>>8; break;
            case 7: byte = (0x000000FF & msgh); break;
            default:
                    byte = 0; // should never get here
                    break;
        }
        _cli_enqueue_rx(byte);
    }
}

void _cli_enqueue_tx(char c)
{
    if( cli_tx_buf_size < CLI_TX_BUF_SIZE )
    { // make sure you don't overrun the buffer size
        cli_tx_buf[cli_tx_idx_b++] = c;
        if(cli_tx_idx_b >= CLI_TX_BUF_SIZE)
        {
            cli_tx_idx_b = 0;
        }
        cli_tx_buf_size++;
    }
}

char _cli_dequeue_tx()
{
    char byte = 0;
    if(cli_tx_buf_size > 0)
    {
        byte = cli_tx_buf[cli_tx_idx_f++];
        if(cli_tx_idx_f >= CLI_TX_BUF_SIZE)
        {
            cli_tx_idx_f = 0;
        }
        cli_tx_buf_size--;
        if(cli_tx_buf_size == 0)
            delay_following_frames = false;
    }
    return byte;
}

void _cli_dequeue_can(Uint16 *len, Uint32 *msgh, Uint32 *msgl)
{
    unsigned int i;
    Uint16 byte;
    (*msgh) = 0x00000000;
    (*msgl) = 0x00000000;
    (*len) = 0;
    for(i = 0; i < 8 && cli_tx_buf_size > 0; i++)
    {
        byte = _cli_dequeue_tx();
        (*len) = i+1;
        switch(i)
        {
            case 0: (*msgl) |= (Uint32)(0xFF&byte)<<24; break;
            case 1: (*msgl) |= (Uint32)(0xFF&byte)<<16; break;
            case 2: (*msgl) |= (Uint32)(0xFF&byte)<<8; break;
            case 3: (*msgl) |= (Uint32)(0xFF&byte); break;
            case 4: (*msgh) |= (Uint32)(0xFF&byte)<<24; break;
            case 5: (*msgh) |= (Uint32)(0xFF&byte)<<16; break;
            case 6: (*msgh) |= (Uint32)(0xFF&byte)<<8; break;
            case 7: (*msgh) |= (Uint32)(0xFF&byte); break;
            default:
                    // should never get here
                    break;
        }
    }
}

void _cli_buff_to_can_msg_uint16(Uint16 *buf, Uint16 len, Uint32 *msgh, Uint32 *msgl)
{
    unsigned int ii;
    Uint16 byte;
    (*msgh) = 0x00000000;
    (*msgl) = 0x00000000;
    for(ii = 0; ii < len; ii++)
    {
        CH_GET(buf, ii, byte);
        //byte = buf[ii];
        //len = ii+1;
        switch(ii)
        {
            case 0: (*msgl) |= (Uint32)(0xFF&byte)<<24; break;
            case 1: (*msgl) |= (Uint32)(0xFF&byte)<<16; break;
            case 2: (*msgl) |= (Uint32)(0xFF&byte)<<8; break;
            case 3: (*msgl) |= (Uint32)(0xFF&byte); break;
            case 4: (*msgh) |= (Uint32)(0xFF&byte)<<24; break;
            case 5: (*msgh) |= (Uint32)(0xFF&byte)<<16; break;
            case 6: (*msgh) |= (Uint32)(0xFF&byte)<<8; break;
            case 7: (*msgh) |= (Uint32)(0xFF&byte); break;
            default:
                    // should never get here
                    break;
        }
    }
}

void _cli_buff_to_can_msg(char *buf, Uint16 len, Uint32 *msgh, Uint32 *msgl)
{
    unsigned int ii;
    Uint16 byte;
    (*msgh) = 0x00000000;
    (*msgl) = 0x00000000;
    for(ii = 0; ii < len; ii++)
    {
        byte = buf[ii];
        //len = ii+1;
        switch(ii)
        {
            case 0: (*msgl) |= (Uint32)(0xFF&byte)<<24; break;
            case 1: (*msgl) |= (Uint32)(0xFF&byte)<<16; break;
            case 2: (*msgl) |= (Uint32)(0xFF&byte)<<8; break;
            case 3: (*msgl) |= (Uint32)(0xFF&byte); break;
            case 4: (*msgh) |= (Uint32)(0xFF&byte)<<24; break;
            case 5: (*msgh) |= (Uint32)(0xFF&byte)<<16; break;
            case 6: (*msgh) |= (Uint32)(0xFF&byte)<<8; break;
            case 7: (*msgh) |= (Uint32)(0xFF&byte); break;
            default:
                    // should never get here
                    break;
        }
    }
}

Uint16 _cli_tx_buf_size()
{
    return cli_tx_buf_size;
}

Uint32 _cli_rx_ready()
{
    return cli_rx_buf_size;
}

void _cli_empty_queue()
{
    while(_cli_rx_ready()){
        _cli_dequeue_rx();
    }
}

void _cli_send_tx_data(Uint16 mbox)
{
    Uint16 msg_len;
    Uint32 msgh, msgl;
    _cli_dequeue_can(&msg_len, &msgh, &msgl);
    if(msg_len>0)
        msg_id_broadcast(cli_frame.priority, cli_frame.msg_id);
    ecana_send(mbox /*mailbox num*/, 0x03 /*msg id*/, msg_len, msgh, msgl);
}

char _cli_deserialize_byte()
{
    return _cli_dequeue_rx();
}

Uint32 _cli_deserialize_uint32()
{
    char h1,h2,l1,l2;
    Uint32 result;
    h1 = _cli_dequeue_rx();
    h2 = _cli_dequeue_rx();
    l1 = _cli_dequeue_rx();
    l2 = _cli_dequeue_rx();

    result = h1;
    result <<= 8;

    result |= h2;
    result <<= 8;

    result |= l1;
    result <<= 8;

    result |= l2;

    return result;
}

int32 _cli_deserialize_int32()
{
    char h1,h2,l1,l2;
    int32 result;
    h1 = _cli_dequeue_rx();
    h2 = _cli_dequeue_rx();
    l1 = _cli_dequeue_rx();
    l2 = _cli_dequeue_rx();

    result = h1;
    result <<= 8;

    result |= h2;
    result <<= 8;

    result |= l1;
    result <<= 8;

    result |= l2;

    return result;
}

Uint16 _cli_deserialize_uint16()
{
    Uint16 result = 0;
    result = (_cli_dequeue_rx()<<8);
    result |= _cli_dequeue_rx();

    return result;
}

int16 _cli_deserialize_int16()
{
    int16 result = 0;
    result = (_cli_dequeue_rx()<<8);
    result |= _cli_dequeue_rx();
    return result;
}

float _cli_deserialize_float()
{
    int32 val = _cli_deserialize_int32();
    void *ptr = &val;
    float32 result = *((float32 *)ptr);
    return result;
}

Uint16 _cli_deserialize_bytes(char *buffer, int len)
{
    // Returns size read
    int ii = 0;
    while(_cli_rx_ready() && (ii < len))
    {
        buffer[ii] = _cli_dequeue_rx();
        ii++;
    }
    if(_cli_rx_ready())
    {
        _cli_dequeue_rx();
    }
    return ii;
}


Uint16 _cli_deserialize_string_w_uint16(Uint16 *buffer, int len)
{
    // Returns size read
    int ii = 0;
    while(_cli_rx_ready() && (ii < len) && (_cli_peek_rx() != 0))
    {
        CH_PUT(buffer, ii, _cli_dequeue_rx());
        //buffer[ii] = _cli_dequeue_rx();
        ii++;
    }
    if(_cli_rx_ready() && (_cli_peek_rx() == 0))
    {
        _cli_dequeue_rx();
    }
    return ii;
}

Uint16 _cli_deserialize_string(char *buffer, int len)
{
    // Returns size read
    int ii = 0;
    while(_cli_rx_ready() && (ii < len) && (_cli_peek_rx() != 0))
    {
        buffer[ii] = _cli_dequeue_rx();
        ii++;
    }
    if(_cli_rx_ready() && (_cli_peek_rx() == 0))
    {
        _cli_dequeue_rx();
    }
    return ii;
}

Uint16 _cli_deserialize_string_to_word_buf(Uint16 *buffer, Uint16 start, int len)
{
    // Returns size read
    int ii = 0;
    char c;
    while(_cli_rx_ready() && (ii < len) && (_cli_peek_rx() != 0))
    {
        c = _cli_dequeue_rx();
        CH_PUT(buffer, start + ii, c);
        ii++;
    }
    if(_cli_rx_ready() && (_cli_peek_rx() == 0))
    {
        _cli_dequeue_rx();
    }
    return ii;
}

void _handle_end_of_stream(Uint16 nacks)
{
    Uint32 msgh, msgl;
    char send_buf[1] = {0};
    if (nacks > DEFAULT_NACK_RETRIES)
    {
        send_buf[0] = NACK_MESSAGE;
    }
    else
    {
        send_buf[0] = EOT_MESSAGE;
    }
    _cli_buff_to_can_msg(&send_buf[0], 1, &msgh, &msgl);
    ecana_send(DEFAULT_TX_MBOX /*mailbox num*/, 0x03 /*msg id*/, 1, msgh, msgl);
    streaming = 0;
}

void _cli_load_tx(Uint16 priority)
{
    Uint16 ii;
    Uint16 length = ecana_pkg_idx;
    ecana_pkg_idx = 0;
    loading_tx_lock = true;
    // Single Frame
    if (priority == CAN_PRIORITY_REALTIME_SINGLE || priority == CAN_PRIORITY_QUERY_SFRAME || priority == CAN_PRIORITY_BRCAST_SINGLE)
    {
        for (ii = 0; ii < length; ii++)
        {
            _cli_enqueue_tx(ecana_pkg_buf[ii]);
        }
    }
    // Stream
    else if (priority == CAN_PRIORITY_STREAM)
    {
    }
    // Multi Frame
    else
    {
        Uint16 size = 0;
        Uint16 jj;
        Uint16 mod = 0;
        Uint16 data_to_crc = 0;
        Uint16 crc = 0;
        // add 2 to length for crc
        length += 2;
        while (length - size)
        {
            mod = 0;
            Uint16 frame_size = MIN((length-size), 6);
            _cli_enqueue_tx(size+frame_size);
            _cli_enqueue_tx(length);
            for (jj = 0; jj < frame_size; jj++)
            {
                if ((size + jj) == length -2)
                {
                    //if ((length % 2) == 1)
                    //{
                    //crc = crc_16_update(crc, data_to_crc);
                    //}
                    _cli_enqueue_tx((crc>>8) & 0xFF);
                }
                else if ((size + jj) == length -1)
                {
                    _cli_enqueue_tx(crc & 0xFF);
                }
                else {
                    _cli_enqueue_tx(ecana_pkg_buf[size + jj]);
                    if (mod == 0)
                    {
                        data_to_crc = ecana_pkg_buf[size + jj] << 8;
                    }
                    else
                    {
                        data_to_crc |= ecana_pkg_buf[size + jj];
                        crc = crc_16_update(crc, data_to_crc);
                    }
                }
                mod = (mod +1) % 2;
            }
            size += frame_size;
        }
    }
    loading_tx_lock = false;
}
void _cli_load_and_send(Uint16 priority, Uint16 mbox)
{
//    if(priority == CAN_PRIORITY_BRCAST_MULTI)
//        delay_following_frames = true;
    _cli_load_tx(priority);
    _cli_send_tx_data(mbox);
}

Uint16 dbg_frame_not_null = 0;
Uint16 dbg_frame_tool_type = 0;
Uint16 dbg_frame_msg_id = 256;

Uint16 cli_could_handle_frame(cli_frame_data_t * frame)
{


    if (!frame)
    {
        dbg_frame_not_null = 7;
        dbg_frame_tool_type = 19;
        dbg_frame_msg_id = 257;
        return false;
    }

    if (_cli_function_buf[frame->tool_type] != NULL)
    {
        if (*(_cli_function_buf[frame->tool_type])->array[frame->msg_id] != NULL)
        {
            if ((Uint32)*(_cli_function_buf[frame->tool_type])->array[frame->msg_id] != 0xFFFFFFFF)
                return true;
        }
    }
    dbg_frame_not_null = 13;
    dbg_frame_tool_type = frame->tool_type;
    dbg_frame_msg_id = frame->msg_id;
    return false;
}

static inline _clean_up_for_command(cli_frame_data_t cli_frame)
{
    switch (cli_frame.priority)
    {
    case 1:
    case 2:
    case 4:
    case 7:
        multiframe_map_remove_frame(&cli_frame);
        break;
    case 0:
    case 3:
    case 6:
    default:
        break;
    }
}

static Uint16 _cli_wait_mframe_done(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following)
{
    Uint16 ii;
    Uint16 err = 0;
    if((err = wait_mframe_done(frame_id, size, timeout_ms, data_following)) != 0)
    {
        for (ii = 0; ii < size; ii++)
            _cli_dequeue_rx();
    }
    _clean_up_for_command(cli_frame);
    return err;
}

static Uint16 _cli_wait_mframe_stream_cmd_done(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following)
{
    Uint16 ii;
    Uint16 err;
    if((err = wait_mframe_done(frame_id, size, timeout_ms, data_following)) != 0)
    {
        for (ii = 0; ii < size; ii++)
            _cli_dequeue_rx();
        return err;
    }
    return 0;
}

void save_existing_cli_frame_data(cli_frame_data_t *frame)
{
    memcpy(frame, &cli_frame, sizeof(cli_frame));
}

void overwrite_cli_frame(cli_frame_data_t *frame)
{
    memcpy(&cli_frame, frame, sizeof(cli_frame));
}
/******************************************
 *******************************************
 **   END ERDOS MILLER'S BACKGROUND IP   ***
 *******************************************
 ******************************************/

