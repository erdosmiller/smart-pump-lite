/*
 * usb.c
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#include "usb.h"
#include "adc.h"
#include "fuse.h"
#include "version.h"
#include "util.h"

#include <xdc/cfg/global.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Clock.h>



extern uint32_t adc_err_crc1_count;
extern uint32_t adc_err_crc2_count;
extern uint32_t adc_err_flags;
extern uint32_t adc_err_state_count;
extern int32_t adc_err_sync;

uint32_t _usb_err_flags;
uint32_t _usb_cmd;
uint32_t _usb_cmd_len;
uint8_t _usb_cmd_data[64];
uint8_t _usb_eeprom_req_block_num;
uint8_t _usb_eeprom_req_block_section;
int _usb_eeprom_write_limit;

bool eeprom_save_req_from_usb;
uint8_t eeprom_req_block_num;
char eeprom_usb_data[256];


char msg_adc_block_count[12 + 4] = {0x01, 0x45, 0x4D, 0x00, 0x00,
                                    0x00, 0x40, // command
                                    0x00, 0x04, // length
                                    0x02,
                                    0x00, 0x00, 0x00, 0x00,
                                    0x03, 0x04};


void usb_init(void)
{
    // Enable and Setup USB
    g_bUSBConfigured = false;
    _usb_eeprom_write_limit = 10;
    eeprom_req_block_num = 0;
    eeprom_save_req_from_usb = false;

    USBBufferInit(&g_sTxBuffer);
    USBBufferInit(&g_sRxBuffer);

    SysCtlVCOGet(SYSCTL_XTAL_25MHZ, &ui32PLLRate);
    USBDCDFeatureSet(0, USBLIB_FEATURE_CPUCLK, &ui32SysClock);
    USBDCDFeatureSet(0, USBLIB_FEATURE_USBPLL, &ui32PLLRate);

    USBStackModeSet(0, eUSBModeForceDevice, 0); // initialize stack
    USBDBulkInit(0, &g_sBulkDevice); // place the device on bus

}

uint32_t usb_receive_cmd(tUSBDBulkDevice *psDevice, uint8_t *pi8Data, uint_fast32_t ui32NumBytes)
{
    uint_fast32_t idx_read;
    uint32_t ii;

    // Find the read buffer index
    idx_read = (uint32_t)(pi8Data - g_pui8USBRxBuffer);

    // Just read the command header for now
    for(ii = 0; ii < 7; ii++) {
        _usb_cmd_data[ii] = g_pui8USBRxBuffer[idx_read++];
        idx_read = (idx_read == BULK_RX_SIZE) ? 0 : idx_read;
    }

    // Parse the received command
    _usb_cmd = (((uint32_t)_usb_cmd_data[5])<<8) | ((uint32_t)_usb_cmd_data[6]);


    switch(_usb_cmd) {
    case CMD_HW_VERSION:
        _usb_cmd_len = 12;
        break;
    case CMD_FW_VERSION:
        _usb_cmd_len = 12;
        break;
    case CMD_DEVICE_MODE:
        _usb_cmd_len = 13;
        break;
    case CMD_EEPROM_READ:
        _usb_cmd_len = 14;
        break;
    case CMD_EEPROM_WRITE:
        _usb_cmd_len = CMD_BASE_LENGTH + EEPROM_BLOCK_MESSAGE_LENGTH;
        break;
    case CMD_EEPROM_SAVE:
        _usb_cmd_len = 13;
        break;
    case CMD_BOOTLOAD_MODE:
        _usb_cmd_len = 12;
        break;
    case REQ_FUSE_STATES:
        _usb_cmd_len = 12;
        break;
    case RESET_FUSE:
        _usb_cmd_len = 13;
        break;
    case REQ_FUSE_RESET_COUNTS:
        _usb_cmd_len = 12;
        break;
    case REQ_ERROR_FLAGS:
        _usb_cmd_len = 12;
        break;
    case CMD_ADC_BLOCK_CNT:
        _usb_cmd_len = 12;
        break;
    case CMD_ADC_BLOCK_REQ:
        _usb_cmd_len = 12;
        break;
    case CMD_ADC_BLOCK_POP:
        _usb_cmd_len = 12;
        break;
    case REQ_RESEND_ADC1:
        _usb_cmd_len = 16;
        break;
    case REQ_ID_BLOCK:
        _usb_cmd_len = 12;
        break;
    case SET_ID_BLOCK_1:
        _usb_cmd_len = 52;
        break;
    case SET_ID_BLOCK_2:
        _usb_cmd_len = 44;
        break;
    case SET_ID_BLOCK_3:
        _usb_cmd_len = 60;
        break;
    default:
        // ERROR: invalid command
        _usb_err_flags |= USB_ERR_FLAG_CMD_UNKNOWN;
        _usb_cmd_len = ui32NumBytes; // read all of the data
        break;
    }

    if(ui32NumBytes != _usb_cmd_len) {
        _usb_err_flags |= USB_ERR_FLAG_CMD_BAD_LEN;
    }

    // Read the rest of the data, fill the buffer but don't overflow
    for(ii = 7; ii < _usb_cmd_len; ii++) {
        // Check for buffer overflow
        if(ii >= 64) {
            _usb_err_flags |= USB_ERR_FLAG_CMD_OVRFLOW;
            break;
        }

        _usb_cmd_data[ii] = g_pui8USBRxBuffer[idx_read++];

        // Handle buffer wrap
        if(idx_read >= BULK_RX_SIZE) {
            idx_read = 0;
        }
    }

    Swi_post(usb_data_process_swi_hndl);


    //
    // We processed as much data as we can directly from the receive buffer so
    // we need to return the number of bytes to allow the lower layer to
    // update its read pointer appropriately.
    //
    return(ui32NumBytes);
}

void usb_data_process_swi(void)
{
    uint8_t *adc_buf = 0;
    uint32_t adc_buf_len = 0;
    uint32_t adc_blocks_ready = 0;
    uint32_t ii;

    //
    // WARNING: !!! priority of this SWI should be LOWER than ADC SWI !!!
    // otherwise a race condition might be introduced into block count calculations
    //

    // TODO: check for malformed header

    // TODO: check for malformed tail

    // TODO: Should we set a timeout for 'USBBufferWrite' transmissions?
    // We could potentially use 'TxHandler' to check if the transmission was read by the Host

    switch(_usb_cmd)
    {
        case CMD_HW_VERSION:
            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)hw_message, sizeof(hw_message));
            break;

        case CMD_FW_VERSION:
            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)fw_message, sizeof(fw_message));
            break;

        case CMD_DEVICE_MODE:
            device_mode = _usb_cmd_data[10];
            mode_message[MODE_OFFSET] = device_mode;
            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)mode_message, sizeof(mode_message));
            break;

        case CMD_EEPROM_READ:

            _usb_eeprom_req_block_num = _usb_cmd_data[CMD_BODY_START_INDEX];
            _usb_eeprom_req_block_section = _usb_cmd_data[CMD_BODY_START_INDEX + 1];

            if ((_usb_eeprom_req_block_num < EEPROM_BLOCK_COUNT) &&
                (_usb_eeprom_req_block_section < EEPROM_BLOCK_SECTION_COUNT))
            {
                // On Successful block or section numbers, send success status, and info back.
                eeprom_read_block_message[CMD_BODY_START_INDEX    ] = 0xB6;
                eeprom_read_block_message[CMD_BODY_START_INDEX + 1] = _usb_eeprom_req_block_num;
                eeprom_read_block_message[CMD_BODY_START_INDEX + 2] = _usb_eeprom_req_block_section;
                for (ii = 0; ii < 32; ii++)
                {
                    eeprom_read_block_message[CMD_BODY_START_INDEX + 3 + ii] = eeprom_usb_data[(_usb_eeprom_req_block_section * EEPROM_BYTES_PER_SECTION) + ii];
                }
            }
            else
            {
                // On Erroneous block or section numbers, send error status of 0xBB, and the errorneous info back.
                eeprom_read_block_message[CMD_BODY_START_INDEX    ] = 0xBB;
                eeprom_read_block_message[CMD_BODY_START_INDEX + 1] = _usb_eeprom_req_block_num;
                eeprom_read_block_message[CMD_BODY_START_INDEX + 2] = _usb_eeprom_req_block_section;
            }

            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)eeprom_read_block_message, sizeof(eeprom_read_block_message));

            break;
        
        case CMD_EEPROM_WRITE:

            _usb_eeprom_req_block_num = _usb_cmd_data[CMD_BODY_START_INDEX];
            _usb_eeprom_req_block_section = _usb_cmd_data[CMD_BODY_START_INDEX + 1];

            if (_usb_eeprom_req_block_num < EEPROM_BLOCK_COUNT &&
                _usb_eeprom_req_block_section < EEPROM_BLOCK_SECTION_COUNT)
            {
                // Obtaining 32 bytes from the usb message
                for (ii = 0; ii < 32; ii++)
                {
                    eeprom_usb_data[(_usb_eeprom_req_block_section * EEPROM_BYTES_PER_SECTION) + ii] = _usb_cmd_data[CMD_BODY_START_INDEX + 2 + ii];
                }
                // Setting ACK status of message to 0xB6 = Block (info) 6ood.
                eeprom_write_block_resp_message[CMD_BODY_START_INDEX] = 0xB6;
                USBBufferWrite(&g_sTxBuffer, (const uint8_t *)eeprom_write_block_resp_message, sizeof(eeprom_write_block_resp_message));
            }
            else
            {
                // Setting ACK status message to 0xBB = Block (info) Bad
                eeprom_write_block_resp_message[CMD_BODY_START_INDEX] = 0xBB;
                USBBufferWrite(&g_sTxBuffer, (const uint8_t *)eeprom_write_block_resp_message, sizeof(eeprom_write_block_resp_message));
            }
            break;

        case CMD_EEPROM_SAVE:
            if (_usb_eeprom_write_limit > 0)
            {
                if (_usb_cmd_data[CMD_BODY_START_INDEX] < EEPROM_BLOCK_COUNT)
                {
                    eeprom_req_block_num = _usb_cmd_data[CMD_BODY_START_INDEX];
                    eeprom_save_req_from_usb = true;
                    _usb_eeprom_write_limit--;
                    // Send success response (0x55 = 5ave 5uccessful)
                    eeprom_save_response[CMD_BODY_START_INDEX] = 0x55;
                    USBBufferWrite(&g_sTxBuffer, (const uint8_t *)eeprom_save_response, sizeof(eeprom_save_response));
                }
                else
                {
                    // Send error response (0xBB = Block (info) Bad)
                    eeprom_save_response[CMD_BODY_START_INDEX] = 0xBB;
                    USBBufferWrite(&g_sTxBuffer, (const uint8_t *)eeprom_save_response, sizeof(eeprom_save_response));
                }
            }
            else
            {
                // Send error response (0x1E = 1imit Exceeded)
                _usb_err_flags |= USB_ERR_FLAG_WRITE_INFO_LIM;
                eeprom_save_response[CMD_BODY_START_INDEX] = 0x1E;
                USBBufferWrite(&g_sTxBuffer, (const uint8_t *)eeprom_save_response, sizeof(eeprom_save_response));
            }
            break;    
        
        case CMD_BOOTLOAD_MODE:
            go_to_bootloader_flag = 1;
            break;

        case REQ_FUSE_STATES:

            if(GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_2) == 0x00) {
                fuse_states_message[CMD_BODY_START_INDEX] = 0x00;
            } else {
                fuse_states_message[CMD_BODY_START_INDEX] = 0x01;
            }

            if(GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_4) == 0x00) {
                fuse_states_message[CMD_BODY_START_INDEX + 1] = 0x00;
            } else {
                fuse_states_message[CMD_BODY_START_INDEX + 1] = 0x01;
            }

            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)fuse_states_message, sizeof(fuse_states_message));
            break;
        
        case RESET_FUSE:
            break;
        case REQ_FUSE_RESET_COUNTS:
            get_fuse_reset_counts( (uint8_t *) &(fuse_reset_counts_message[CMD_BODY_START_INDEX    ]),
                                   (uint8_t *) &(fuse_reset_counts_message[CMD_BODY_START_INDEX + 1]) );
            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)fuse_reset_counts_message, sizeof(fuse_reset_counts_message));
            break;
        case CMD_ADC_BLOCK_CNT:
            adc_blocks_ready = adc_block_count();
            msg_adc_block_count[10] = (0xFF000000 & adc_blocks_ready) >> 24;
            msg_adc_block_count[11] = (0x00FF0000 & adc_blocks_ready) >> 16;
            msg_adc_block_count[12] = (0x0000FF00 & adc_blocks_ready) >> 8;
            msg_adc_block_count[13] = (0x000000FF & adc_blocks_ready) >> 0;
            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)msg_adc_block_count, sizeof(msg_adc_block_count));
            break;

        case CMD_ADC_BLOCK_REQ:
            adc_blocks_ready = adc_block_read(&adc_buf, &adc_buf_len);
            if(adc_blocks_ready > 0) {
                USBBufferWrite(&g_sTxBuffer, (const uint8_t *)adc_buf, adc_buf_len);
            } else {
                _usb_err_flags |= USB_ERR_FLAG_BUF_UNDERFLOW;
            }
            break;

        case CMD_ADC_BLOCK_POP:
            adc_block_pop();
            break;

        case REQ_RESEND_ADC1:
            break;

        case REQ_ID_BLOCK:
            break;

        case SET_ID_BLOCK_1:
            break;

        case SET_ID_BLOCK_2:
            break;

        case SET_ID_BLOCK_3:
            break;

        case REQ_ERROR_FLAGS:

            error_flags_response_message[CMD_BODY_START_INDEX    ] = adc_err_flags >> 24;
            error_flags_response_message[CMD_BODY_START_INDEX + 1] = adc_err_flags >> 16;
            error_flags_response_message[CMD_BODY_START_INDEX + 2] = adc_err_flags >>  8;
            error_flags_response_message[CMD_BODY_START_INDEX + 3] = adc_err_flags;

            error_flags_response_message[CMD_BODY_START_INDEX + 4] = adc_err_state_count >> 24;
            error_flags_response_message[CMD_BODY_START_INDEX + 5] = adc_err_state_count >> 16;
            error_flags_response_message[CMD_BODY_START_INDEX + 6] = adc_err_state_count >>  8;
            error_flags_response_message[CMD_BODY_START_INDEX + 7] = adc_err_state_count;

            error_flags_response_message[CMD_BODY_START_INDEX + 8]  = adc_err_crc1_count >> 24;
            error_flags_response_message[CMD_BODY_START_INDEX + 9]  = adc_err_crc1_count >> 16;
            error_flags_response_message[CMD_BODY_START_INDEX + 10] = adc_err_crc1_count >>  8;
            error_flags_response_message[CMD_BODY_START_INDEX + 11] = adc_err_crc1_count;

            error_flags_response_message[CMD_BODY_START_INDEX + 12] = adc_err_crc2_count >> 24;
            error_flags_response_message[CMD_BODY_START_INDEX + 13] = adc_err_crc2_count >> 16;
            error_flags_response_message[CMD_BODY_START_INDEX + 14] = adc_err_crc2_count >>  8;
            error_flags_response_message[CMD_BODY_START_INDEX + 15] = adc_err_crc2_count;

            error_flags_response_message[CMD_BODY_START_INDEX + 16] = adc_err_sync >> 24;
            error_flags_response_message[CMD_BODY_START_INDEX + 17] = adc_err_sync >> 16;
            error_flags_response_message[CMD_BODY_START_INDEX + 18] = adc_err_sync >>  8;
            error_flags_response_message[CMD_BODY_START_INDEX + 19] = adc_err_sync;

            error_flags_response_message[CMD_BODY_START_INDEX + 20] = _usb_err_flags >> 24;
            error_flags_response_message[CMD_BODY_START_INDEX + 21] = _usb_err_flags >> 16;
            error_flags_response_message[CMD_BODY_START_INDEX + 22] = _usb_err_flags >>  8;
            error_flags_response_message[CMD_BODY_START_INDEX + 23] = _usb_err_flags;

            error_flags_response_message[CMD_BODY_START_INDEX + 24] = eeprom_crc_error_count;
            error_flags_response_message[CMD_BODY_START_INDEX + 25] = eeprom_read_error_count;
            error_flags_response_message[CMD_BODY_START_INDEX + 26] = eeprom_write_error_count;

            USBBufferWrite(&g_sTxBuffer, (const uint8_t *)error_flags_response_message, sizeof(error_flags_response_message));
            break;

        default:
            break;
    }

    return;
}


uint32_t TxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue, void *pvMsgData)
{
  return(0);
}

uint32_t RxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue, void *pvMsgData)
{
    //
    // Which event are we being sent?
    //
    switch(ui32Event)
    {
        //
        // We are connected to a host and communication is now possible.
        //
        case USB_EVENT_CONNECTED:
        {
            g_bUSBConfigured = true;
            g_ui32Flags |= COMMAND_STATUS_UPDATE;

            //
            // Flush our buffers.
            //
            USBBufferFlush(&g_sTxBuffer);
            USBBufferFlush(&g_sRxBuffer);

            break;
        }

        //
        // The host has disconnected.
        //
        case USB_EVENT_DISCONNECTED:
        {
            g_bUSBConfigured = false;
            g_ui32Flags |= COMMAND_STATUS_UPDATE;
            break;
        }

        //
        // A new packet has been received.
        //
        case USB_EVENT_RX_AVAILABLE:
        {
            tUSBDBulkDevice *psDevice;

            //
            // Get a pointer to our instance data from the callback data
            // parameter.
            //
            psDevice = (tUSBDBulkDevice *)pvCBData;

            //
            // Read the new packet and echo it back to the host.
            //
            return(usb_receive_cmd(psDevice, pvMsgData, ui32MsgValue));
        }

        //
        // Ignore SUSPEND and RESUME for now.
        //
        case USB_EVENT_SUSPEND:
        case USB_EVENT_RESUME:
            break;

            //
            // Ignore all other events and return 0.
            //
        default:
            break;
    }

    return(0);
}

