/*
 * fuse.c
 *
 *  Created on: June 14, 2021
 *      Author: Jack Baker
 */


#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/cfg/global.h>
#include <ti/sysbios/BIOS.h>

#include <stdbool.h>
#include <stdint.h>

#include "fuse.h"
#include "globals.h"
#include "adc_internal.h"
#include "adc.h"
#include "util.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/timer.h"
#include "driverlib/eeprom.h"


uint8_t _fuse1_reset_count = 0;
uint8_t _fuse2_reset_count = 0;

bool _fuse1_to_be_reset = false;
bool _fuse2_to_be_reset = false;


void fuse_ch1_enable()
{
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_3, GPIO_PIN_3);
}

void fuse_ch1_disable()
{
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_3, 0x0);
}

void fuse_ch2_enable()
{
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_5, GPIO_PIN_5);
}

void fuse_ch2_disable()
{
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_5, 0x0);
}

void init_fuses()
{
    fuse_ch1_enable();
    util_led_rgb_2(LED_BLU);

    fuse_ch2_disable();
    util_led_rgb_1(LED_RED);
    mp_channel_2_is_off = true;
}

void fuse_monitor_task(void)
{

    while(1)
    {
        if (_fuse1_to_be_reset)
        {
            _fuse1_to_be_reset = false;
            _fuse1_reset_count++;

            // Resetting fuse 1
            GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_3, GPIO_PIN_3);

        }
        else if (_fuse2_to_be_reset)
        {
            _fuse2_to_be_reset = false;
            _fuse2_reset_count++;

            // Resetting fuse 2
            if (!mp_channel_2_is_off)
            {
                GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_5, GPIO_PIN_5);
            }
        }
        else
        {
            // Check if either of the eFuses have tripped and set blinking LED if necessary
            fuse1_state = GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_2) == 0x00? 0x00:
                                                                            0x01;
            if(!fuse1_state)
            {
                _fuse1_to_be_reset = true;

                if(!led_rgb_2_is_blinking)
                {
                    util_led_rgb_2(LED_OFF);
                    //Timer_start(util_rgb_led_2_eflt);
                    led_rgb_2_is_blinking = true;
                }
            }
            else
            {
                if(led_rgb_2_is_blinking)
                {
                    //Timer_stop(util_rgb_led_2_eflt);
                    util_led_rgb_2(LED_BLU);
                    led_rgb_2_is_blinking = false;
                    adc1_ma_data_init = true;
                }
            }


            fuse2_state = GPIOPinRead(GPIO_PORTN_BASE, GPIO_PIN_4) == 0x00? 0x00:
                                                                            0x01;
            if(!fuse2_state && !mp_channel_2_is_off)
            {
                _fuse2_to_be_reset = true;

                if(!led_rgb_1_is_blinking)
                {
                    util_led_rgb_1(LED_OFF);
                    //Timer_start(util_rgb_led_1_eflt);
                    led_rgb_1_is_blinking = true;
                }
            }
            else if(!mp_channel_2_is_off)
            {
                if(led_rgb_1_is_blinking)
                {
                    //Timer_stop(util_rgb_led_1_eflt);
                    util_led_rgb_1(LED_BLU);
                    led_rgb_1_is_blinking = false;
                    adc2_ma_data_init = true;
                }
            }

        }
    
        // Sleep for a tenth of a second (100 cpu ticks) since Cpu period is 1000 us.
        Task_sleep(100);
    }
    
}

void get_fuse_reset_counts(uint8_t *fuse1, uint8_t *fuse2)
{
    *(fuse1) = _fuse1_reset_count;
    *(fuse2) = _fuse2_reset_count;
    return;
}

