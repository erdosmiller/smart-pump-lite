/*
 * adc_internal.c
 *
 *  Created on: July 2, 2020
 *      Author: Nathan
 */

#include <stdbool.h>
#include <stdint.h>

#include "adc_internal.h"
#include "adc.h"

#include "globals.h"
#include "util.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_adc.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/rom.h"
