/*
 * globals.h
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "version.h"

#include <stdbool.h>
#include <stdint.h>

extern volatile bool g_bUSBConfigured;

extern uint32_t ui32PLLRate;
extern uint32_t ui32SysClock;


//*****************************************************************************
// Flags used to pass commands from interrupt context to the main loop.
//*****************************************************************************
#define COMMAND_PACKET_RECEIVED 0x00000001
#define COMMAND_STATUS_UPDATE   0x00000002

extern volatile uint32_t g_ui32Flags;


//*****************************************************************************
//
// Global variables
//
//*****************************************************************************

#define ADC_SAMPLES             256
#define USB_BYTES_TO_SEND       ADC_SAMPLES * 4
// 4 for data block index
// 2 for number of samples
// 2 for number of CRC (ADC1 only for now)
#define ADC1_DATA_BODY_LENGTH    (ADC_SAMPLES * 3) + 8
#define ADC_DATA_BODY_LENGTH    (ADC_SAMPLES * 3) + 6
// CAN xdcr will just be four byte floats
#define CAN_XDCR_DATA_BODY_LENGTH    (ADC_SAMPLES * 4) + 6

#define ERROR_FLAGS_RESP_BODY_LENGTH (64 - CMD_BASE_LENGTH)

#define EEPROM_BLOCK_MESSAGE_LENGTH 34 // eeprom block number + eeprom block section number + 32 bytes of eeprom 
#define EEPROM_READ_RESP_LENGTH (EEPROM_BLOCK_MESSAGE_LENGTH + 1) // eeprom block info + 1 byte for status of successful or unsuccessful read.
#define EEPROM_WRITE_RESP_LENGTH    1
#define EEPROM_SAVE_RESP_LENGTH     1

#define ADC_1   0
#define ADC_2   1

#define EEPROM_BLOCK_COUNT 1
#define EEPROM_BLOCK_SECTION_COUNT 8

#define EEPROM_BYTES_PER_BLOCK 256
#define EEPROM_BYTES_PER_SECTION (EEPROM_BYTES_PER_BLOCK / EEPROM_BLOCK_SECTION_COUNT) // 256 / 32 = 8

//*****************************************************************************
//           Command Definitions / Kind of....
//*****************************************************************************
#define CMD_HW_VERSION       0x0000
#define CMD_FW_VERSION       0x0001
#define CMD_DEVICE_MODE      0x0002
#define CMD_BOOTLOAD_MODE    0x0010
#define CMD_ADC1_DATA        0x0120 // v2 command
#define CMD_ADC2_DATA        0x0021
#define CMD_CAN_XDCR1_DATA   0x0022
#define CMD_CAN_XDCR2_DATA   0x0023

#define CMD_ADC_BLOCK_CNT    0x0040
#define CMD_ADC_BLOCK_REQ    0x0041
#define CMD_ADC_BLOCK_POP    0x0042

#define CMD_EEPROM_READ      0x00E2
#define CMD_EEPROM_WRITE     0x00E3
#define CMD_EEPROM_SAVE      0x00E4

//From PC
#define REQ_FUSE_STATES       0x0030
#define RESET_FUSE            0x0031
#define REQ_FUSE_RESET_COUNTS 0x002C
#define REQ_RESEND_ADC1       0x0050
#define REQ_RESEND_ADC2       0x0051

#define REQ_ID_BLOCK   0x0061
#define SET_ID_BLOCK_1 0x0062
#define SET_ID_BLOCK_2 0x0063
#define SET_ID_BLOCK_3 0x0064

#define REQ_ERROR_FLAGS 0x00EF

//From board
#define FUSE_STATES        0x0030
#define FUSE_STATE_CHANGED 0x0031
#define FUSE_RESET_COUNTS  0x002C

#define NOTIFY_24V_DISCONNECTED 0x0032
#define NOTIFY_24V_CONNECTED    0x0033

#define ID_BLOCK_RESPONSE 0x0061

#define ERROR_FLAGS_RESP  0x00EF

#define HW_MAJOR_OFFSET     10
#define HW_MINOR_OFFSET     11
#define MODE_OFFSET         10
// 10-13 is block index
#define ADC_BLOCK_OFFSET    10

#define CMD_HEADER   0x01, 0x45, 0x4D
#define CMD_RESERVED 0x00, 0x00
#define CMD_STX      0x02
#define CMD_FOOTER   0x03, 0x04

#define CMD_BASE_LENGTH 12
#define CMD_BODY_START_INDEX 10

extern char fw_message[CMD_BASE_LENGTH + 16];
extern char hw_message[CMD_BASE_LENGTH + 16];
extern char mode_message[CMD_BASE_LENGTH + 1];
extern char adc1_header[16];
extern char adc2_header[16];
extern char can_xdcr1_header[16];
extern char can_xdcr2_header[16];

extern char error_flags_response_message[CMD_BASE_LENGTH + ERROR_FLAGS_RESP_BODY_LENGTH];

extern char eeprom_write_block_resp_message[CMD_BASE_LENGTH + EEPROM_WRITE_RESP_LENGTH];
extern char eeprom_read_block_message[CMD_BASE_LENGTH + EEPROM_READ_RESP_LENGTH];
extern char eeprom_save_response[CMD_BASE_LENGTH + EEPROM_SAVE_RESP_LENGTH];

extern char fuse_reset_counts_message[CMD_BASE_LENGTH + 2];
extern char fuse_states_message[CMD_BASE_LENGTH + 2];
extern char fuse_state_changed_message[CMD_BASE_LENGTH + 2];

extern char notify_24v_disconnected[CMD_BASE_LENGTH];
extern char notify_24v_connected[CMD_BASE_LENGTH];

extern char id_block_response[CMD_BASE_LENGTH + 120];

extern char footer_message[2];

#define CONFIG_MODE     0
#define ADC_MODE        1
#define ADC_CAN_MODE    2

extern uint8_t device_mode;

extern uint16_t new_adc1_data;
extern uint16_t new_adc2_data;
//extern int current_adc1_data[ADC_SAMPLES];
//extern int current_adc2_data[ADC_SAMPLES];
//extern int send_adc1_data[ADC_SAMPLES];
//extern int send_adc2_data[ADC_SAMPLES];

//extern uint8_t adc1_final_data[(sizeof(adc1_header) + (sizeof(send_adc1_data)-(sizeof(send_adc1_data)/4)) + 2/*crc*/ + sizeof(footer_message))];
//extern uint8_t adc2_final_data[(sizeof(adc1_header) + (sizeof(send_adc1_data)-(sizeof(send_adc1_data)/4)) + sizeof(footer_message))];
//extern uint32_t adc1_final_data_index;
//extern uint32_t adc2_final_data_index;

//#define ADC_SEND_BUFFER_SIZE 80
//typedef struct{
//    uint32_t blockIndex;
//    uint8_t data[sizeof(adc1_final_data)];
//} AdcSendBufferItem;
//
//extern AdcSendBufferItem adc1SendBuffer[ADC_SEND_BUFFER_SIZE];
//extern AdcSendBufferItem* adc1SendBufferHead;
//
//extern AdcSendBufferItem adc2SendBuffer[ADC_SEND_BUFFER_SIZE];
//extern AdcSendBufferItem* adc2SendBufferHead;

//extern uint32_t debug_adcSendCount;
//extern uint32_t debug_adcSendREQEDIndex;


//CAN Transducer
extern uint16_t new_can_xdcr1_data;
extern uint16_t new_can_xdcr2_data;
extern int current_can_xdcr1_data[ADC_SAMPLES];
extern int current_can_xdcr2_data[ADC_SAMPLES];
extern int send_can_xdcr1_data[ADC_SAMPLES];
extern int send_can_xdcr2_data[ADC_SAMPLES];
//extern uint8_t can_xdcr1_final_data[(sizeof(adc1_header) +
//                                     sizeof(send_adc1_data) +
//                                     sizeof(footer_message))];
//extern uint8_t can_xdcr2_final_data[(sizeof(adc1_header) +
//                                     sizeof(send_adc1_data) +
//                                     sizeof(footer_message))];
extern uint32_t can_xdcr1_final_data_index;
extern uint32_t can_xdcr2_final_data_index;
extern uint16_t current_can_xdcr1_index;
extern uint16_t current_can_xdcr2_index;
extern uint32_t can_xdcr1_block_index;
extern uint32_t can_xdcr2_block_index;

#define ADC1_CAN_DATA_SIZE 4 + ADC_SAMPLES * 3
extern uint8_t adc1_can_data[ADC1_CAN_DATA_SIZE];

//extern uint16_t current_adc1_index;
//extern uint16_t current_adc2_index;
//extern uint32_t adc1_block_index;
//extern uint32_t adc2_block_index;
//extern uint32_t adc1_usb_write_error;
//extern uint32_t adc2_usb_write_error;

extern uint16_t go_to_bootloader_flag;

extern uint32_t set_ch_1_fuse_after_time;
extern uint32_t set_ch_2_fuse_after_time;


// Internal ADC
/**********************************/
//#define ADC_INTERNAL_SS   0
//#define SS_FIFO_DEPTH     8

#define ADC_INTERNAL_SS   1
#define SS_FIFO_DEPTH     4

//#define ADC_INTERNAL_SS   2
//#define SS_FIFO_DEPTH     4

//#define ADC_INTERNAL_SS   3
//#define SS_FIFO_DEPTH     1
/**********************************/
extern float vsns_24v;
extern float vsns_24v_prev;
extern bool vsns_24v_init;
extern bool adc1_ma_data_init;
extern bool adc2_ma_data_init;
extern uint32_t adc0_raw[SS_FIFO_DEPTH];
extern bool sample_adc0;

#define VIN_THRESH 22 // Mud Pulse Channel 2 shuts off when VIN drops below threshold
//#define MA_LOOP_THRESH 3 // Change LED status when loop current falls below 3 mA
#define MA_LOOP_THRESH 1207959 // 3mA in ADC counts

extern int32_t led_rgb_1_state;
extern int32_t led_rgb_2_state;
extern bool led_rgb_1_is_blinking;
extern bool led_rgb_2_is_blinking;
extern float current_adc1_data_ma;
extern float current_adc2_data_ma;
extern float previous_adc1_data_ma;
extern float previous_adc2_data_ma;
extern char fuse1_state;
extern char fuse2_state;
extern bool mp_channel_2_is_off;
extern bool startup;

extern uint8_t eeprom_crc_error_count;
extern uint8_t eeprom_write_error_count;
extern uint8_t eeprom_read_error_count;

#endif /* GLOBALS_H_ */
