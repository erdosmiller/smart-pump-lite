/*
 * util.h
 *
 *  Created on: July 2, 2020
 *      Author: Nathan
 */

#ifndef UTIL_H_
#define UTIL_H_

// RGB LED Set Values
#define LED_OFF 7
#define LED_RED 6
#define LED_GRN 5
#define LED_YEL 4
#define LED_BLU 3
#define LED_VIO 2
#define LED_CYN 1
#define LED_WHT 0

void util_init_leds_rgb();
void util_led_rgb_1(uint8_t led_set);
void util_led_rgb_2(uint8_t led_set);

void util_rgb_led_1_eflt_isr();
void util_rgb_led_2_eflt_isr();

void util_init_mp_pwr();
void util_mp_pwr_ch1_enable();
void util_mp_pwr_ch1_disable();
void util_mp_pwr_ch2_enable();
void util_mp_pwr_ch2_disable();

void util_mp_rx_status_update_isr();

void util_init_eeprom();

void util_eeprom_write();
void util_eeprom_read();

bool util_eeprom_write_block(uint8_t block_num, char *block_data);
bool util_eeprom_read_block(uint8_t block_num, char *block_data);

#endif /* UTIL_H_ */
