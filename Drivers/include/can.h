/*
 * can.h
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#ifndef CAN_H_
#define CAN_H_

#include "globals.h"
#include "emlib.h"

#include <stdbool.h>
#include <stdint.h>

#include "inc/hw_memmap.h"
#include "inc/hw_can.h"
#include "driverlib/can.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"

extern tCANMsgObject sCANMessage;
extern uint8_t pui8MsgData[8];
extern volatile uint32_t g_ui32MsgCount;
extern volatile bool g_bErrFlag;

extern tCANMsgObject sCANMessage_2;
extern uint8_t pui8MsgData_2[8];

void EM_CANInit();
void EchoFrame();
void EM_CANSend(Uint64 id, Uint16 len, Uint64 msg);

//ISR
void CANIntHandler();
void CANIntHandler_2();

#endif /* CAN_H_ */
