
///
/// Erdos Miller CONFIDENTIAL
/// Unpublished Copyright (c) 2015 Erdos Miller, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Erdos Miller. The intellectual and technical concepts contained
/// herein are proprietary to Erdos Miller and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Erdos Miller.  Access to the source code contained herein is hereby forbidden to anyone except current Erdos Miller employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of Erdos Miller.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

/******************************************
 *******************************************
 **  BEGIN ERDOS MILLER'S BACKGROUND IP  ***
 *******************************************
 ******************************************/

/*
 * ecana.h
 *
 *  Created on: Nov 9, 2012
 *      Author: Abe
 */

#ifndef ECANA_H_
#define ECANA_H_

#include "emlib.h"
#include <stdint.h>


// 0x0001 1000 0111 1111 1111 0000 0000 0000
// 0x0001 1111 1111 1111 1111 0000 0000 0000
//      p pptt tttm mmmm mmmr rrrr ssss ssss
//        26   21    13         8        0
#define TOOL_MSG_ONLY_H  0x001F
#define TOOL_DONT_CARE_H 0x1C1F
#define TOOL_DONT_CARE_L 0xE0FF

#define EOT_MESSAGE 0x04
#define ACK_MESSAGE 0x06
#define NACK_MESSAGE 0x15
#define SYNC_MESSAGE 0x16
#define DEFAULT_NACK_RETRIES 5
typedef struct {
    Uint32 serial:8;
    Uint32 reserved:5;
    Uint32 msg_id:8;
    Uint32 tool_type:5;
    Uint32 priority:3;
    Uint32 settings:3;
    Uint32 rtr : 1;
} cli_msg_t;

typedef enum {
    CANBAUD_125KBIT_CAN,
    CANBAUD_250KBIT_CAN,
    CANBAUD_500KBIT_CAN,
    CANBAUD_1MBIT_CAN,
    CANBAUD_COUNT
} can_baud_enum;

union CliMsg
{
    Uint64 all;
    cli_msg_t bit;
};

//#include "DSP280x_Device.h"     // DSP280x Headerfile Include File
//#include "DSP280x_Examples.h"   // DSP280x Examples Include File

/* Usage:
 * ecana_init_gpio(); // this function is board and processor specific
 * ecana_init(100E6, 1E6); // doesn't actually do bitrate calculations... edit '_ecan_calc_bit_rate'
 * ecana_init_rx_callback(0, 0x01, _cli_enqueue_can);     // initialize mailbox 0 as RX for ID = 0x01
 *                                                         // callback to _cli_enqueue_can(Uint16 len, Uint32 msgh, Uint32 msgl);
 *
 * ecana_init_tx_callback(1, _cli_dequeue_can); // optional; initialize mailbox 1 with a callback function, e.g., if you want to empty a queue
 *                                                 // _cli_dequeue_can(Uint16 *len, Uint32 *msgh, Uint32 *msgl); is called on TX finish
 *                                                 // if the fn. returns len = 0, the mailbox interrupt is turned off, start new transfer with 'ecana_send'
 * ecana_send(1, 0x02, msg_len, msgh, msgl);     // send a msg from mailbox 1 as ID = 0x02 of 'msg_len' bytes
 *                                                 // will got to the callback tx finish if initialized on
 */

typedef struct ECANA_TEST_STRUCT
{
    Uint32 MsgH,MsgL;
    Uint16 Length, ecana_isr_0_cntr, ecana_isr_1_cntr, ecana_isr_1_rx_cntr, ecana_isr_1_tx_cntr,
           ecana_rx_interrupt_cntr,ecana_isr1_ack_cntr;
}ECANA_STR;

//#define SIMPLE_CAN_QUEUE_SIZE 4
//#define SIMPLE_CAN_RX_MBOX 4
//#define SIMPLE_CAN_TX_MBOX 5
//#define SIMPLE_CAN_SEND_MSGID config.tool_info.simple_can_rx_msg_id+1
//#define SIMPLE_CAN_RECV_MSGID config.tool_info.simple_can_rx_msg_id

//#define AZI_GAMMA_BROADCAST_MBOX 7
typedef enum {
    ECANA_SUCCESS,
    ECANA_TIMEOUT,
    ECANA_CRC_FAILURE,
    ECANA_NACK_RECIEVED,
    ECANA_INCOMPLETE_DATA,
    ECANA_INVALID_DATA,
    ECANA_NO_FRAME_ENTRY
} EcanaError;


extern union CliMsg recvFrameId;
extern Uint16 recvFrameRTR;
extern Uint16 recvFrameSize;
extern Uint16 recvTotalSize;
extern ECANA_STR ecana_str;
extern char ecana_pkg_buf[2000];
//extern em_circleq_simple_can_t simple_can_cmd_cq;
//extern Uint16 simple_can_cmd_queue[SIMPLE_CAN_QUEUE_SIZE];
//extern Uint64 simple_can_data_in;
//extern Uint64 simple_can_data_out;

extern Uint16 ecana_pkg_idx;
extern Uint16 ecana_start_multiframe_flag;
extern cli_frame_data_t recv_frame;
extern em_circleq_recv_frame_t recv_frame_cq;
extern Uint16 streaming;
extern Uint16 loading_tx_lock;

void msg_id_broadcast(Uint16 priority, Uint16 msg_id);
Uint16 wait_mbox_empty(Uint16 mbox, Uint32 timeout_ms);
Uint16 wait_ack_done(Uint16 frame_id, Uint32 timeout_ms, Uint16 * result);
Uint16 wait_response(Uint16 tool_type, Uint16 frame_id, Uint32 timeout_ms);
Uint16 wait_cli_clear();
Uint16 wait_mframe_done(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following);
Uint16 wait_mframe_done_no_response(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following, Uint16 yes_tx_data);
Uint16 wait_sframe_done(Uint32 timeout_ms);
//void _simple_can_enqueue(Uint16 len, Uint32 msgh, Uint32 msgl);
void _ecana_fill_ordered_buf_mframe(cli_frame_data_t frame, Uint32 msgh, Uint32 msgl);
void ecana_swap32(Uint32 *sw32);
void ecana_isr_100_service(void);
void ecana_isr_101_service(void);
void ecana_isr_tx_complete();

void _cana_rx_interrupt(Uint16, Uint32, Uint32);
void ecana_pwr_down(void);
void ecana_init(Uint32 bus_freq);
void ecana_init_rx_callback(Uint16 mailbox_num, void (*rx_callback)(Uint16 len, Uint32 msgh, Uint32 msgl));
//void ecana_init_rx_callback(Uint16 mailbox_num, Uint16 tool_type, Uint16 priority, Uint32 msg_id, Uint16 serial, Uint16 resvd, void (*rx_callback)(Uint16 len, Uint32 msgh, Uint32 msgl));
//void ecana_init_rx_callback(Uint16 mailbox_num, Uint16 tool_type, Uint16 priority, Uint32 msg_id, void (*rx_callback)(Uint16 len, Uint32 msgh, Uint32 msgl));
void ecana_init_tx_callback(Uint16 mailbox_num, void (*tx_callback)(Uint16 *len, Uint32 *msgh, Uint32 *msgl));
void ecana_send(Uint16 mailbox_num, Uint16 msg_id, Uint16 msg_len, Uint32 msgh, Uint32 msgl);
void ecana_send_wo_delay(Uint16 mailbox_num, Uint16 msg_id, Uint16 msg_len, Uint32 msgh, Uint32 msgl);
Uint16 ecana_pkg_char(char value);
Uint16 ecana_pkg_string(char *outstring, Uint16 len);
Uint16 ecana_pkg_string_from_word_buf(Uint16 *outstring, Uint16 start_pos, Uint16 len);
Uint16 ecana_pkg_uint8(Uint16 value);
Uint16 ecana_pkg_int16(int16 value);
Uint16 ecana_pkg_uint16(Uint16 value);
Uint16 ecana_pkg_int32(int32 value);
Uint16 ecana_pkg_uint32(Uint32 value);
Uint16 ecana_pkg_float32(float32 value);
Uint16 ecana_pkg_float64(float64 value);
Uint16 ecana_pkg_uint64(Uint64 value);
Uint16 ecana_pkg(Uint64 value, Uint16 length);
Uint16 ecana_tx_ack(Uint16 mailbox_num);
//void _ecana_calc_bit_rate(Uint32 clk_rate, Uint32 bus_freq, Uint16 *brp, Uint16 *tseg1, Uint16 *tseg2);
Uint16 ecan_rml();
Uint32 ecan_error();
void ecana_set_transframe_id(Uint16 priority, Uint16 msg_id, Uint16 tool_type, Uint16 resvd, Uint16 serial, Uint16 rtr);
void ecana_send_no_data(Uint16 mbox);
void ecana_clear_send_frame_buffer();

Uint16 multiframe_map_remove_frame(cli_frame_data_t * frame);
Uint16 multiframe_map_remove_frame_by_id(Uint16 frame_id);
Uint16 expected_frame_map_add(cli_frame_data_t * frame);
Uint16 expected_frame_map_remove(cli_frame_data_t * frame);

Uint16 ecana_rcv_frame_enqueue(cli_frame_data_t * frame);
Uint16 ecana_rcv_frame_dequeue(cli_frame_data_t * frame);
Uint16 ecana_rcv_frame_queue_is_empty();

void ecana_setup_mailbox_tx(Uint16 mbox_num, Uint32 msg_id, Uint16 priority);
void ecana_setup_mailbox_rx(Uint16 mbox_num, Uint32 msg_id);
void ecana_transmit(Uint16 mbox_num, Uint16 msg_len, Uint32 msg_l, Uint32 msg_h);

//extern Uint16 dbg_frame_since;
//extern Uint16 dbg_single_frame_since;
//extern Uint16 dbg_multi_frame_since;
//extern Uint16 dbg_multi_frame_since_2;
//extern Uint16 dbg_multi_frame_since_3;
//extern Uint16 dbg_single_byte_since;
//extern Uint16 dbg_multi_byte_since;
//extern Uint16 dbg_multi_byte_since_2;
//extern Uint16 dbg_multi_byte_since_3;


#endif /* ECANA_H_ */

/******************************************
 *******************************************
 **   END ERDOS MILLER'S BACKGROUND IP   ***
 *******************************************
 ******************************************/

