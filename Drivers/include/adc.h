/*
 * adc.h
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#ifndef ADC_H_
#define ADC_H_

#include <stdint.h>

#define ADC_ERR_FLAG_FIFO           (1<<0)
#define ADC_ERR_FLAG_STATE          (1<<1)
#define ADC_ERR_FLAG_SYNC1          (1<<2)
#define ADC_ERR_FLAG_SYNC2          (1<<3)
#define ADC_ERR_FLAG_UDMA           (1<<4)
#define ADC_ERR_FLAG_ADCCFG1        (1<<5)
#define ADC_ERR_FLAG_ADCCFG2        (1<<6)
#define ADC_ERR_FLAG_ADCCRC1        (1<<7)
#define ADC_ERR_FLAG_ADCCRC2        (1<<8)

//AD7124 read commands definitions
#define AD7124_READ_STATUS          0x40
#define AD7124_READ_CONTROL         0x41
#define AD7124_READ_DATA            0x42
#define AD7124_READ_IO_CONTROL_1    0x43
#define AD7124_READ_IO_CONTROL_2    0x44
#define AD7124_READ_ID              0x45
#define AD7124_READ_ERROR           0x46
#define AD7124_READ_ERROR_ENABLE    0x47
#define AD7124_READ_MCLK_COUNT      0x48
#define AD7124_READ_CHANNEL_0       0x49
#define AD7124_READ_CHANNEL_1       0x4A
#define AD7124_READ_CHANNEL_2       0x4B
#define AD7124_READ_CHANNEL_3       0x4C
#define AD7124_READ_CHANNEL_4       0x4D
#define AD7124_READ_CHANNEL_5       0x4E
#define AD7124_READ_CHANNEL_6       0x4F
#define AD7124_READ_CHANNEL_7       0x50
#define AD7124_READ_CHANNEL_8       0x51
#define AD7124_READ_CHANNEL_9       0x52
#define AD7124_READ_CHANNEL_10      0x53
#define AD7124_READ_CHANNEL_11      0x54
#define AD7124_READ_CHANNEL_12      0x55
#define AD7124_READ_CHANNEL_13      0x56
#define AD7124_READ_CHANNEL_14      0x57
#define AD7124_READ_CHANNEL_15      0x58

//AD7124 read commands definitions
//Note less than reads. Some registers are read only
#define AD7124_WRITE_CONTROL            0x01
#define AD7124_WRITE_IO_CONTROL_1       0x03
#define AD7124_WRITE_IO_CONTROL_2       0x04
#define AD7124_WRITE_ERROR_ENABLE       0x07
#define AD7124_WRITE_CHANNEL_0          0x09
#define AD7124_WRITE_CHANNEL_1          0x0A
#define AD7124_WRITE_CHANNEL_2          0x0B
#define AD7124_WRITE_CHANNEL_3          0x0C
#define AD7124_WRITE_CHANNEL_4          0x0D
#define AD7124_WRITE_CHANNEL_5          0x0E
#define AD7124_WRITE_CHANNEL_6          0x0F
#define AD7124_WRITE_CHANNEL_7          0x10
#define AD7124_WRITE_CHANNEL_8          0x11
#define AD7124_WRITE_CHANNEL_9          0x12
#define AD7124_WRITE_CHANNEL_10         0x13
#define AD7124_WRITE_CHANNEL_11         0x14
#define AD7124_WRITE_CHANNEL_12         0x15
#define AD7124_WRITE_CHANNEL_13         0x16
#define AD7124_WRITE_CHANNEL_14         0x17
#define AD7124_WRITE_CHANNEL_15         0x18

void adc_ssi0_hwi(void);
void adc_umda_err_hwi(void);

void _adc_dma_start_xfer(uint8_t *rx_buf);
void _adc_dma_init(void);
void adc_ssi_init(void);
void _adc_reset_blocking(void);
void _adc_ssi_clear_fifo(void);
void _adc_ssi_xfer_blocking(uint32_t ui32Base, uint32_t *buf, uint32_t len);
void _adc_ssi_rreg(uint32_t ui32Base, uint32_t addr, uint32_t *buf, uint32_t len);
void _adc_ssi_wreg(uint32_t ui32Base, uint32_t addr, uint32_t *buf, uint32_t len);
void _adc_reg_setup(uint32_t *buf);
void adc_init_ads131(void);
void adc_init_ad7124_8(void);

void ADCStart();
void ADCStop();

void StreamADCCan();

uint32_t _adc_check_crc(uint32_t data, uint32_t crc);
uint32_t _adc_data_check(uint32_t *val, uint8_t *buf, uint32_t num_sets);
void _adc_state_machine(void);

void adc_dready_hwi(void);
void adc_data_process_swi(void);

uint32_t adc_block_count(void);
uint32_t adc_block_read(uint8_t **buf, uint32_t *len);
void adc_block_pop(void);

#endif /* ADC_H_ */
