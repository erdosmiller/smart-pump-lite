/*
 * version.h
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#ifndef VERSION_H_
#define VERSION_H_


#define FW_MAJOR 2
#define FW_MINOR 5
#define FW_PATCH 4

#define HW_MAJOR 1
#define HW_MINOR 5

#endif /* VERSION_H_ */
