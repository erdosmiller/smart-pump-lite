/*
 * usb.h
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#ifndef USB_H_
#define USB_H_

#include "globals.h"

#include <stdbool.h>
#include <stdint.h>

#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "usblib/usblib.h"
#include "usblib/usb-ids.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdbulk.h"
#include "usb_bulk_structs.h"
#include "driverlib/sysctl.h"


#define USB_ERR_FLAG_CMD_UNKNOWN    (1<<0)
#define USB_ERR_FLAG_CMD_BAD_LEN    (1<<1)
#define USB_ERR_FLAG_CMD_OVRFLOW    (1<<2)
#define USB_ERR_FLAG_BUF_UNDERFLOW  (1<<3)
#define USB_ERR_FLAG_WRITE_INFO_LIM (1<<4)

extern volatile uint32_t g_ui32TxCount;
extern volatile uint32_t g_ui32RxCount;

extern bool eeprom_save_req_from_usb;
extern uint8_t eeprom_req_block_num;
extern char eeprom_usb_data[EEPROM_BYTES_PER_BLOCK];

void usb_init(void);
uint32_t usb_receive_cmd(tUSBDBulkDevice *psDevice, uint8_t *pi8Data, uint_fast32_t ui32NumBytes);
void usb_data_process_swi(void);

void service_usb_eeprom_write_request();

//ISR
uint32_t TxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
                   void *pvMsgData);
uint32_t RxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
                   void *pvMsgData);

#endif /* USB_H_ */
