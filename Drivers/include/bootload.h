/*
 * bootload.h
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#ifndef DRIVER_BOOTLOAD_H_
#define DRIVER_BOOTLOAD_H_

void JumpToBootLoader();

#endif /* DRIVER_BOOTLOAD_H_ */
