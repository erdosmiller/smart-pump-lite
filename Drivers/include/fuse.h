/*
 * fuse.h
 *
 *  Created on: June 14, 2021
 *      Author: Jack Baker
 */

#ifndef FUSE_H_
#define FUSE_H_

void init_fuses();
void fuse_ch1_enable();
void fuse_ch1_disable();
void fuse_ch2_enable();
void fuse_ch2_disable();

void get_fuse_reset_counts(uint8_t *fuse1, uint8_t *fuse2);

#endif /* FUSE_H_ */

