///
/// Erdos Miller CONFIDENTIAL
/// Unpublished Copyright (c) 2015 Erdos Miller, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Erdos Miller. The intellectual and technical concepts contained
/// herein are proprietary to Erdos Miller and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Erdos Miller.  Access to the source code contained herein is hereby forbidden to anyone except current Erdos Miller employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of Erdos Miller.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

/******************************************
 *******************************************
 **  BEGIN ERDOS MILLER'S BACKGROUND IP  ***
 *******************************************
 ******************************************/

#ifndef CLI_H
#define CLI_H

//#include "util.h"
#include "emlib.h"

#define CLI_TX_BUF_SIZE 2000
#define CLI_RX_BUF_SIZE 344

#define TOOL_TYPES_SIZE 32

#define TOOL_TYPE_DDM 3
#define TOOL_TYPE_MICROPULSE 2
#define TOOL_TYPE_ROTATIONAL_GAMMA 5
#define TOOL_TYPE_GAMMA_DUMB 6
#define TOOL_TYPE_SECONDARY_DM 4
#define TOOL_TYPE_COMPANION 9
#define TOOL_TYPE_ENHANCED_DM 11
#define TOOL_TYPE_EM_DAQ 20
#define TOOL_TYPE_BCAST 31

#define ROT_GAMMA_BUF_SIZE 8

#define CLI_BROADCAST_TIMEOUT 60000

#define CAN_YES_TX_DATA 1
#define CAN_NO_TX_DATA 0
#define CAN_DATA_FOLLOWING 1
#define CAN_NO_DATA_FOLLOWING 0

#define DEFAULT_TX_MBOX 25
#define DEFAULT_TX_TIMEOUT 10000
#define MIN_TX_TIMEOUT 5
#define BROADCAST_TX_MBOX 20
#define BCAST_AZG_MBOX 3

#define UKF_BCAST1_MBOX 10
#define UKF_BCAST2_MBOX 11
#define UKF_BCAST3_MBOX 12
#define UKF_BCAST4_MBOX 13
#define UKF_BCAST5_MBOX 14
#define UKF_BCAST_FRAME1_ID 0x570
#define UKF_BCAST_FRAME2_ID 0x571
#define UKF_BCAST_FRAME3_ID 0x572
#define UKF_BCAST_FRAME4_ID 0x573
#define UKF_BCAST_FRAME5_ID 0x574

#define WAIT_MFRAME_DEFAULT_TIMEOUT 5000
#define WAIT_RESPONSE_DEFAULT_TIMEOUT 5000

#define CLI_SIZE 256

#define FAT_FILENAME_SIZE 12

#define NOTIFY_COMPANIAN_FOR_DATA 12
#define GET_EXT_SURVEY_CMD 62

typedef enum {
    BCAST_AZG_DATA = 1,
    BCAST_TIME_QUERY = 2,
    BCAST_FLOW_EVAL_TIME_QUERY = 3,
    BCAST_SURVEY_DELAY_TIME_QUERY = 4,
} broadcast_msg_ids;

typedef enum {
    IDLE = 0,
    GOT_LEN = 1,
    GOT_CMD = 2,
    SEND_START = 3,
    SEND_WAIT = 4,
    CERROR = 255
} Cli_State;

typedef struct function_array {
    Uint16 length;
    void (*array[CLI_SIZE])(Uint16);
} function_array_t;

void cli_init();
void cli_reset();
void cli_execute();
void _cli_broadcast();
void _cli_broadcast_ticks();

void _cli_cmd_exec(Uint16 cmd);
char _cli_dequeue_rx();
void _cli_enqueue_rx(char c);
void _cli_enqueue_can(Uint16 len, Uint32 msgh, Uint32 msgl);
char _cli_dequeue_tx();
void _cli_dequeue_can(Uint16 *len, Uint32 *msgh, Uint32 *msgl);
void _cli_buff_to_can_msg_uint16(Uint16 *buf, Uint16 len, Uint32 *msgh, Uint32 *msgl);
void _cli_buff_to_can_msg(char *buf, Uint16 len, Uint32 *msgh, Uint32 *msgl);
Uint16 _cli_tx_buf_size();
Uint32 _cli_rx_ready();
void _cli_empty_queue();
void _cli_send_tx_data(Uint16 mbox);

char _cli_deserialize_byte();
Uint32 _cli_deserialize_uint32();
int32 _cli_deserialize_int32();
float _cli_deserialize_float();
Uint16 _cli_deserialize_bytes(char *buffer, int len);
Uint16 _cli_deserialize_string_w_uint16(Uint16 *buffer, int len);
Uint16 _cli_deserialize_string(char *buffer, int len);
Uint16 _cli_deserialize_string_to_word_buf(Uint16 *buffer, Uint16 start, int len);
Uint16 _cli_deserialize_uint16();
int16 _cli_deserialize_int16();
void _cli_serialize_uint16(Uint16 val);
void _cli_serialize_32(void *_val);
void _cli_serialize_uint32(Uint32 val);
void _cli_serialize_float(float val);
void _cli_serialize_int32(int32 val);
void _cli_serialize_string(char *out,Uint16 len);
void _cli_serialize_uint16(Uint16 val);
void _cli_serialize_int16(int16 val);
void _cli_load_tx(Uint16 priority);
void _cli_load_and_send(Uint16 priority, Uint16 mbox);
void _handle_end_of_stream(Uint16 nacks);
void clear_ordered_collected_buf();
void get_rot_gamma(Uint16 mbox);
int append_4k_to_file();

void _set_cli_frame(Uint16 priority, Uint16 msg_id);
void _cli_load_and_send(Uint16 priority, Uint16 mbox);
extern cli_frame_data_t cli_frame;

Uint16 cli_could_handle_frame(cli_frame_data_t * frame);
void save_existing_cli_frame_data(cli_frame_data_t *frame);
void overwrite_cli_frame(cli_frame_data_t *frame);

extern Uint16 cli_ordered_recieved;
extern char cli_rx_buf[CLI_RX_BUF_SIZE];
extern int cli_rx_idx;
extern int cli_read_idx;
extern int cli_rx_buf_size;
extern Uint16 cli_eeprom_busy;
extern Uint16 broadcast_data_available_to_moonraker_flag;
extern Uint16 cli_processing_cmd_flag;
extern Uint16 delay_following_frames;

extern Uint16 mp_can_in_use;
extern Uint16 mp_can_waiting_cli_caution;
extern Uint32 session_mode_start_time;
extern Uint16 streaming;
extern Uint16 THIS_TOOL_TYPE;
extern Uint16 use_extended_address_for_simple_can;

extern survey_t ext_survey;
extern float32 ext_survey_tf_offset;
extern Uint16 using_external_survey_tf_offset;
void get_external_survey(Uint16 mbox);
void reset_external_survey();

#define PULSER_FUNCTIONS_SIZE 1
#define MICROPULSE_FUNCTIONS_SIZE CLI_SIZE
#define ROTGAMMA_FUNCTIONS_SIZE 10

typedef enum {
    func1,
    func2,
    func3,
} _DBG_FUNCS;


#define STREAM_CHUNK_SIZE                    1790
#define DOWNLOAD_FLASH_READ_SIZE             56
#define DOWNLOAD_FLASH_SEND_WORDS_PER_BLOCK  8
#define DOWNLOAD_FLASH_SEND_BUFFERS_USED     16
#define DOWNLOAD_FLASH_SEND_BLOCKS_PER_CHUNK 16
#define DOWNLOAD_FLASH_STREAM_CHUNKS         14060
#define DOWNLOAD_FLASH_TOTAL_MEM_BYTES       0x1800000

/*
   CAN Frame Definition

   Priority    Tool Type    Message ID    Reserved    Serial Number
   2-bits        4-bits        9-bits        2-bits        12-bits

   Priority Definitions
   0    Bootload
   1    Stream / Multi-Frame
   2    Query / Set
   3    Broadcast

   Place all temporary debug commands under broadcast priority level.
*/
// TODO Handle RTR requests for gets

/*
    @func: get_clock_ticks
    @description: "Returns the number of milliseconds since the processor has powered up"
    @id: 0
    @priority: 3
    @rtr: 1
    @param: @void void
    @rtn: @uint32 ticks
    @retdesc: @ticks "Milliseconds since boot up."
    @dec: DictPrint()
*/
#define GET_CLOCK_TICKS 0
void get_clock_ticks(Uint16 mbox);

/*
    @func: get_device_unique_id
    @description: "Returns the 64-bit unique ID of the board"
    @id: 1
    @priority: 3
    @rtr: 1
    @param: @void void
    @rtn: @uint32 upper
    @retdesc: @upper "MSB 32 bits of the uid."
    @rtn: @uint32 lower
    @retdesc: @lower "LSB 32 bits of the uid."
    @dec: DictPrint()
*/
#define GET_DEVICE_UNIQUE_ID 1
void get_device_unique_id(Uint16 mbox);

/*
    @func: get_device_firmware_version
    @description: "Returns the firmware version as three uint8 values for MAJOR.MINOR.PATCH version number"
    @id: 2
    @priority: 3
    @rtr: 1
    @param: @void void
    @rtn: @uint8 major
    @retdesc: @major "Major Version Number."
    @rtn: @uint8 minor
    @retdesc: @minor "Minor Version Number."
    @rtn: @uint8 patch
    @retdesc: @patch "Patch Version Number."
    @rtn: @uint32 debug
    @retdesc: @debug "Debug version Number. (git commit hash)"
    @dec: DictPrint()
*/
#define GET_DEVICE_FIRMWARE_VERSION 2
void get_device_firmware_version(Uint16 mbox);

/*
    @func: get_device_hardware_version
    @description: "Returns the hardware version as MAJOR.MINOR version number"
    @id: 3
    @priority: 3
    @rtr: 1
    @param: @void void
    @rtn: @uint8 device_hardware_major
    @retdesc: @device_hardware_major "Major Hardware Version."
    @rtn: @uint8 device_hardware_minor
    @retdesc: @device_hardware_minor "Minro Hardware Version."
    @dec: DictPrint()
*/
#define GET_DEVICE_HARDWARE_VERSION 3
void get_device_hardware_version(Uint16 mbox);

/*
    @func: set_device_mode
    @description: "Sets the device mode"
    @id: 10
    @priority: 3
    @rtr: 1
    @param: @uint8 mode
    @rtn: @uint8 device_mode
    @retdesc: @device_mode "device mode set"
    @dec: DictPrint()
*/
#define SET_DEVICE_MODE 10
void set_device_mode(Uint16 mbox);

#define SET_ADC_GAIN 11
void set_adc_gain(Uint16 mbox);


//Broadcasts
#define _ADC_STREAM 100
void _adc_stream(char* b, Uint16 size);


// NEW_CLI_HELPER_TAG //

// #pragma DATA_SECTION(_pulser_functions, "pulserfuncs")
static const function_array_t _pulser_functions = { \
    PULSER_FUNCTIONS_SIZE, \
    { \
        NULL, \
    } \
};

//#pragma DATA_SECTION(_micropulse_functions, "micropulsefuncs")
static const function_array_t _micropulse_functions = { \
    MICROPULSE_FUNCTIONS_SIZE, \
    { \
// TOOL_FUNCTIONS_START
        &get_clock_ticks,                   // 0
        &get_device_unique_id,              // 1
        &get_device_firmware_version,       // 2
        &get_device_hardware_version,       // 3
        NULL,                               // 4
        NULL,                               // 5
        NULL,                               // 6
        NULL,                               // 7
        NULL,                               // 8
        NULL,                               // 9
        &set_device_mode,                   // 10
        &set_adc_gain,                      // 11
// TOOL_FUNCTIONS_END
    } \
};

// #pragma DATA_SECTION(_rot_gamma_functions, "rotgammafuncs")
//static const function_array_t _rot_gamma_functions = {\
//    ROTGAMMA_FUNCTIONS_SIZE, \
//    { \
//        NULL,NULL,&get_rot_gamma,NULL,\
//            NULL,NULL,NULL,NULL,\
//            NULL,NULL,\
//    } \
//};

static const function_array_t _generic_functions = {\
    1, \
    { \
        &get_clock_ticks,\
    } \
};

#endif

/******************************************
 *******************************************
 **   END ERDOS MILLER'S BACKGROUND IP   ***
 *******************************************
 ******************************************/



