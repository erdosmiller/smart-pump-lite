/*
 * pwm.h
 *
 *  Created on: Jul 25, 2021
 *      Author: Yifang.Yang
 */

#ifndef DRIVERS_INCLUDE_PWM_H_
#define DRIVERS_INCLUDE_PWM_H_

typedef enum Smart_Pump_PWMName {
    M0_PWM0 = 0,
    M0_PWM1,
    M0_PWM2,

    M0_PWM_PWMCOUNT
} Smart_Pump_PWMName;

void PWM_Init(void);

#endif /* DRIVERS_INCLUDE_PWM_H_ */
