/*
 * adc_internal.h
 *
 *  Created on: July 2, 2020
 *      Author: Nathan
 */

#ifndef ADC_INTERNAL_H_
#define ADC_INTERNAL_H_

void adc_internal_init();
void adc_internal_24vsns_update();

#endif /* ADC_INTERNAL_H_ */
