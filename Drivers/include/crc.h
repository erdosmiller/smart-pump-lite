/*
 * crc.h
 *
 *  Created on: September 10, 2020
 *      Author: Nathan
 */

/*
 * USB CRC INFORMATION
 * WIDTH           : 16-bits
 * POLYNOMIAL      : 0x8005
 * INITIAL VALUE   : 0x0000
 * FINAL Xor VALUE : 0x0000
 * INPUT REFLECTED : NO
 * RESULT REFLECTED: NO
 * CHECK VALUE     : 0xFEE8 for input string "123456789"
 */

#ifndef CRC_H_
#define CRC_H_

#define CRC_USB_POLY 0x8005

typedef uint16_t crc16_t;

#define WIDTH (8 * sizeof(crc16_t))
#define TOPBIT (1 << (WIDTH - 1))

crc16_t CRC16_USB_TABLE[256];
void crc16_usb_init();
crc16_t crc16_usb(uint8_t const message[], int nBytes);
crc16_t adc1_crc;
crc16_t adc2_crc;

#endif /* CRC_H_ */
