
///
/// Erdos Miller CONFIDENTIAL
/// Unpublished Copyright (c) 2015 Erdos Miller, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Erdos Miller. The intellectual and technical concepts contained
/// herein are proprietary to Erdos Miller and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Erdos Miller.  Access to the source code contained herein is hereby forbidden to anyone except current Erdos Miller employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of Erdos Miller.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

/******************************************
 *******************************************
 **  BEGIN ERDOS MILLER'S BACKGROUND IP  ***
 *******************************************
 ******************************************/

#ifndef EMLIB_H_
#define EMLIB_H_

#include <stdint.h>
#include <xdc/std.h>

typedef int                 int16;
typedef long                int32;
typedef long long           int64;
//typedef unsigned int        Uint16;
//typedef unsigned long       Uint32;
typedef unsigned long long  Uint64;
typedef float               float32;
typedef long double         float64;



#define DEFAULT_TIMEOUT_MS 5000
#define CENTI_SEC_TO_MILLI_SEC(VAL) (10*VAL)
#define MILLI_SEC_TO_CENTI_SEC(VAL) (VAL/10)
#define EYE {{1,0,0},{0,1,0},{0,0,1}}

typedef enum {
    EM_SERIALIZE_BYTE = 0,
    EM_SERIALIZE_UINT16 = 1,
    EM_SERIALIZE_INT16 = 2,
    EM_SERIALIZE_UINT32 = 3,
    EM_SERIALIZE_INT32 = 4,
    EM_SERIALIZE_FLOAT_SINGLE = 5,
    EM_SERIALIZE_FLOAT_DOUBLE = 6,
    EM_SERIALIZE_STRING = 7
} can_serialize_data_type_enum;

typedef struct {
    Uint16 serial:8;
    Uint16 reserved:5;
    Uint16 msg_id:8;
    Uint16 tool_type:5;
    Uint16 priority:3;
    Uint16 settings:3;
    Uint16 length:4;
    Uint16 mframe_length:9;
    Uint16 rtr:3;
} cli_frame_data_t;

union FloatorUint32
{
    float32 fl;
    Uint32 ui;
};

// Calibration structure
#define CAL_STRUCT_SIZE27 30
typedef struct {
    float32 cal[3][3];
    float32 offset[3];
    float32 diag[3];
} em_calibration27_t;
#define EM_CAL_DEFAULTS27 {        \
    {{1,0,0},{0,1,0},{0,0,1}}, \
    {0}, \
}

// Calibration structure
#define RADS_TO_DEGREES 57.29577951308232
#define THREE_X_THREE 0
#define ONE_X_THREE   1
#define DIAGONAL_MX   2
#define ONE_X_SIX   3
#define CAL_STRUCT_SIZE 120

#ifdef QDC_CALIBRATION
typedef struct {
	float32 scale[4][3];
	float32 bias[4][3];
	float32 misalignments[4][6];
	float32 diag[4][3];
} em_calibration_t;
#else
typedef struct {
    float32 cal[4][3][3];
    float32 xyz_offset[4][3];
    float32 diag[4][3];
} em_calibration_t;
#endif

#define EM_CAL_DEFAULTS {        \
    {0},             \
    {0},             \
    {0},             \
}

#define EM_CIRCLEQ_SIZE 16
#define EM_CIRCLEQ_FULL 1
#define EM_CIRCLEQ_EMPTY 2

typedef struct {
    float32 queue[EM_CIRCLEQ_SIZE];
    Uint16 size;
    Uint16 my_size;
    long head;
    long tail;
    float32 val;
    Uint16 err;
    Uint16 ow;
} em_circleq_float32_t;

#define EM_CIRCLEQ_F32_DEFAULTS {        \
    {0},             \
    0,             \
    EM_CIRCLEQ_SIZE,             \
    -1,             \
    -1,             \
    0,             \
    0,             \
    0,           \
}

#define EM_CIRCLEQ_U16_SIZE 256
typedef struct {
    Uint16 queue[EM_CIRCLEQ_U16_SIZE];
    Uint16 size;
    Uint16 my_size;
    long head;
    long tail;
    Uint16 val;
    Uint16 err;
    Uint16 ow;
} em_circleq_uint16_t;

#define EM_CIRCLEQ_U16_DEFAULTS {        \
    {0},             \
    0,             \
    EM_CIRCLEQ_U16_SIZE,             \
    -1,             \
    -1,             \
    0,             \
    0,             \
    0,           \
}

#define EM_CIRCLEQ_U32_SIZE 128
typedef struct {
    Uint32 queue[EM_CIRCLEQ_U32_SIZE];
    Uint16 size;
    Uint16 my_size;
    long head;
    long tail;
    Uint32 val;
    Uint16 err;
    Uint16 ow;
} em_circleq_uint32_t;

#define EM_CIRCLEQ_U32_DEFAULTS {        \
    {0},             \
    0,             \
    EM_CIRCLEQ_U32_SIZE,             \
    -1,             \
    -1,             \
    0,             \
    0,             \
    0,           \
}

typedef struct {
    cli_frame_data_t queue[EM_CIRCLEQ_SIZE];
    Uint16 size;
    Uint16 my_size;
    long head;
    long tail;
    cli_frame_data_t val;
    Uint16 err;
    Uint16 ow;
} em_circleq_recv_frame_t;
#define EM_CIRCLEQ_RECV_FRAME_DEFAULTS {        \
    {0},             \
    0,             \
    EM_CIRCLEQ_SIZE,             \
    -1,             \
    -1,             \
    0,             \
    0,             \
    0,           \
}

typedef struct {
    Uint16 queue[4];
    Uint16 size;
    Uint16 my_size;
    long head;
    long tail;
    Uint16 val;
    Uint16 err;
    Uint16 ow;
} em_circleq_simple_can_t;
#define EM_CIRCLEQ_SIMPLE_CAN_DEFAULTS {               \
    {0},                    \
    0,                      \
    4,                      \
    -1,                     \
    -1,                     \
    0,                      \
    0,                      \
    0,           \
}

typedef enum {
    //I have to put everything on mcbspa here to sequence them, not only files.
    file_job_read,
    file_job_append,
    file_job_append_always,
    file_job_erase,
    file_job_create,
    file_job_format,
    file_job_rewrite,
    file_job_lifetime_dump,
    spi_job_save_cfg_to_eeprom,
    spi_job_restore_cfg_from_eeprom,
    spi_job_save_sequence_to_eeprom,
    spi_job_load_sequence_from_eeprom,
    spi_job_read_temperature_on_spi,
    spi_job_update_life_time_tracking_data_in_eeprom,
    spi_job_save_accel_calibration,
    spi_job_save_mag_calibration,
    spi_job_flush_life_time_tracking_data_to_eeprom,
    file_job_get_free_space,
    file_job_get_root_entry,
    file_job_reintialize_wear_counter,
    file_job_reset_lifetime_tracking_data,
    file_job_get_file_size,
    raw_job_open_file,
    raw_job_read_more,
    raw_job_close_file,
    raw_job_file_lseek,
    raw_job_open_root_dir,
    raw_job_get_next_dir,
    raw_job_close_root_dir,
    raw_job_write_to_eeprom,
    raw_job_read_from_eeprom,
    raw_job_direct_read_from_flash,
} file_job_type_enum;

typedef enum {
    buffer_type_word,
    buffer_type_byte
} buffer_type_enum;


/*
 * @param Q   : struct-> Array to hold Queue values
 * @param VAL : value to insert into the array
 */
#define EM_CIRCLEQ_ENQUEUE( Q, VAL ) \
    do {if(((Q.head + 1) % Q.my_size) != Q.tail) { \
        Q.head = (++Q.head) % (Q.my_size); \
        Q.queue[Q.head] = (VAL); \
        Q.tail = ((Q.tail) < 0)?Q.head:(Q.tail); \
        Q.size++; \
        Q.err = 0; \
    } \
        else { \
            Q.head = (++Q.head) % (Q.my_size); \
            Q.queue[Q.head] = (VAL); \
            Q.tail = (++Q.tail) % (Q.my_size); \
            Q.err = 0; \
            Q.ow++; \
        }} while (0)
//else {Q.err = EM_CIRCLEQ_FULL; Q.ow++;} \

/*
 * @param Q   : struct-> Array to hold Queue values
 * @return Q.val : value to dequeued from the array
 */
#define EM_CIRCLEQ_DEQUEUE(Q) \
    do {if ((Q.tail != -1)&&(Q.size > 0)) { \
        Q.val = Q.queue[(Q.tail)]; \
        Q.tail = (Q.tail == Q.head)?(-1):(((Q.tail) + 1) % (Q.my_size)); \
        Q.size--; \
        if (Q.tail == -1) Q.size = 0; \
        Q.err = 0; \
    } \
        else {Q.err = EM_CIRCLEQ_EMPTY;} \
    } while (0)

#define EM_CIRCLEQ_ISEMPTY(Q) ((Q.tail == -1) || (Q.size <= 0))

#define EM_CIRCLEQ_GET_SIZE(Q) \
        (((Q.tail) < 0) ? 0 : (((Q.head) + (Q.my_size) + 1 - (Q.tail)) % (Q.my_size)))
// State machine

typedef struct state {
    void (*statefunc)();
    struct state *next_state;
    Uint16 quit;
} state_t;

#define EM_STATE_RUN(S) \
    do { \
        S.statefunc(); \
        S = S.next_state; \
    } while (!S.quit)



float32 apply_temperature_cal(em_calibration_t *cal_struct, Uint16 matrix, Uint16 idx, Uint16 idy, float32 temp);
Uint16 starts_with(const char *str, const char *pre);
Uint16 crc_16_update(Uint16 crc, Uint16 data);

#define SURVEY_STRUCT_SIZE 37 // Sizeof Uint16's
// tstamp = 5, rtc = 7, 19 tags
#define SURVEY_STRUCT_SIZE_BYTES 74 // Sizeof Uint16's
typedef struct survey {
    float32 inclination;
    float32 azimuth;
    float32 toolface_g;
    float32 toolface_m;
    float32 dipangle;
    float32 temperature;
    float32 magf;
    float32 grav;
    float32 cal_gx;
    float32 cal_gy;
    float32 cal_gz;
    float32 cal_gx2;
    float32 cal_gy2;
    float32 cal_gz2;
    float32 cal_mx;
    float32 cal_my;
    float32 cal_mz;
    float32 batv;
    Uint16 bat2;
    float32 raw_ax;
    float32 raw_ay;
    float32 raw_az;
    float32 raw_mx;
    float32 raw_my;
    float32 raw_mz;
} survey_t;

#ifdef AZI_GAMMA_BUILD
    #define PERIODICAL_GAMMA_STRUCT_SIZE 6
    #define PERIODICAL_GAMMA_STRUCT_SIZE_BYTES 12
#else
    #define PERIODICAL_GAMMA_STRUCT_SIZE 4
    #define PERIODICAL_GAMMA_STRUCT_SIZE_BYTES 8
#endif
typedef struct periodical_gamma_data_struct {
    float32 gamma_bulk_corrected;
#ifdef AZI_GAMMA_BUILD
	float32 gamma_up_corrected;
	float32 gamma_down_corrected;
#endif
} periodic_gamma_data;

#define PERIODICAL_VIBE_STRUCT_SIZE 13
#define PERIODICAL_VIBE_STRUCT_SIZE_BYTES 14 // Ditched xl, yl and zl.
typedef struct periodical_vibe_data_struct {
    float32 xh_accel;
    float32 yh_accel;
    float32 zh_accel;
//    float32 xl_accel;
//    float32 yl_accel;
//    float32 zl_accel;
    Uint16 flow;
} periodic_vibe_data;

#define PERIODICAL_VOLTAGE_STRUCT_SIZE 16
#define PERIODICAL_VOLTAGE_STRUCT_SIZE_BYTES 32
typedef struct periodical_voltage_data_struct {
    float32 bat1;
    float32 bat2;
    float32 bat3;
    float32 bus;
    float32 v5v0;
    float32 v3v3;
    float32 v1v8;
    float32 system_current;
} periodic_voltage_data;

#define PERIODICAL_TEMPERATURE_STRUCT_SIZE 4
#define PERIODICAL_TEMPERATURE_STRUCT_SIZE_BYTES 8
typedef struct periodical_temperature_data_struct {
    float32 temperature_c;
    float32 temperature_adc;
} periodic_temperature_data;

// 0 or false if out of range
#define VERIFY_IN_RANGE(VAL, LOW, UPP) ((VAL < LOW) || (VAL > UPP))?0:1;
// for use in boolean statements
#define IN_RANGE(VAL, LOW, UPP) !((VAL < LOW) || (VAL > UPP))

// Char to word array macros to be used with Uint16 arrays
// when indexing with single get/put use byte index (0-2*size)
// when indexing with 2 get/put use 
#define CH_GET(AR, IDX, VAL) do { \
    VAL = ((IDX) & 1)?AR[(IDX)>>1]&0xFF:(AR[(IDX)>>1] >> 8); \
    } while(0)

#define CH_PUT(AR, IDX, VAL) do { \
    AR[(IDX)>>1] = ((IDX) & 1)? (AR[(IDX)>>1] & 0xFF00) | ((VAL) & 0x00FF):((((Uint16)(VAL))<<8)&0xFF00) | (AR[(IDX)>>1] & 0x00FF); \
    } while(0)

#define CH_2GET(AR, IDX, VAL1, VAL2) do { \
    VAL1 = AR[IDX]>>8; \
    VAL2 = AR[IDX]&0xFF; \
} while(0)

#define CH_2PUT(AR, IDX, VAL1, VAL2) do { \
    AR[IDX] = ((((Uint16)VAL1)<<8)&0xFF00) | (VAL2 & 0x00FF); \
} while(0)

/* TODO create macros to us *ptr++ with this.
#define CH_GET(AR, IDX, VAL) do { \
VAL = (IDX & 1)?AR[IDX>>1]&0xFF:(AR[IDX>>1] >> 8); \
} while(0)

#define CH_PUT(AR, IDX, VAL) do { \
AR[IDX>>1] = (IDX & 1)? (AR[IDX>>1] & 0xFF00) | (VAL & 0x00FF):((((Uint16)VAL)<<8)&0xFF00) | (AR[IDX>>1] & 0x00FF); \
} while(0)

#define CH_2GET(AR, IDX, VAL1, VAL2) do { \
VAL1 = AR[IDX]>>8; \
VAL2 = AR[IDX]&0xFF; \
} while(0)

#define CH_2PUT(AR, IDX, VAL1, VAL2) do { \
AR[IDX] = ((((Uint16)VAL1)<<8)&0xFF00) | (VAL2 & 0x00FF); \
} while(0)
*/

#define CHAR_TO_UINT16_TOP_8(VAL) (((Uint16)VAL & 0xFF)<<8)
#define CHAR_TO_UINT16_BOT_8(VAL) (((Uint16)VAL & 0xFF))
#define DATA_TO_CHAR_NUM_SHIFT(VAL, SHIFT) ((VAL & (0xFF<<SHIFT))>>SHIFT)
#define UINT16_TO_CHAR_TOP_8(VAL) ((VAL & 0xFF00)>>8)
#define UINT16_TO_CHAR_BOT_8(VAL) (VAL & 0xFF)

// Safest...doesn't compile
//#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
    //#define min(a,b) \
        ({ __typeof__ (a) _a = (a); \
         __typeof__ (b) _b = (b); \
         _a < _b ? _a : _b; })
        // Second Safest....doesn't compile...
        //#define max(a,b) \
            ({ typeof (a) _a = (a); \
             typeof (b) _b = (b); \
             _a > _b ? _a : _b; })
            //#define min(a,b) \
                ({ typeof (a) _a = (a); \
                 typeof (b) _b = (b); \
                 _a < _b ? _a : _b; })
                // Most unsafe...compiles...
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define EQUALS(X,Y) ((X == Y))
                extern Uint16 crc_error_inject;

#define CAN_PRIORITY_REALTIME_SINGLE  0
#define CAN_PRIORITY_REALTIME_MULTI   1
#define CAN_PRIORITY_STREAM           2
#define CAN_PRIORITY_QUERY_SFRAME     3
#define CAN_PRIORITY_QUERY_MFRAME     4
#define CAN_PRIORITY_RESERVED         5
#define CAN_PRIORITY_BRCAST_SINGLE    6
#define CAN_PRIORITY_BRCAST_MULTI     7

#define CAN_RTR_SEND 0
#define CAN_RTR_GET  1

#define true 1
#define false 0

                void em_lib_reverse_string(char* str, int len);
                int em_lib_ltoa_space(long n, char* buf, int width, int forced_neg);
                int em_lib_ltoa_space_with_forced_sign(long n, char* buf, int width, int forced_neg);
                int em_lib_ultoa_space(unsigned long n, char* buf, int width);
                int em_lib_lltoa_space(long long n, char* buf, int width, int forced_neg);
                int em_lib_lltoa_space_with_forced_sign(long long n, char* buf, int width, int forced_neg);
                int em_lib_ulltoa_space(unsigned long long n, char* buf, int width);
                //Float to string, with fixed number of digits after the point.
                int em_lib_ftoa_space_digit(float f, char* buf, int before_digit_space, int after_point_digits);
                int em_lib_ftoa_space_digit_with_forced_sign(float f, char* buf, int before_point_space, int after_point_digits);
                //Float to string, with fixed number of overall digits.
                int em_lib_ftoa_digit(float f, char* buf, int significant_digits);
                //Float to string, in science format
                int em_lib_etoa_space_digit_digit(float f, char* buf, int before_digit_space, int after_point_digits, int ext_digits);
                int em_lib_ftoa_free_style(float f, char* buf);

                Uint16 em_lib_crc_16_update_by_byte(Uint16 crc, char data);
                Uint16 em_lib_crc_16_update_by_Uint16(Uint16 crc, Uint16 data);
                Uint16 em_lib_crc_16_update_by_Uint32(Uint16 crc, Uint32 data);
                Uint16 em_lib_crc_16_update_by_Uint64(Uint16 crc, Uint64 data);
                Uint16 em_lib_crc_16_update_by_float32(Uint16 crc, float32 data);
                Uint16 em_lib_crc_16_update_by_string(Uint16 crc, char * data);

                int em_lib_strlen_in_word_buf(Uint16 * buf);
                void em_lib_buf_byte_to_word(Uint16 * word_buf, int start_pos, char * byte_buf, int len);
                int em_lib_buf_string_to_word(Uint16 * word_buf, int start_pos, const char * string_buf);
                void em_lib_buf_word_to_word(Uint16 * dst_buf, const Uint16 dst_start_pos, Uint16 * src_buf, Uint16 src_start_pos, Uint16 len);
                int em_lib_buf_word_to_word_string(Uint16 * dst_buf, Uint16 dst_start_pos, const Uint16 * src_buf, Uint16 src_start_pos);
                void em_lib_buf_word_to_byte(char * dst_buf, const Uint16 dst_start_pos, Uint16 * src_buf, Uint16 src_start_pos, Uint16 len);
                int em_lib_buf_word_to_string(char * dst_buf, Uint16 dst_start_pos, const Uint16 * src_buf, Uint16 src_start_pos);
                int em_lib_buf_string_to_byte(char* byte_buf, int start_pos, const char * string_buf);

                unsigned char em_lib_load_byte_from_word_buffer(unsigned int * buf, unsigned int offset);
                unsigned int em_lib_load_word_from_word_buffer(unsigned int * buf, unsigned int offset);
                unsigned long em_lib_load_dword_from_word_buffer(unsigned int * buf, unsigned int offset);
                void em_lib_set_byte_to_word_buffer(unsigned int * buf, unsigned int offset, unsigned char val);
                void em_lib_set_word_to_word_buffer(unsigned int * buf, unsigned int offset, unsigned int val);
                void em_lib_set_dword_to_word_buffer(unsigned int * buf, unsigned int offset, unsigned long val);

                void swap32(char* buf);

#endif

                /******************************************
                 *******************************************
                 **   END ERDOS MILLER'S BACKGROUND IP   ***
                 *******************************************
                 ******************************************/

