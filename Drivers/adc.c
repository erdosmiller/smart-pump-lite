/*
 * adc.c
 *
 *  Created on: Apr 2, 2019
 *      Author: jamison
 */

#include <xdc/cfg/global.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Swi.h>

#include "adc.h"

#include "cli.h"
#include "globals.h"

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ssi.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "driverlib/udma.h"



#if defined(ewarm)
#pragma data_alignment=1024
uint8_t pui8ControlTable[1024];
#elif defined(ccs)
#pragma DATA_ALIGN(pui8ControlTable, 1024)
uint8_t pui8ControlTable[1024];
#else
uint8_t pui8ControlTable[1024] __attribute__ ((aligned(1024)));
#endif


#define ADC_BLOCK_COUNT         64
#define ADC_BLOCK_SAMPLES       128
#define ADC_BLOCK_LEN           (16 + (2 * ADC_BLOCK_SAMPLES * 3) + 4) /* happens to be divisible by 4 */
#define ADC_BLOCK_IDX_HEAD      0
#define ADC_BLOCK_IDX_DATA      16
#define ADC_BLOCK_IDX_TAIL      (16 + (256 * 3))
#define ADC_DMA_BUF             16


typedef enum {
    ADC_STATE_IDLE,
    ADC_STATE_ADC1_READ,
    ADC_STATE_ADC2_READ,
} adc_state_t;


adc_state_t _adc_state;
uint32_t _adc_query_flags;
uint32_t _adc_read_flags;
uint32_t adc_err_flags;
uint32_t adc_err_state_count;
uint32_t adc_err_crc1_count;
uint32_t adc_err_crc2_count;
int32_t adc_err_sync;
int32_t _adc_val_1;
int32_t _adc_val_2;
uint8_t adc_tx_buf[ADC_DMA_BUF]; // only read 15 bytes, leave one for end marker
uint8_t adc_rx_buf_1[ADC_DMA_BUF]; // only read 15 bytes, leave one for end marker
uint8_t adc_rx_buf_2[ADC_DMA_BUF]; // only read 15 bytes, leave one for end marker

#pragma DATA_ALIGN(_adc_block_buf, 4)
uint8_t _adc_block_buf[ADC_BLOCK_COUNT * ADC_BLOCK_LEN]; // aligned to an int32 to allow for 32-bit read/write
uint32_t _adc_block_idx;
uint32_t _adc_block_count;
uint32_t _adc_block_data_count;
uint32_t _adc_block_ready;
uint32_t _adc_block_read_idx;
uint32_t _adc_block_serial;


void adc_ssi0_hwi(void)
{
    uint32_t ui32Status;

    SSIDMADisable(SSI0_BASE, SSI_DMA_RX);
    SSIDMADisable(SSI0_BASE, SSI_DMA_TX);

    ui32Status = SSIIntStatus(SSI0_BASE, true);
    SSIIntClear(SSI0_BASE, ui32Status);

    _adc_state_machine();

    return;
}

void adc_umda_err_hwi(void)
{
    uint32_t ui32Status;
    ui32Status = uDMAErrorStatusGet();

    if(ui32Status) {
        uDMAErrorStatusClear();
        adc_err_flags = ADC_ERR_FLAG_UDMA;
    }
}

void _adc_dma_start_xfer(uint8_t *rx_buf) {
    if(!uDMAChannelIsEnabled(UDMA_CHANNEL_SSI0TX)) {
        _adc_ssi_clear_fifo();
        uDMAChannelTransferSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT, UDMA_MODE_BASIC, (void *)(SSI0_BASE + SSI_O_DR), (void *)rx_buf, 15); // 3x send 5 bytes of zeros
        uDMAChannelTransferSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT, UDMA_MODE_BASIC, (void *)adc_tx_buf, (void *)(SSI0_BASE + SSI_O_DR), 15); // 3x receive 1 status byte, 3 data bytes, 1 crc byte
        uDMAChannelEnable(UDMA_CHANNEL_SSI0RX);
        uDMAChannelEnable(UDMA_CHANNEL_SSI0TX);
        SSIDMAEnable(SSI0_BASE, SSI_DMA_RX | SSI_DMA_TX);   // GO!
    }
    return;
}

void _adc_dma_init(void)
{
    // Enable the uDMA peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);

    // Enable and set up the uDMA channel
    uDMAEnable();
    memset(pui8ControlTable, 0, sizeof(pui8ControlTable));
    uDMAControlBaseSet(pui8ControlTable);

    SSIDMADisable(SSI0_BASE, SSI_DMA_TX); // Disable while reconfiguring
    uDMAChannelAssign(UDMA_CH10_SSI0RX);
    uDMAChannelAssign(UDMA_CH11_SSI0TX);

    // Setup DMA channel for SSI0
    uDMAChannelAttributeDisable(UDMA_CHANNEL_SSI0RX, UDMA_ATTR_ALL);  //UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK);
    uDMAChannelAttributeEnable(UDMA_CHANNEL_SSI0RX, UDMA_ATTR_USEBURST);
    uDMAChannelControlSet(UDMA_CHANNEL_SSI0RX | UDMA_PRI_SELECT, UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 | UDMA_ARB_4);

    uDMAChannelAttributeDisable(UDMA_CHANNEL_SSI0TX, UDMA_ATTR_ALL);  //UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK);
    uDMAChannelAttributeEnable(UDMA_CHANNEL_SSI0TX, UDMA_ATTR_USEBURST);
    uDMAChannelControlSet(UDMA_CHANNEL_SSI0TX | UDMA_PRI_SELECT, UDMA_SIZE_8 | UDMA_SRC_INC_8 | UDMA_DST_INC_NONE | UDMA_ARB_4);

    SSIIntDisable(SSI0_BASE, SSI_TXEOT | SSI_DMATX | SSI_DMARX | SSI_TXFF | SSI_RXFF | SSI_RXTO | SSI_RXOR);
    SSIIntEnable(SSI0_BASE, SSI_DMARX);

    IntEnable(INT_UDMAERR);
    IntEnable(INT_SSI0);

    return;
}

void adc_ssi_init(void)
{
    // Setup Pinouts for SSI peripheral 0
    // SSI0 interfaces with 4 AD7124-8 ADCs and thus has 4 CS and DRDY lines
    GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_1);    // RTD ADC nSYNC pin
    GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_0);    // RTD1 ADC nCS pin
    GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_1);    // RTD2 ADC nCS pin
    GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_2);    // RTD3 ADC nCS pin
    GPIOPinTypeGPIOOutput(GPIO_PORTH_BASE, GPIO_PIN_3);    // RTD4 ADC nCS pin
    GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_0);    // RTD1 ADC DRDY pin
    GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_1);    // RTD2 ADC DRDY pin
    GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_2);    // RTD3 ADC DRDY pin
    GPIOPinTypeGPIOOutput(GPIO_PORTP_BASE, GPIO_PIN_3);    // RTD4 ADC DRDY pin

    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);    // nCS disable all ADC for RTD sensor


    // Setup SSI0 port for SPI communication to ADCs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK); // SPI clock
    GPIOPinConfigure(GPIO_PA4_SSI0XDAT0); // SPI MOSI
    GPIOPinConfigure(GPIO_PA5_SSI0XDAT1); // SPI MISO
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_4 | GPIO_PIN_5 );

    // Setup and enable SSI0 port
    // Note that SSI0 port communicates with RTD sensors through the ADC AD7124-8. This ADC uses SPI frame mode 3
    SSIConfigSetExpClk(SSI0_BASE, ui32SysClock, SSI_FRF_MOTO_MODE_3, SSI_MODE_MASTER, 2500000, 8);
    SSIEnable(SSI0_BASE);

    // Setup Pinouts for SSI peripheral 1
    GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_0);    // mA ADC nSYNC pin
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_2);    // mA ADC nCS pin
    GPIOPinTypeGPIOOutput(GPIO_PORTQ_BASE, GPIO_PIN_1);    // mA ADC DRDY pin

    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PIN_2);    // nCS disable ADC for mA sensor



    // Setup SSI1 port for SPI communication to ADCs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);
    GPIOPinConfigure(GPIO_PB5_SSI1CLK); // SPI clock (for both ADCs)
    GPIOPinConfigure(GPIO_PE4_SSI1XDAT0); // SPI MOSI (for both ADCs)
    GPIOPinConfigure(GPIO_PE5_SSI1XDAT1); // SPI MISO (for both ADCs)
    GPIOPinTypeSSI(GPIO_PORTB_BASE, GPIO_PIN_5 );
    GPIOPinTypeSSI(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5 );

    // Setup and enable SSI1 port
    // Note that SSI1 port communicates with mA sensors through the ADC ADS131M08. This ADC uses SPI frame mode 1
    SSIConfigSetExpClk(SSI1_BASE, ui32SysClock, SSI_FRF_MOTO_MODE_1, SSI_MODE_MASTER, 2500000, 8);
    SSIEnable(SSI1_BASE);

    // Setup Pinouts for SSI peripheral 3
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_6);    // Accel ADC nSYNC pin
    GPIOPinTypeGPIOOutput(GPIO_PORTK_BASE, GPIO_PIN_6);    // Accel ADC nCS pin
    GPIOPinTypeGPIOOutput(GPIO_PORTQ_BASE, GPIO_PIN_4);    // Accel ADC DRDY pin

    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, GPIO_PIN_6);    // nCS disable ADC for Accel sensor


    // Setup SSI3 port for SPI communication to ADCs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);
    GPIOPinConfigure(GPIO_PQ0_SSI3CLK); // SPI clock
    GPIOPinConfigure(GPIO_PQ2_SSI3XDAT0); // SPI MOSI
    GPIOPinConfigure(GPIO_PQ3_SSI3XDAT1); // SPI MISO
    GPIOPinTypeSSI(GPIO_PORTQ_BASE, GPIO_PIN_0 | GPIO_PIN_2 | GPIO_PIN_3 );

    // Setup and enable SSI3 port
    // Note that SSI3 port communicates with Accel sensors through the ADC ADS131M08. This ADC uses SPI frame mode 1
    SSIConfigSetExpClk(SSI3_BASE, ui32SysClock, SSI_FRF_MOTO_MODE_1, SSI_MODE_MASTER, 2500000, 8);
    SSIEnable(SSI3_BASE);

    return;
}

void _adc_reset_blocking(void)
{
    uint32_t delay;

    // Reset both ADCs at the same time
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_1 | GPIO_PIN_0, GPIO_PIN_1 | GPIO_PIN_0);
    for(delay = 0; delay < 1000000; delay++) {};
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_1 | GPIO_PIN_0, 0x00);
    for(delay = 0; delay < 1000000; delay++) {};
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_1 | GPIO_PIN_0, GPIO_PIN_1 | GPIO_PIN_0);
    for(delay = 0; delay < 1000000; delay++) {};

    return;
}

void _adc_ssi_clear_fifo(void) {
    // Empty the SSI0 RX FIFO, in case it's not empty
    int32_t tmp, count;

    count = 0;
    while(SSIDataGetNonBlocking(SSI0_BASE, (uint32_t*)&tmp) != 0) {
        count++;
    };
    if(count > 0) {
        adc_err_flags = ADC_ERR_FLAG_FIFO;
    }
    return;
}

void _adc_ssi_xfer_blocking(uint32_t ui32Base, uint32_t *buf, uint32_t len)
{
    int32_t ii;

    for(ii = 0; ii < len; ii++) {
        SSIDataPut(ui32Base, buf[ii]);
        SSIDataGet(ui32Base, &(buf[ii]));
    }

    return;
}

void _adc7124_ssi_rreg(uint32_t ui32Base, uint32_t addr, uint32_t *buf, uint32_t len)
{
    uint32_t reg_buf[2];
    reg_buf[0] = (1<<5) | (0x01F & addr);
    reg_buf[1] = len - 1;

    _adc_ssi_xfer_blocking(ui32Base, reg_buf, 2);
    _adc_ssi_xfer_blocking(ui32Base, buf, len);
    return;
}

void _adc_ssi_rreg(uint32_t ui32Base, uint32_t addr, uint32_t *buf, uint32_t len)
{
    uint32_t reg_buf[2];
    reg_buf[0] = (1<<5) | (0x01F & addr);
    reg_buf[1] = len - 1;

    _adc_ssi_xfer_blocking(ui32Base, reg_buf, 2);
    _adc_ssi_xfer_blocking(ui32Base, buf, len);
    return;
}

void _adc_ssi_wreg(uint32_t ui32Base, uint32_t addr, uint32_t *buf, uint32_t len)
{
    uint32_t reg_buf[2];
    reg_buf[0] = (1<<6) | (0x01F & addr);
    reg_buf[1] = len - 1;

    _adc_ssi_xfer_blocking(ui32Base, reg_buf, 2);
    _adc_ssi_xfer_blocking(ui32Base, buf, len);
    return;
}

void _adc_ads131_reg_setup(uint32_t *buf)
{
    // ADC register setup, see datasheet page 72
    buf[0] = 0x00; // clear POR flag
    buf[1] = (0<<4) /* MUXP */ | (1<<0) /* MUXN */; // MUXP = AIN0, MUXN = AIN1 (default)
    buf[2] = (0<<5) /* DELAY */ | (0<<3) /* PGA_EN */ | (0<<0) /* GAIN */; // no chop, PGA disabled, GAIN = 1 (default)
    buf[3] = (0<<7) /* G_CHOP */ | (1<<6) /* CLK */ | (0<<5) /* MODE */ | (1<<4) /* FILTER */ | (0xB<<0) /* DR */; // CLK = external, DR = 1kHz
    buf[4] = (0<<6) /* FL_REF_EN */ | (0<<5) /* nREFP_BUF */ | (1<<4) /* nREFN_BUF */ | (0<<2) /* REFSEL */ | (0<<0) /* REFCON */; // default
    buf[5] = (0<<7) /* FL_RAIL_EN */ | (0<<6) /* PSW */ | (0<<0) /* IMAG */; // default
    buf[6] = (0xF<<4) /* I2MUX */ | (0xF<<0) /* I1MUX */; // default
    buf[7] = (0<<7) /* VB_LEVEL */ | (0<<6) /* VB_AINC */ | (0<<5) /* VB_AIN5 */ | (0<<4) /* VB_AIN4 */
             | (0<<3) /* VB_AIN3 */ | (0<<2) /* VB_AIN2 */ | (0<<1) /* VB_AIN1 */ | (0<<0) /* VB_AIN0 */; // default
    buf[8] = (0<<5) /* SYS_MON */ | (2<<3) /* CAL_SAMP */ | (0<<2) /* TIMEOUT */ | (1<<1) /* CRC */ | (1<<0) /* SENDSTAT */; // enable CRC, enable STAT
    buf[9] = 0x00; // OFCAL0 (default)
    buf[10] = 0x00; // OFCAL1 (default)
    buf[11] = 0x00; // OFCAL2 (default)
    buf[12] = 0x00; // FSCAL0 (default)
    buf[13] = 0x00; // FSCAL1 (default)
    buf[14] = 0x40; // FSCAL2 (default)
    buf[15] = 0x00; // GPIODAT (default)
    buf[16] = 0x00; // GPIOCON (default)
    return;
}

void _adc_ad7124_reg_setup(uint32_t *buf)
{
    // ADC register setup, see datasheet page 72
    buf[0] = AD7124_WRITE_CONTROL; // clear POR flag
    buf[1] = 0x1E;
    buf[2] = 0x43;
    buf[3] = AD7124_WRITE_IO_CONTROL_1;
    buf[4] = 0x00;
    buf[5] = 0x24;
    buf[6] = 0x01;
    buf[7] = AD7124_WRITE_CHANNEL_0;
    buf[8] = 0x80;
    buf[9] = 0x43;
    buf[10] = AD7124_WRITE_CHANNEL_1;
    buf[11] = 0x80;
    buf[12] = 0x85;
    buf[13] = AD7124_WRITE_CHANNEL_2;
    buf[14] = 0x81;
    buf[15] = 0x09;
    buf[16] = AD7124_WRITE_CHANNEL_3;
    buf[17] = 0x81;
    buf[18] = 0x8D;

    return;
}

void adc_init_ad7124_8(void)
{
    uint32_t reg_adc_setup[20];

    _adc_ad7124_reg_setup(reg_adc_setup);

    // Write to RTD ADC1
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_0, 0); // select ADC1
    _adc_ssi_wreg(SSI0_BASE, 0x01, reg_adc_setup, 19);
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_0, GPIO_PIN_0); // de-select ADC1

    // Write to RTD ADC1
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_1, 0); // select ADC2
    _adc_ssi_wreg(SSI0_BASE, 0x01, reg_adc_setup, 19);
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_1, GPIO_PIN_1); // de-select ADC2

    // Write to RTD ADC1
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_2, 0); // select ADC3
    _adc_ssi_wreg(SSI0_BASE, 0x01, reg_adc_setup, 19);
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_2, GPIO_PIN_2); // de-select ADC3

    // Write to RTD ADC1
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_3, 0); // select ADC4
    _adc_ssi_wreg(SSI0_BASE, 0x01, reg_adc_setup, 19);
    GPIOPinWrite(GPIO_PORTH_BASE, GPIO_PIN_3, GPIO_PIN_3); // de-select ADC4
}

void adc_init_ads131(void)
{
    int32_t ii;
    uint32_t reg_buf_adc1[18];
    uint32_t reg_buf_adc2[18];
    uint32_t reg_adc_setup[17];


    _adc_state = ADC_STATE_IDLE;
    _adc_query_flags = 0;
    _adc_read_flags = 0;
    adc_err_flags = 0;
    adc_err_state_count = 0;
    adc_err_crc1_count = 0;
    adc_err_crc2_count = 0;
    adc_err_sync = 0;
    _adc_block_idx = 0;
    _adc_block_count = 0;
    _adc_block_data_count = 0;
    _adc_block_ready = 0;
    _adc_block_read_idx = 0;
    _adc_block_serial = 0;

    // Initialize the first header block
    for(ii = 0; ii < sizeof(adc1_header); ii++) {
        _adc_block_buf[_adc_block_idx++] = adc1_header[ii];
    }

    //_adc_dma_init();
    _adc_reset_blocking();
    _adc_ssi_clear_fifo();

    // Read all mA ADC registers, should be default as per pg. 72 of datasheet
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, 0); // select mA
    _adc_ssi_rreg(SSI1_BASE, 0x00, reg_buf_adc1, 18);
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PIN_2); // de-select mA

    // Read all Accel ADC registers, should be default as per pg. 72 of datasheet
    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, 0); // select Accel
    _adc_ssi_rreg(SSI3_BASE, 0x00, reg_buf_adc2, 18);
    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, GPIO_PIN_6); // de-select Accel


    // Write all mA ADC registers, should be default as per pg. 72 of datasheet
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, 0); // select mA
    _adc_ads131_reg_setup(reg_adc_setup);
    _adc_ssi_wreg(SSI1_BASE, 0x01, reg_adc_setup, 17);
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PIN_2); // de-select mA

    // Write all Accel ADC registers, should be default as per pg. 72 of datasheet
    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, 0); // select Accel
    _adc_ads131_reg_setup(reg_adc_setup);
    _adc_ssi_wreg(SSI3_BASE, 0x01, reg_adc_setup, 17);
    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, GPIO_PIN_6); // de-select Accel


    // Read all ADC1 registers, should be default as per pg. 72 of datasheet
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, 0); // select mA
    _adc_ssi_rreg(SSI1_BASE, 0x00, reg_buf_adc1, 18);
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2, GPIO_PIN_2); // de-select mA

    // Read all ADC2 registers, should be default as per pg. 72 of datasheet
    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, 0); // select Accel
    _adc_ssi_rreg(SSI3_BASE, 0x00, reg_buf_adc2, 18);
    GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, GPIO_PIN_6); // de-select Accel

    return;
}

void ADCStart()
{
    return;
}

void ADCStop()
{
    return;
}

void StreamADCCan(){
//  uint16_t i = 0;
//  Uint16 packetIndex = 0;
//
//  adc1_can_data[packetIndex++] = adc1_block_index >> 24;
//  adc1_can_data[packetIndex++] = adc1_block_index >> 16;
//  adc1_can_data[packetIndex++] = adc1_block_index >> 8;
//  adc1_can_data[packetIndex++] = adc1_block_index;
//  ++adc1_block_index;
//
//  for(i = 0; i < ADC_SAMPLES; i++)
//  {
//    adc1_can_data[packetIndex++] = (send_adc1_data[i] & 0x00FF0000) >> 16;
//    adc1_can_data[packetIndex++] = (send_adc1_data[i] & 0x0000FF00) >> 8;
//    adc1_can_data[packetIndex++] = (send_adc1_data[i] & 0x000000FF);
//  }
//
//  _adc_stream(adc1_can_data, ADC1_CAN_DATA_SIZE);
//
//  adc1_final_data_index = 0;
//  new_adc1_data = 0;
}

uint32_t _adc_check_crc(uint32_t data, uint32_t crc)
{
    uint64_t num = data;
    uint64_t thr = 0x00000100; // threshold, num needs to be less than this
    uint64_t div; // 9-bit value
    int32_t msb_pos; // we shift a 32-bit unsigned int by 8 bits, it's MSB has to be in position 39 or less (indexed from 0)

    num <<= 8; // we'll compensate for this shift below

    // Run two iterations of a binary search to find about where the MSB is
    if(data & 0xFFFF0000) {
        if(data & 0xFF000000) {
            msb_pos = 31 + 8;
        } else {
            msb_pos = 23 + 8;
        }
    } else {
        if(data & 0x0000FF00) {
            msb_pos = 15 + 8;
        } else {
            msb_pos = 7 + 8;
        }
    }

    // Calculate CRC
    while(num >= thr) {
        while((num & ((uint64_t)1<<msb_pos)) == 0) {
            msb_pos--;
        }
        div = 0x0107;
        div <<= (msb_pos - 8);
        num ^= div;
    }
    return num - crc; // should equal zero if all is well
}

uint32_t _adc_data_check(uint32_t *val, uint8_t *buf, uint32_t num_sets)
{
    uint32_t crc;
    uint32_t tmp;
    uint32_t ofst = 0;

    do {
        tmp = (buf[ofst + 0]<<24) | (buf[ofst + 1]<<16) | (buf[ofst + 2]<<8) | (buf[ofst + 3]);
        crc = _adc_check_crc(tmp, buf[ofst + 4]);
        ofst += 5;
    } while((crc != 0) && (ofst < ((2 * 5) + 1))); // each block is 5-bytes, stop with at the third block

    (*val) = tmp; // assign a good (or the last) value, including STATUS byte

    return crc;
}

void _adc_state_machine(void)
{
    switch(_adc_state) {
    case ADC_STATE_IDLE:
        if(_adc_read_flags > 0) {
            adc_err_flags |= ADC_ERR_FLAG_SYNC1;
        }

        if(_adc_query_flags & (1<<0)) {
            _adc_query_flags &= ~(1<<0);
            _adc_state = ADC_STATE_ADC1_READ;

            // Start ADC1 read
            GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, 0); // select ADC1
            _adc_dma_start_xfer(adc_rx_buf_1);
        } else if(_adc_query_flags & (1<<1)) {
            _adc_query_flags &= ~(1<<1);
            _adc_state = ADC_STATE_ADC2_READ;

            // Start ADC2 read
            GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_7, 0); // select ADC2
            _adc_dma_start_xfer(adc_rx_buf_2);
        }
        break;
    case ADC_STATE_ADC1_READ:
        GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, GPIO_PIN_6); // de-select ADC1
        _adc_read_flags |= (1<<0);
        adc_err_sync++;

        if(_adc_query_flags & (1<<1)) {
            _adc_query_flags &= ~(1<<1);
            _adc_state = ADC_STATE_ADC2_READ;

            GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_7, 0); // select ADC2
            _adc_dma_start_xfer(adc_rx_buf_2);
        } else {
            _adc_state = ADC_STATE_IDLE;
            Swi_post(adc_data_process_swi_hndl);
        }
        break;
    case ADC_STATE_ADC2_READ:
        GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_7, GPIO_PIN_7); // de-select ADC2
        _adc_read_flags |= (1<<1);
        adc_err_sync--;

        if(_adc_query_flags & (1<<0)) {
            _adc_query_flags &= ~(1<<0);
            _adc_state = ADC_STATE_ADC1_READ;

            GPIOPinWrite(GPIO_PORTK_BASE, GPIO_PIN_6, 0); // select ADC1
            _adc_dma_start_xfer(adc_rx_buf_1);
        } else {
            _adc_state = ADC_STATE_IDLE;
            Swi_post(adc_data_process_swi_hndl);
        }
        break;
    default:
        break;
    }
    return;
}

void adc_dready_hwi(void)
{
    uint32_t int_pins = 0;
    int_pins = GPIOIntStatus(GPIO_PORTN_BASE, 1);


    if(int_pins & GPIO_INT_PIN_0)
    {
        GPIOIntClear(GPIO_PORTN_BASE, GPIO_INT_PIN_0);
        _adc_query_flags |= (1<<0);
    }

    if(int_pins & GPIO_INT_PIN_1)
    {
        GPIOIntClear(GPIO_PORTN_BASE, GPIO_INT_PIN_1);
        _adc_query_flags |= (1<<1);
    }

    if(_adc_state == ADC_STATE_IDLE) {
        _adc_state_machine();
    } else {
        adc_err_flags |= ADC_ERR_FLAG_STATE;
        adc_err_state_count++; // ERROR: we should have finished all transactions by now
    }

    return;
}

void adc_data_process_swi(void)
{
    uint32_t tmp;
    uint32_t tmp_crc;
    uint32_t tmp_val;

    // Check ADC1 data CRC
    tmp_crc = _adc_data_check(&tmp_val, adc_rx_buf_1, 3);
    if(tmp_crc != 0) {
        adc_err_crc1_count++;
        adc_err_flags |= ADC_ERR_FLAG_ADCCRC1;
    } else {
        _adc_val_1 = tmp_val;
    }

    // Check ADC2 data CRC
    tmp_crc = _adc_data_check(&tmp_val, adc_rx_buf_2, 3);
    if(tmp_crc != 0) {
        adc_err_crc1_count++;
        adc_err_flags |= ADC_ERR_FLAG_ADCCRC1;
    } else {
        _adc_val_2 = tmp_val;
    }

    // Check for synchronization errors between the two ADCs (i.e. if one is running faster than the other)
    if(adc_err_sync != 0) {
        adc_err_flags |= ADC_ERR_FLAG_SYNC2;
    }

    _adc_block_buf[_adc_block_idx++] = (0x00FF0000 & _adc_val_1)>>16;
    _adc_block_buf[_adc_block_idx++] = (0x0000FF00 & _adc_val_1)>>8;
    _adc_block_buf[_adc_block_idx++] = (0x000000FF & _adc_val_1)>>0;

    _adc_block_buf[_adc_block_idx++] = (0x00FF0000 & _adc_val_2)>>16;
    _adc_block_buf[_adc_block_idx++] = (0x0000FF00 & _adc_val_2)>>8;
    _adc_block_buf[_adc_block_idx++] = (0x000000FF & _adc_val_2)>>0;


    //
    // WARNING: !!! priority of this SWI should be HIGHER than USB SWI !!!
    // otherwise a race condition might be introduced into block count calculations
    //


    // Handle the End-of-Block transition
    _adc_block_data_count++;
    if(_adc_block_data_count >= ADC_BLOCK_SAMPLES) {
        _adc_block_data_count = 0;

        // Write the block tail
        _adc_block_buf[_adc_block_idx++] = 0; // CRC high byte placeholder
        _adc_block_buf[_adc_block_idx++] = 0; // CRC low byte placeholder
        _adc_block_buf[_adc_block_idx++] = 0x03; // tail marker 1
        _adc_block_buf[_adc_block_idx++] = 0x04; // tail marker 2

        // Increment the block counter, reset to 0 as necessary
        _adc_block_count++;
        if(_adc_block_count >= ADC_BLOCK_COUNT) {
            _adc_block_count = 0;
        }

        // Increment the count of blocks ready to be read
        _adc_block_ready++;
        if(_adc_block_ready >= (ADC_BLOCK_COUNT - 1)) {
            _adc_block_ready = (ADC_BLOCK_COUNT - 1); // one block is always being written

            tmp = _adc_block_count + (ADC_BLOCK_COUNT - _adc_block_ready);
            tmp = (tmp >= ADC_BLOCK_COUNT) ? (tmp - ADC_BLOCK_COUNT) : tmp;
            _adc_block_read_idx = tmp * ADC_BLOCK_LEN;
        }


        // Update the block index
        _adc_block_idx = (_adc_block_count * ADC_BLOCK_LEN);

        // Copy the next header block
        for(tmp = 0; tmp < sizeof(adc1_header); tmp++) {
            _adc_block_buf[_adc_block_idx++] = adc1_header[tmp];
        }

        // Update the block serial number
        _adc_block_serial++;
        _adc_block_buf[_adc_block_idx - 6] = (0xFF000000 & _adc_block_serial) >> 24;
        _adc_block_buf[_adc_block_idx - 5] = (0x00FF0000 & _adc_block_serial) >> 16;
        _adc_block_buf[_adc_block_idx - 4] = (0x0000FF00 & _adc_block_serial) >> 8;
        _adc_block_buf[_adc_block_idx - 3] = (0x000000FF & _adc_block_serial) >> 0;

    }

    _adc_read_flags = 0;

    return;
}

uint32_t adc_block_count(void)
{
    return _adc_block_ready;
}

uint32_t adc_block_read(uint8_t **buf, uint32_t *len)
{
    // WARNING: 'buf' pointer might be stale, only ready if return value is > 0
    (*buf) = (uint8_t *)(_adc_block_buf + _adc_block_read_idx);
    (*len) = ADC_BLOCK_LEN;

    return _adc_block_ready;
}

void adc_block_pop(void)
{
    uint32_t tmp;
    if(_adc_block_ready > 0) {
        _adc_block_ready--;

        tmp = _adc_block_count + (ADC_BLOCK_COUNT - _adc_block_ready);
        tmp = (tmp >= ADC_BLOCK_COUNT) ? (tmp - ADC_BLOCK_COUNT) : tmp;
        _adc_block_read_idx = tmp * ADC_BLOCK_LEN;
    }
    return;
}

