
///
/// Erdos Miller CONFIDENTIAL
/// Unpublished Copyright (c) 2015 Erdos Miller, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Erdos Miller. The intellectual and technical concepts contained
/// herein are proprietary to Erdos Miller and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Erdos Miller.  Access to the source code contained herein is hereby forbidden to anyone except current Erdos Miller employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of Erdos Miller.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

/******************************************
 *******************************************
 **  BEGIN ERDOS MILLER'S BACKGROUND IP  ***
 *******************************************
 ******************************************/

/** \file ecana.c
 *  \ingroup Communication
 *  \ingroup CANProtocol
 *  \brief Low level CAN driver
 *
 */

/**
 * \defgroup Communication Communication Documentation
 * \brief Various protocols and commands Micropulse supports
 *
 */

/**
 * \defgroup CANProtocol CAN Bus Protocol
 * \ingroup Communication
 * \brief Micropulse main supported communication protocol
 *
 */
#include "ecana.h"
#include "cli.h"
#include "can.h"

#include "emlib.h"
#include <ti/sysbios/knl/Clock.h>
#include "string.h"

#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/cfg/global.h>
//#include "DSP2833x_Device.h"
//#include "DSP2833x_GlobalPrototypes.h" // For ServiceDog
//#include "DSP2833x_Examples.h"
//#include "data.h"
//#include "config.h"
//#include "manager.h"
//#include "util.h"


//#pragma CODE_SECTION(ecana_swap32, "ramfuncs");
//#pragma CODE_SECTION(ecana_isr_100_service, "ramfuncs");
//#pragma CODE_SECTION(ecana_isr_101_service, "ramfuncs");
////#pragma CODE_SECTION(ecana_send, "ramfuncs");
//#pragma CODE_SECTION(ecana_transmit, "ramfuncs");
//#pragma CODE_SECTION(ecana_rec_frame_enqueue, "ramfuncs");
//#pragma CODE_SECTION(_multiframe_map_register_frame_in_rx_queue, "ramfuncs");
//#pragma CODE_SECTION(_get_multiframe_from_map, "ramfuncs");

#define EM_CAN_RAW_RAME_CIRCLEQ_SIZE 6

typedef struct {
    Uint32 MDL;
    Uint32 MDH;
    Uint32 MSGID;
    Uint16 mailbox_num :5;
    Uint16 DLC:4;
    Uint16 RTR:1;
} raw_frame_data_t;

typedef struct {
	raw_frame_data_t queue[EM_CAN_RAW_RAME_CIRCLEQ_SIZE];
    Uint16 size;
    Uint16 my_size;
    int head;
    int tail;
    Uint16 err;
} em_circleq_raw_frame_t;
#define EM_CIRCLEQ_RAW_FRAME_DEFAULTS {        \
    {0},             \
    0,             \
    EM_CAN_RAW_RAME_CIRCLEQ_SIZE,             \
    -1,             \
    -1,             \
    0,             \
}

union CliMsg recvFrameId;
union CliMsg transFrameId;
em_circleq_raw_frame_t raw_frame_cq = EM_CIRCLEQ_RAW_FRAME_DEFAULTS;
em_circleq_recv_frame_t recv_frame_cq = EM_CIRCLEQ_RECV_FRAME_DEFAULTS;
//em_circleq_simple_can_t simple_can_cmd_cq = EM_CIRCLEQ_SIMPLE_CAN_DEFAULTS;
//Uint64 simple_can_data_in = 0;
//Uint64 simple_can_data_out = 0;
Uint16 recvFrameRTR = 0;
Uint16 recvFrameSize = 0;
Uint16 recvTotalSize = 0;
Uint16 ecana_pkg_idx = 0;

Uint16 streaming = 0;

Uint16 loading_tx_lock = false;

//Uint16 dbg_frame_since = 0;
//Uint16 dbg_single_frame_since = 0;
//Uint16 dbg_multi_frame_since = 0;
//Uint16 dbg_multi_frame_since_2 = 0;
//Uint16 dbg_multi_frame_since_3 = 0;
//Uint16 dbg_single_byte_since = 0;
//Uint16 dbg_multi_byte_since = 0;
//Uint16 dbg_multi_byte_since_2 = 0;
//Uint16 dbg_multi_byte_since_3 = 0;


#define _MULTIFRAME_MAP_SIZE 8

typedef struct {
    cli_frame_data_t frame;
    Uint16 crc                 :  16;
    Uint16 start_pos_in_queue  :  10;
    Uint16 ordered_recieved    :  10;
    Uint16 ack_nak_sync_rcved  :   8;
    Uint16 empty               :   1;
    Uint16 crc_rcved           :   1;
} _multiframe_map_entry_t;

_multiframe_map_entry_t _multiframe_map[_MULTIFRAME_MAP_SIZE];

#define _EXPECTED_FRAME_MAP_SIZE 4

typedef struct {
    cli_frame_data_t frame;
    Uint16 empty               :   1;
} _expected_frame_map_entry_t;

_expected_frame_map_entry_t _expected_frame_map[_EXPECTED_FRAME_MAP_SIZE];

void ecana_tst_init(void);
void ecana_wake_up(void);

// FIXME:SIZE
// TODO change all CAN stuff to use CH2GET/PUT
char ecana_pkg_buf[2000] = {0};

void (*ecana_tx_call[32])(Uint16 *len, Uint32 *msgh, Uint32 *msgl) = {0};
void (*ecana_rx_call[32])(Uint16 len, Uint32 msgh, Uint32 msgl) = {0};

ECANA_STR ecana_str;

void ecana_swap32(Uint32*);
static void _multiframe_map_init();
static void _reset_multiframe_map_entry(_multiframe_map_entry_t *entry);
static Uint16 _multiframe_map_get_start_pos_for_frame(cli_frame_data_t frame);
static Uint16 _multiframe_map_register_frame_in_rx_queue(cli_frame_data_t frame);
static _multiframe_map_entry_t * _get_multiframe_from_map_by_id(Uint16 frame_id);
static _multiframe_map_entry_t * _get_multiframe_from_map(cli_frame_data_t frame);

static void _expected_frame_map_init();
static Uint16 _expected_frame_map_exist(cli_frame_data_t frame);

static inline Uint16 _ecana_raw_frame_enqueue(raw_frame_data_t * frame);
static inline Uint16 _ecana_raw_frame_dequeue(raw_frame_data_t * frame);
static inline Uint16 _ecana_raw_frame_queue_is_empty();

volatile Uint32 _ecana_tx_ack = 0;

void ecana_swap32(Uint32 *sw32) {
    Uint32 tmp;

    tmp = 0;
    tmp = ((*sw32 & 0x000000FF)<<24|
            (*sw32 & 0x0000FF00)<<8 |
            (*sw32 & 0x00FF0000)>>8 |
            (*sw32 & 0xFF000000)>>24);
    *sw32 = tmp;
}

void ecana_isr_100_service() {
//    Uint32 error_flags_0 = ECanaRegs.CANGIF0.all & 0x0003FF1F;
//    if(error_flags_0) {
//        if(ECanaRegs.CANGIF0.bit.WLIF0) { // warning level
//            ECanaRegs.CANGIF0.bit.WLIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.EPIF0) { // error passive
//            ECanaRegs.CANGIF0.bit.EPIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.BOIF0) { // bus off
//            ECanaRegs.CANGIF0.bit.BOIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.WUIF0) { // wake up
//            ECanaRegs.CANGIF0.bit.WUIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.WDIF0) { // write denied interrupt
//            ECanaRegs.CANGIF0.bit.WDIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.TCOF0) { // time-stamp counter overflow
//            ECanaRegs.CANGIF0.bit.TCOF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.RMLIF0) { // receive message lost
//            ECanaRegs.CANRML.all = 0xFFFFFFFF; // acknowledge all lost messages
//            ECanaRegs.CANGIF0.bit.RMLIF0 = 1;
//            //ECanaRegs.CANRMP.all = 0xFFFFFFFF; // acknowledge all lost messages
//        }
//        if(ECanaRegs.CANGIF0.bit.AAIF0) { //  abort-acknowledge
//            ECanaRegs.CANAA.all = 0xFFFFFFFF; // acknowledge all aborts
//            ECanaRegs.CANGIF0.bit.AAIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.GMIF0) { // global mailbox interrupt
//            ECanaRegs.CANGIF0.bit.GMIF0 = 1;
//        }
//        if(ECanaRegs.CANGIF0.bit.MTOF0) { // mailbox time-out
//            ECanaRegs.CANTOS.all = 0xFFFFFFFF; // clear all mailbox timeouts
//            ECanaRegs.CANGIF0.bit.MTOF0 = 1;
//        }
//    }
}

Uint16 wait_mbox_empty(Uint16 mbox, Uint32 timeout_ms)
{
    Uint32 start_time = Clock_getTicks();
//    switch(mbox)
//    {
//        case 0:
//            while (((!ECanaRegs.CANTA.bit.TA0) && ECanaRegs.CANTRS.bit.TRS0 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 1:
//            while (((!ECanaRegs.CANTA.bit.TA1) && ECanaRegs.CANTRS.bit.TRS1 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 2:
//            while (((!ECanaRegs.CANTA.bit.TA2) && ECanaRegs.CANTRS.bit.TRS2 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 3:
//            while (((!ECanaRegs.CANTA.bit.TA3) && ECanaRegs.CANTRS.bit.TRS3 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 4:
//            while (((!ECanaRegs.CANTA.bit.TA4) && ECanaRegs.CANTRS.bit.TRS4 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 5:
//            while (((!ECanaRegs.CANTA.bit.TA5) && ECanaRegs.CANTRS.bit.TRS5 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 6:
//            while (((!ECanaRegs.CANTA.bit.TA6) && ECanaRegs.CANTRS.bit.TRS6 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 7:
//            while (((!ECanaRegs.CANTA.bit.TA7) && ECanaRegs.CANTRS.bit.TRS7 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 8:
//            while (((!ECanaRegs.CANTA.bit.TA8) && ECanaRegs.CANTRS.bit.TRS8 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 9:
//            while (((!ECanaRegs.CANTA.bit.TA9) && ECanaRegs.CANTRS.bit.TRS9 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 10:
//            while (((!ECanaRegs.CANTA.bit.TA10) && ECanaRegs.CANTRS.bit.TRS10 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 11:
//            while (((!ECanaRegs.CANTA.bit.TA11) && ECanaRegs.CANTRS.bit.TRS11 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 12:
//            while (((!ECanaRegs.CANTA.bit.TA12) && ECanaRegs.CANTRS.bit.TRS12 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 13:
//            while (((!ECanaRegs.CANTA.bit.TA13) && ECanaRegs.CANTRS.bit.TRS13 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 14:
//            while (((!ECanaRegs.CANTA.bit.TA14) && ECanaRegs.CANTRS.bit.TRS14 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 15:
//            while (((!ECanaRegs.CANTA.bit.TA15) && ECanaRegs.CANTRS.bit.TRS15 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 16:
//            while (((!ECanaRegs.CANTA.bit.TA16) && ECanaRegs.CANTRS.bit.TRS16 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 17:
//            while (((!ECanaRegs.CANTA.bit.TA17) && ECanaRegs.CANTRS.bit.TRS17 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 18:
//            while (((!ECanaRegs.CANTA.bit.TA18) && ECanaRegs.CANTRS.bit.TRS18 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 19:
//            while (((!ECanaRegs.CANTA.bit.TA19) && ECanaRegs.CANTRS.bit.TRS19 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 20:
//            while (((!ECanaRegs.CANTA.bit.TA20) && ECanaRegs.CANTRS.bit.TRS20 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 21:
//            while (((!ECanaRegs.CANTA.bit.TA21) && ECanaRegs.CANTRS.bit.TRS21 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 22:
//            while (((!ECanaRegs.CANTA.bit.TA22) && ECanaRegs.CANTRS.bit.TRS22 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 23:
//            while (((!ECanaRegs.CANTA.bit.TA23) && ECanaRegs.CANTRS.bit.TRS23 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 24:
//            while (((!ECanaRegs.CANTA.bit.TA24) && ECanaRegs.CANTRS.bit.TRS24 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 25:
//            while (((!ECanaRegs.CANTA.bit.TA25) && ECanaRegs.CANTRS.bit.TRS25 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 26:
//            while (((!ECanaRegs.CANTA.bit.TA26) && ECanaRegs.CANTRS.bit.TRS26 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 27:
//            while (((!ECanaRegs.CANTA.bit.TA27) && ECanaRegs.CANTRS.bit.TRS27 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 28:
//            while (((!ECanaRegs.CANTA.bit.TA28) && ECanaRegs.CANTRS.bit.TRS28 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 29:
//            while (((!ECanaRegs.CANTA.bit.TA29) && ECanaRegs.CANTRS.bit.TRS29 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 30:
//            while (((!ECanaRegs.CANTA.bit.TA30) && ECanaRegs.CANTRS.bit.TRS30 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        case 31:
//            while (((!ECanaRegs.CANTA.bit.TA31) && ECanaRegs.CANTRS.bit.TRS31 && (ECanaRegs.CANES.bit.EP || ECanaRegs.CANES.bit.TM || ECanaRegs.CANES.bit.ACKE)) && ((Clock_getTicks() - start_time) < timeout_ms)) {};
//            break;
//        default:
//            break;
//    }
    if (Clock_getTicks() - start_time > timeout_ms)
    {
        return ECANA_TIMEOUT;
    }
    return ECANA_SUCCESS;
}
Uint16 wait_ack_done(Uint16 frame_id, Uint32 timeout_ms, Uint16 * result)
{
    char send_buf[1] = {0};
    Uint32 start_time = Clock_getTicks();
    Uint32 msgh, msgl;
    Uint16 not_done = 1;

    _multiframe_map_entry_t * frame_entry = _get_multiframe_from_map_by_id(frame_id);
    if (!frame_entry)
    {
        if (result)
            *result = 0;
        return 1; // Fail, no entry in map
    }

    while(((Clock_getTicks() - start_time) < timeout_ms) && not_done)
    {
        not_done = (frame_entry->ack_nak_sync_rcved == 0) ? true:false;
//        ServiceDog();
        //for (ii = 0; ii < size; ii++)
        //{
            //not_done |= (~cli_ordered_collected_buf[ii])&1;
        //}
    }
    if (not_done)
    {
        send_buf[0] = NACK_MESSAGE;
        _cli_buff_to_can_msg(&send_buf[0], 1, &msgh, &msgl);
        ecana_send(DEFAULT_TX_MBOX /*mailbox num*/, 0x03 /*msg id*/, 1, msgh, msgl);
        frame_entry->ack_nak_sync_rcved = 0;
        return 1;// timeout
    }
    if (result)
        *result = frame_entry->ack_nak_sync_rcved;

    frame_entry->ack_nak_sync_rcved = 0;
    return 0; // success
}

Uint16 err1 = 19;
Uint16 err2 = 21;

Uint16 wait_response(Uint16 tool_type, Uint16 frame_id, Uint32 timeout_ms)
{
    cli_frame_data_t val;
    Uint32 start = Clock_getTicks();
    while (true)
    {
        if (!ecana_rcv_frame_queue_is_empty())
        {
            val = recv_frame_cq.queue[(recv_frame_cq.tail)];
            if ((val.tool_type == tool_type)&&(val.msg_id == frame_id))
            {
            	ecana_rcv_frame_dequeue(&recv_frame_cq.val);
                return true;
            }
            else
            {
            	err1 = val.msg_id;
            	err2 = frame_id;
            }
        }
        if (Clock_getTicks() - start > timeout_ms)
            return false;

        mp_can_in_use = false;
        mp_can_waiting_cli_caution = true;
        Task_sleep(50);
        // Wait until cli finishe processing current cmd
        wait_cli_clear();
        mp_can_waiting_cli_caution = false;
        mp_can_in_use = true;
    }
    return false;
}

Uint16 wait_cli_clear()
{
    while (cli_processing_cmd_flag)
        Task_sleep(50);
    return true;
}

Uint16 wait_mframe_done(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following)
{
    char send_buf[1] = {0};
    Uint32 start_time = Clock_getTicks();
    Uint32 msgh, msgl;
    Uint16 not_done = 1;
    Uint16 ii, calc_crc, temp;
    // CRC

    _multiframe_map_entry_t * frame_entry = _get_multiframe_from_map_by_id(frame_id);
    if (!frame_entry)
    {
        return 1; // Fail, no entry in map
    }

    while(((Clock_getTicks() - start_time) < timeout_ms) && not_done)
    {
        not_done = ((frame_entry->ordered_recieved == size) && frame_entry->crc_rcved)?0:1;
//        ServiceDog();
    }
    if (not_done)
    {
        return 3;// timeout
    }
    calc_crc = 0;

    for (ii = 0; ii < (size / 2); ii++)
    {
        temp = (cli_rx_buf[(frame_entry->start_pos_in_queue + 2*ii) % CLI_RX_BUF_SIZE] & 0xFF)<<8;
        temp |= cli_rx_buf[(frame_entry->start_pos_in_queue + 2*ii + 1) % CLI_RX_BUF_SIZE] & 0xFF;
        calc_crc = crc_16_update(calc_crc, temp);
    }
    if (frame_entry->crc != calc_crc)
    {
        send_buf[0] = NACK_MESSAGE;
        _cli_buff_to_can_msg(&send_buf[0], 1, &msgh, &msgl);
        ecana_send(DEFAULT_TX_MBOX /*mailbox num*/, 0x03 /*msg id*/, 1, msgh, msgl);
        return 2; // crc failure
    }
    // crc success

    if (!streaming)
    {
        transFrameId.bit.rtr = 0;
        send_buf[0] = (data_following)?SYNC_MESSAGE:ACK_MESSAGE;
        _cli_buff_to_can_msg(&send_buf[0], 1, &msgh, &msgl);
        msg_id_broadcast(recvFrameId.bit.priority, recvFrameId.bit.msg_id);
        ecana_send(DEFAULT_TX_MBOX /*mailbox num*/, 0x03 /*msg id*/, 1, msgh, msgl);
    }
    return 0;// success
}

Uint16 size1, size2;

Uint16 wait_mframe_done_no_response(Uint16 frame_id, Uint16 size, Uint32 timeout_ms, Uint16 data_following, Uint16 yes_tx_data)
{
    Uint32 start_time = Clock_getTicks();
    Uint16 not_done = 1;
    Uint16 ii, calc_crc, temp;
    char ack = 0;

    if (yes_tx_data)
    {
        ii = wait_sframe_done(timeout_ms);
        ack = _cli_deserialize_byte();
        if (ack == NACK_MESSAGE)
        {
            return 3;
        }
    }

    _multiframe_map_entry_t * frame_entry = _get_multiframe_from_map_by_id(frame_id);
    if (!frame_entry)
        return 1; // Fail, no entry in map

    while(((Clock_getTicks() - start_time) < timeout_ms) && not_done)
    {
        not_done = ((frame_entry->ordered_recieved == size) && frame_entry->crc_rcved)?0:1;
//        ServiceDog();
    }
    if (not_done)
    {
    	if((frame_entry->ordered_recieved != size)&&(!frame_entry->crc_rcved))
            return 1;// timeout
    	if(frame_entry->ordered_recieved != size)
    	{
            size1 = frame_entry->ordered_recieved;
            size2 = size;
    		return 4;// timeout
    	}
    	if(!frame_entry->crc_rcved)
            return 5;// timeout
    }
    calc_crc = 0;
    for (ii = 0; ii < (size / 2); ii++)
    {
        temp = (cli_rx_buf[(frame_entry->start_pos_in_queue + 2*ii) % CLI_RX_BUF_SIZE] & 0xFF)<<8;
        temp |= cli_rx_buf[(frame_entry->start_pos_in_queue + 2*ii + 1) % CLI_RX_BUF_SIZE] & 0xFF;
        calc_crc = crc_16_update(calc_crc, temp);
    }
    if (frame_entry->crc != calc_crc)
    {
        return 2; // crc failure
    }
    // crc success
    return 0;// success
}

Uint16 wait_sframe_done(Uint32 timeout_ms)
{
    Uint32 start_time = Clock_getTicks();
    Uint16 not_done = 1;
    while(((Clock_getTicks() - start_time) < timeout_ms) && not_done)
    {
        not_done = (cli_rx_buf_size >= 1)?0:1;
//        ServiceDog();
    }
    if (not_done)
    {
        return 1;// timeout
    }
    return 0;// success
}


void ecana_isr_101_service(void) {
    raw_frame_data_t raw_frame;

    Uint16 mailbox_num = 1; // TODO
                            // TODO
                            // TODO
                            // TODO
	Uint32 tool_type, cmd_priority;

    ecana_str.ecana_isr_1_cntr++;

    // check for RX callback fn
    if(ecana_rx_call[mailbox_num] != 0)
    {

		tool_type = (sCANMessage.ui32MsgID & (Uint32)0x3E00000) >> 21;
		cmd_priority = (sCANMessage.ui32MsgID >> 26) & 0x07;
//		if(tool_type == THIS_TOOL_TYPE || tool_type == TOOL_TYPE_BCAST) // TODO
		if(true)
		{
            //(*ecana_rx_call[mailbox_num])(mailbox->MSGCTRL.bit.DLC, mailbox->MDH.all, mailbox->MDL.all);
//			raw_frame.MSGID = mailbox->MSGID.all;
//			raw_frame.RTR = mailbox->MSGCTRL.bit.RTR;
//			raw_frame.MDH = mailbox->MDH.all;
//			raw_frame.MDL = mailbox->MDL.all;
//			raw_frame.DLC = mailbox->MSGCTRL.bit.DLC;
//			raw_frame.mailbox_num = mailbox_num;

		    swap32(sCANMessage.pui8MsgData);
		    swap32(sCANMessage.pui8MsgData + 4);

			raw_frame.MSGID = sCANMessage.ui32MsgID;
			raw_frame.RTR = ((sCANMessage.ui32Flags & MSG_OBJ_REMOTE_FRAME) > 0)? 1 : 0;
			raw_frame.MDL = *(Uint32*)sCANMessage.pui8MsgData;
			raw_frame.MDH = *((Uint32*)sCANMessage.pui8MsgData + 1);
			raw_frame.DLC = sCANMessage.ui32MsgLen;
			raw_frame.mailbox_num = mailbox_num; // TODO

			_ecana_raw_frame_enqueue(&raw_frame);
//			Swi_post(ecana_rx_swi);
		}
    }
    else if((!loading_tx_lock)&&(ecana_tx_call[mailbox_num] != 0))
    {
//        Uint16 msg_len;
//        Uint32 msgh, msgl, temp;
//        mailbox = &ECanaMboxes.MBOX0 + mailbox_num;
//        ECanaRegs.CANTA.all = uShftMbn; // ack the TX mailbox
//        if(delay_following_frames)
//            DELAY_US(1200);
//        (*ecana_tx_call[mailbox_num])(&msg_len, &msgh, &msgl); // get new values for the mailbox
//        mailbox->MSGCTRL.bit.DLC = msg_len;
//        mailbox->MDH.all = msgh;
//        mailbox->MDL.all = msgl;
//        temp = ECanaRegs.CANTRS.all;
//        if(msg_len > 0)
//        { // only send the message if we're sending more than 0 bytes
//            ECanaRegs.CANTRS.all = temp | uShftMbn; // request the new contents of the mailbox to be sent
//        }
    }
    else
    {
//        ECanaRegs.CANTA.all = uShftMbn; // ack the TX mailbox
//        _ecana_tx_ack |= uShftMbn; // set the TX ACK for the mailbox
//        ecana_str.ecana_isr1_ack_cntr++;
    }

}
void ecana_isr_tx_complete(){
    if((!loading_tx_lock)&&(ecana_tx_call[1] != 0))
    {
        Uint16 msg_len;
        Uint32 msgh, msgl;

        (*ecana_tx_call[1])(&msg_len, &msgh, &msgl); // get new values for the mailbox

        if(msg_len > 0)
        { // only send the message if we're sending more than 0 bytes
            ecana_send(1, 0, msg_len, msgh, msgl);
        }
    }
}

void ecana_rx_swi_func()
{
    raw_frame_data_t raw_frame;
    cli_frame_data_t recv_frame;
    Uint16 first_byte;

    while (!_ecana_raw_frame_queue_is_empty())
    {
        _ecana_raw_frame_dequeue(&raw_frame);

        recvFrameId.all = raw_frame.MSGID;
        recvFrameRTR = raw_frame.RTR;
        // data for cli
        recv_frame.msg_id = recvFrameId.bit.msg_id;
        recv_frame.tool_type = recvFrameId.bit.tool_type;
        recv_frame.priority = recvFrameId.bit.priority;
        recv_frame.rtr = raw_frame.RTR;

        _multiframe_map_entry_t * frame_entry;
        Uint16 to_do_callback = false;
        Uint16 to_queue_frame = false;
        recv_frame.length = raw_frame.DLC;
//        if (raw_frame.mailbox_num == SIMPLE_CAN_RX_MBOX)
//        {
//            to_do_callback = true;
//            to_queue_frame = false;
//        }
        if (_expected_frame_map_exist(recv_frame))
            to_do_callback = to_queue_frame = true;
        else if(cli_could_handle_frame(&recv_frame))
            to_do_callback = to_queue_frame = true;
        else
        {
            to_do_callback = to_queue_frame = false;
        }
        if (recv_frame.rtr == 0)
        {
            switch(recv_frame.priority)
            {
                // TODO fix stream and mframe
                case 0:  // RT_SINGLE
                case 3:  // QUERY_SINGLE
                case 6:  // BCAST_SINGLE
                    if (to_do_callback)
                        (*ecana_rx_call[raw_frame.mailbox_num])(recv_frame.length, raw_frame.MDH, raw_frame.MDL);
                    if (to_queue_frame)
                    {
                        recv_frame.mframe_length = recv_frame.length;
                        ecana_rcv_frame_enqueue(&recv_frame);
//                        dbg_single_frame_since ++;
                    }
                    break;

                case 1:  // RT_MULTI
                case 4:  // QUERY_MULTI
                case 7:  // BCAST_MULTI
                case 2:  // STREAM
                    if(!(to_do_callback || to_queue_frame))
                        break;
//                    dbg_multi_frame_since ++;
					frame_entry = _get_multiframe_from_map(recv_frame);
					first_byte = (0xFF000000 & raw_frame.MDL) >> 24;
					if((first_byte <= 6) && (frame_entry != NULL) && (recv_frame.priority != 2))
					{
						_reset_multiframe_map_entry(frame_entry);
						frame_entry = NULL;
					}

                    if ( (recv_frame.length == 1)
                      || ( ( recv_frame.length == 5) &&
                           ( ((0xFF000000 & raw_frame.MDL)>>24) == 3) &&
                           ( ((0x00FF0000 & raw_frame.MDL)>>16) == 3) &&
                           (  (0x000000FF & raw_frame.MDL)      == 0) &&
                           ( ((0xFF000000 & raw_frame.MDH)>>24) == 0) ) )
                    {
                        //This is ACK/NAK/SYNC
                        Uint16 ack_word = 0;
                        if(recv_frame.length == 1)
                            ack_word = (0xFF000000 & raw_frame.MDL)>>24;
                        else
                            ack_word = (0x0000FF00 & raw_frame.MDL)>>8;
                        if(frame_entry)
                            frame_entry->ack_nak_sync_rcved = ack_word;
                    }
                    else
                    {
//                        dbg_multi_frame_since_2 ++;
                        if (!frame_entry)
                        {
                            recv_frame.mframe_length = ((0x00FF0000 & raw_frame.MDL)>>16) - 2;
                            ecana_rcv_frame_enqueue(&recv_frame);
                            _multiframe_map_register_frame_in_rx_queue(recv_frame);
                            cli_rx_idx = (cli_rx_idx + recv_frame.mframe_length) % CLI_RX_BUF_SIZE;
                            cli_rx_buf_size += recv_frame.mframe_length;
//                            for(ii = 0; ii < recv_frame.mframe_length; ii++)
//                            	_cli_enqueue_rx(0x0077);

                        }
                        _ecana_fill_ordered_buf_mframe(recv_frame, raw_frame.MDH, raw_frame.MDL);
                    }
                    break;
                case 5:  // This shouldn't happen
                    break;
                default: // This shouldn't happen
                    break;
            }
        }
        else
        {
            switch(recv_frame.priority)
            {
                case 0:  // RT_SINGLE
                case 3:  // QUERY_SINGLE
                case 6:  // BCAST_SINGLE
                    if (to_queue_frame)
                    {
                        recv_frame.length = 1;
                        recv_frame.mframe_length = 0;
                        ecana_rcv_frame_enqueue(&recv_frame);
                    }
                    break;
                case 1:  // RT_MULTI
                case 4:  // QUERY_MULTI
                case 7:  // BCAST_MULTI
                    if(!(to_do_callback || to_queue_frame))
                        break;
                    ecana_rcv_frame_enqueue(&recv_frame);
                    break;
                case 2:  // STREAM
                    if(!(to_do_callback || to_queue_frame))
                        break;
                    frame_entry = _get_multiframe_from_map(recv_frame);

                    if (!frame_entry)
                    {
                        recv_frame.mframe_length = 0;
                        ecana_rcv_frame_enqueue(&recv_frame);
                        _multiframe_map_register_frame_in_rx_queue(recv_frame);
                    }
                    break;
                case 5:  // This shouldn't happen
                    break;
                default: // This shouldn't happen
                    break;
            }
        }
        ecana_str.ecana_isr_1_rx_cntr++;
    }
}

void _cana_rx_interrupt(Uint16 len, Uint32 msgh, Uint32 msgl) {

    ecana_str.Length = len;
    ecana_str.MsgH = msgh;
    ecana_str.MsgL = msgl;
    ecana_str.ecana_rx_interrupt_cntr++;
    return;
}

void ecana_init_rx_callback(Uint16 mailbox_num,
                            void (*rx_callback)(Uint16 len,
                                                Uint32 msgh,
                                                Uint32 msgl)){
    ecana_rx_call[mailbox_num] = rx_callback;
}

void ecana_init_tx_callback(Uint16 mailbox_num,
                            void (*tx_callback)(Uint16 *len,
                                                Uint32 *msgh,
                                                Uint32 *msgl)) {
    ecana_tx_call[mailbox_num] = tx_callback;
}

void msg_id_broadcast(Uint16 priority, Uint16 msg_id)
{
    transFrameId.bit.priority = priority;
    transFrameId.bit.tool_type = THIS_TOOL_TYPE;
    transFrameId.bit.msg_id = msg_id;
    transFrameId.bit.reserved = 0;
//    transFrameId.bit.serial = config.tool_info.uid_l & 0x00FF;
    transFrameId.bit.serial = 0x0011 & 0x00FF;
}

void ecana_set_transframe_id(Uint16 priority, Uint16 msg_id, Uint16 tool_type, Uint16 resvd, Uint16 serial, Uint16 rtr)
{
    transFrameId.bit.priority = priority;
    transFrameId.bit.tool_type = tool_type;
    transFrameId.bit.msg_id = msg_id;
    transFrameId.bit.reserved = resvd;
    //transFrameId.bit.serial = config.tool_info.uid_l & 0x00FF;
    transFrameId.bit.serial = serial;
    transFrameId.bit.rtr = rtr;
}

void ecana_send_no_data(Uint16 mbox)
{
    Uint32 msgh = 0, msgl = 0;
    ecana_send(mbox, 0x03, 0, msgh, msgl);
    wait_mbox_empty(mbox, DEFAULT_TX_TIMEOUT);
}

void ecana_send_wo_delay(Uint16 mailbox_num, Uint16 msg_id, Uint16 msg_len, Uint32 msgh, Uint32 msgl) {
//    Uint32 temp;
//    volatile struct MBOX *mailbox;
//    mailbox = &ECanaMboxes.MBOX0 + mailbox_num;
//    Uint32 uShftMbn = ((Uint32)1<<mailbox_num);
//    //Uint32 tool_type = THIS_TOOL_TYPE;
//    union CliMsg can_msg;
//	Uint16 error = 0;
//    error = wait_mbox_empty(mailbox_num, MIN_TX_TIMEOUT);
//    if (error)
//        return;
//
//    while(ECanaRegs.CANES.bit.TM)
//    {
//        if(ECanaRegs.CANES.bit.ACKE || ECanaRegs.CANES.bit.EP)
//            return;
//    }
//
//    _ecana_tx_ack &= ~uShftMbn; // reset the TX ACK flag
//    temp = ECanaRegs.CANME.all;
//    temp &= (~uShftMbn);
//
//    ECanaRegs.CANME.all=temp;
//    // Need to clear AAM and AME
//    Uint32 setting = (mailbox->MSGID.bit.IDE << 2);
//    can_msg.all = transFrameId.all;
//    can_msg.bit.settings = setting;
//
//    if (mailbox->MSGID.bit.IDE)
//    {
//        mailbox->MSGID.all = (Uint32)can_msg.all;
//        //mailbox->MSGID.bit.STDMSGID = (msg_id & 0x1FFC0000) >> 18;
//        //mailbox->MSGID.bit.EXTMSGID_L = (msg_id & 0xFFFF);
//        //mailbox->MSGID.bit.EXTMSGID_H = (msg_id & 0x30000) >> 16;
//    }
//    else
//    {
//        mailbox->MSGID.bit.STDMSGID = msg_id & 0x7FF;
//        mailbox->MSGID.bit.EXTMSGID_L = 0;
//        mailbox->MSGID.bit.EXTMSGID_H = 0;
//		mailbox->MSGCTRL.bit.TPL = 15-mailbox_num;
//    }
//
//    mailbox->MSGCTRL.bit.DLC = msg_len;
//    mailbox->MSGCTRL.bit.RTR = transFrameId.bit.rtr;
//    mailbox->MDH.all = msgh;
//    mailbox->MDL.all = msgl;
//
//    temp = ECanaRegs.CANME.all & uShftMbn;
//
//    if(temp == 0) { // check if the mailbox has been initialized, if not, initalize it
//        temp = ECanaRegs.CANMD.all;
//        ECanaRegs.CANMD.all = temp & ~(uShftMbn); // set direction to tx
//        temp = ECanaRegs.CANME.all;
//        ECanaRegs.CANME.all = temp | uShftMbn; // enable mailbox
//
//        EALLOW;
//        temp = ECanaRegs.CANMIM.all;
//        ECanaRegs.CANMIM.all = temp | uShftMbn; // enable the mailbox interrupt
//        temp = ECanaRegs.CANMIL.all;
//        ECanaRegs.CANMIL.all = temp | uShftMbn; // set the rx interrupts to level 1 (INT1)
//        temp = ECanaRegs.CANTOC.all;
//        ECanaRegs.CANTOC.all = temp & ~(uShftMbn);
//        ECanaRegs.CANTSC = 0;
//        EDIS;
//    }
//
//    temp = ECanaRegs.CANTRS.all;
//	ECanaRegs.CANTRS.all = temp | uShftMbn;
//
}

void ecana_send(Uint16 mailbox_num, Uint16 msg_id, Uint16 msg_len, Uint32 msgh, Uint32 msgl) {
    //TODO
//    Uint64
//    char serial = config.tool_info.uid_l & 0x00FF;
    swap32((char*)&msgh);
    swap32((char*)&msgl);
    EM_CANSend(transFrameId.all, msg_len, (((UInt64)msgh) << 32) + msgl);

}

void ecana_pwr_down(void) {
//    struct ECAN_REGS ECanaShadow;
//    EALLOW;
//
//    ECanaShadow.CANGIF0.all = ECanaRegs.CANGIF0.all;
//
//    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
//    ECanaShadow.CANMC.bit.PDR = 1;
//    ECanaShadow.CANMC.bit.WUBA = 1;  //Wake up on bas activity  EALLOW protected
//    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
//    if(!ECanaShadow.CANGIF0.bit.WDIF0) { //WDIF0 Write-denied interrupt flag.
//        ECanaShadow.CANMC.bit.PDR = 1;
//        ECanaShadow.CANMC.bit.WUBA = 1;  //Wake up on bas activity  EALLOW protected
//        ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
//
//        // Wait until the CPU no longer has permission to change the configuration registers
//        do {
//            ECanaShadow.CANES.all = ECanaRegs.CANES.all;
//        }  while(ECanaShadow.CANES.bit.CCE != 0 );
//    }
//    else {
//        ECanaRegs.CANGIF0.all = 0xFFFFFFFF;
//    }
//
//    EDIS;
}

void ecana_wake_up(void) {
//    struct ECAN_REGS ECanaShadow;
//
//    EALLOW;
//    if(ECanaRegs.CANES.bit.PDA) {
//        ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
//        ECanaShadow.CANMC.bit.PDR = 0;
//        //ECanaShadow.CANMC.bit.WUBA = 1;  //Wake up on bas activity  EALLOW protected
//        ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
//    }
//    EDIS;
}

void ecana_init(Uint32 bus_freq) {
    Uint16 i;
    recvFrameId.all = 0;
    transFrameId.all = 0;
    recvFrameRTR = 0;
    recvFrameSize = 0;
    loading_tx_lock = false;

    ecana_str.ecana_isr_0_cntr=0;
    ecana_str.ecana_isr_1_cntr=0;
    ecana_str.ecana_rx_interrupt_cntr=0;
    ecana_str.ecana_isr_1_rx_cntr=0;
    ecana_str.ecana_isr_1_tx_cntr=0;
    ecana_str.ecana_isr1_ack_cntr=0;
    ecana_str.Length = 0;
    ecana_str.MsgH = 0;
    ecana_str.MsgL = 0;

    _ecana_tx_ack = 0;
    for(i = 0; i < 32; i++) {
        ecana_tx_call[i] = 0;
        ecana_rx_call[i] = 0;
    }

    _multiframe_map_init();
    _expected_frame_map_init();
    raw_frame_cq.head = raw_frame_cq.tail = 0;
    memset(raw_frame_cq.queue, 0, sizeof(raw_frame_cq.queue));
}

Uint16 ecan_rml()
{
//    Uint16 temp = ECanaRegs.CANRML.all;
//    return (temp)?1:0;
    return 0;
}

Uint32 ecan_error()
{
//    Uint32 error_status = ECanaRegs.CANES.all & 0x01FF0000;
//    if(error_status > 0) {
//        Uint32 error_mailbox = ECanaRegs.CANGIF0.all & 0x0000001F;
//        if(ECanaRegs.CANES.bit.EW) { // Warning status (CANREC or CANTEC has reached warning level of 96)
//            ECanaRegs.CANES.bit.EW = 1;
//        }
//        if(ECanaRegs.CANES.bit.EP) { // Error passive state (CANREC or CANTEC has reached 128)
//            ECanaRegs.CANES.bit.EP = 1;
//        }
//        if(ECanaRegs.CANES.bit.BO) { // Bus off status
//            ECanaRegs.CANES.bit.BO = 1;
//        }
//        if(ECanaRegs.CANES.bit.ACKE) { // ACK error
//            ECanaRegs.CANES.bit.ACKE = 1;
//        }
//        if(ECanaRegs.CANES.bit.SE) { // Bit stuff error
//            ECanaRegs.CANES.bit.SE = 1;
//        }
//        if(ECanaRegs.CANES.bit.CRCE) { // CRC error
//            ECanaRegs.CANES.bit.CRCE = 1;
//        }
//        if(ECanaRegs.CANES.bit.SA1) { // Stuck at domain error (SA1 bit is always at 1 after a hardware reset, a software reset, or a Bus-Off condition. This bit is cleared when a recessive bit is detected on the bus)
//            ECanaRegs.CANES.bit.SA1 = 1;
//        }
//        if(ECanaRegs.CANES.bit.BE) { // Bit error flag
//            ECanaRegs.CANES.bit.BE = 1;
//        }
//        if(ECanaRegs.CANES.bit.FE) { // Form error flag
//            ECanaRegs.CANES.bit.FE = 1;
//        }
//    }
//
//    return error_status;
    return 0;
}

//Uint16 dbg_f_id = 0;
//cli_frame_data_t dbg_frame_list[8];

void _ecana_fill_ordered_buf_mframe(cli_frame_data_t frame, Uint32 msgh, Uint32 msgl) {
    Uint16 ii, byte, top_index, end_index, temp_index, offset;

    _multiframe_map_entry_t * frame_entry = _get_multiframe_from_map(frame);
    if (!frame_entry)
        return; // I don't want to register it here, if it is not in the map.

    offset = frame.length - 2;
    for(ii = 0; ii < frame.length; ii++) {
        temp_index = ii - 2;
        switch(ii) {
            case 0:
                byte = (0xFF000000 & msgl)>>24;
                break;
            case 1:
            	byte = (0x00FF0000 & msgl)>>16;
                break;
            case 2:
                byte = (0x0000FF00 & msgl)>>8;
                break;
            case 3:
                byte = (0x000000FF & msgl);
                break;
            case 4:
                byte = (0xFF000000 & msgh)>>24;
                break;
            case 5:
                byte = (0x00FF0000 & msgh)>>16;
                break;
            case 6:
                byte = (0x0000FF00 & msgh)>>8;
                break;
            case 7:
                byte = (0x000000FF & msgh);
                break;
            default:
                byte = 0; // should never get here
                break;
        }

        if(ii == 0)
        {
        	top_index = byte;
        }
        else if(ii == 1)
        {
        	end_index = byte;
        }
        else
        {
        	if (top_index - offset + temp_index == end_index - 1)
        	{
        		frame_entry->crc = (frame_entry->crc & 0xFF00) | byte;
        		frame_entry->crc_rcved = true;
        	}
        	else if (top_index - offset + temp_index == end_index - 2)
        	{
        		frame_entry->crc = (frame_entry->crc & 0x00FF) | (byte << 8);
        	}
        	else
        	{
        		cli_rx_buf[(frame_entry->start_pos_in_queue + top_index - offset + temp_index) % CLI_RX_BUF_SIZE] = byte;
        		frame_entry->ordered_recieved++;
        	}
        }
    }
    // make sure the multi-frame transmission sizes make sense
    if(top_index == end_index && top_index != frame_entry->ordered_recieved + 2) // the last mframe contains the CRC and ordered_recieved is not incremented
    {
		_reset_multiframe_map_entry(frame_entry);
    }
//    else if(top_index != end_index && top_index != frame_entry->ordered_recieved) // for all other frames ordered_recieved is incremented
//    {
//    	_multiframe_map_init();
//    }
}

Uint16 ecana_pkg_char(char value)
{
    return ecana_pkg(value, 1);
}
/*
 * ecana_pkg_string
 * @param outsting: buffer to string you want to send
 * @param len: this is the max length you want to read out
 *             to prevent buffer overflow, I would recommend
 *             usinng your buffer size.
 * Notes: This will handle padding a Null char to the end
 *        of the string so don't worry about that.
 */
Uint16 ecana_pkg_string(char *outstring, Uint16 len)
{
    Uint16 ii;
    for(ii = 0; ii < len && outstring[ii] != 0; ii++) { // output everything up to a NULL or LEN
        ecana_pkg_char(outstring[ii]);
    }
    ecana_pkg_char(0); // NULL term
    return 0;
}
Uint16 ecana_pkg_string_from_word_buf(Uint16 *outstring, Uint16 start_pos, Uint16 len)
{
    Uint16 ii = 0;
    char c = '\0';
    for(ii = 0; ii < len; ii++) { // output everything up to a NULL or LEN
        CH_GET(outstring, start_pos + ii, c);
        if(c == '\0')
            break;
        ecana_pkg_char(c);
    }
    ecana_pkg_char(0); // NULL term
    return 0;
}
Uint16 ecana_pkg_uint8(Uint16 value)
{
    return ecana_pkg(value, 1);
}
Uint16 ecana_pkg_int16(int16 value)
{
    return ecana_pkg(value, 2);
}
Uint16 ecana_pkg_uint16(Uint16 value)
{
    return ecana_pkg(value, 2);
}
Uint16 ecana_pkg_int32(int32 value)
{
    return ecana_pkg(value, 4);
}
Uint16 ecana_pkg_uint32(Uint32 value)
{
    return ecana_pkg(value, 4);
}
Uint16 ecana_pkg_float32(float32 value)
{
    union FloatorUint32 temp;
    temp.fl = value;
    return ecana_pkg(temp.ui, 4);
}
Uint16 ecana_pkg_float64(float64 value)
{
    return ecana_pkg(value, 8);
}
Uint16 ecana_pkg_uint64(Uint64 value)
{
    return ecana_pkg(value, 8);
}
Uint16 ecana_pkg(Uint64 value, Uint16 length)
{
    Uint16 ii;
    for (ii = 0; ii < length; ii++)
    {
        ecana_pkg_buf[ecana_pkg_idx++] = (char) (((value >> (8*((length-ii)-1)))) & 0xFF);
    }
    return 0;
}

static void _multiframe_map_init()
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        _multiframe_map[ii].empty = true;
        _multiframe_map[ii].frame.msg_id = 0;
        _multiframe_map[ii].start_pos_in_queue = 0;
        _multiframe_map[ii].ordered_recieved = 0;
        _multiframe_map[ii].ack_nak_sync_rcved = false;
        _multiframe_map[ii].crc = 0;
        _multiframe_map[ii].crc_rcved = false;
    }
}

static void _reset_multiframe_map_entry(_multiframe_map_entry_t *entry)
{
	entry->empty = true;
	entry->frame.msg_id = 0;
	entry->start_pos_in_queue = 0;
	entry->ordered_recieved = 0;
	entry->ack_nak_sync_rcved = false;
	entry->crc = 0;
	entry->crc_rcved = false;
}

static Uint16 _multiframe_map_get_start_pos_for_frame(cli_frame_data_t frame)
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        if(!_multiframe_map[ii].empty)
            if(_multiframe_map[ii].frame.msg_id == frame.msg_id)
                return _multiframe_map[ii].start_pos_in_queue;
    }
    return 0xFFFF;
}

static Uint16 _multiframe_map_register_frame_in_rx_queue(cli_frame_data_t frame)
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        if(_multiframe_map[ii].empty)
        {
            _multiframe_map[ii].empty = false;
            _multiframe_map[ii].start_pos_in_queue = cli_rx_idx;
            _multiframe_map[ii].ordered_recieved = 0;
            _multiframe_map[ii].frame = frame;
            _multiframe_map[ii].ack_nak_sync_rcved = false;
            _multiframe_map[ii].crc = 0;
            _multiframe_map[ii].crc_rcved = false;
            return true;
        }
    }
    return false;
}

Uint16 multiframe_map_remove_frame(cli_frame_data_t * frame)
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        if(!_multiframe_map[ii].empty)
            if(_multiframe_map[ii].frame.msg_id == frame->msg_id)
            {
                _multiframe_map[ii].empty = true;
                _multiframe_map[ii].start_pos_in_queue = 0;
                _multiframe_map[ii].ordered_recieved = 0;
                _multiframe_map[ii].frame.msg_id = 0;
                _multiframe_map[ii].ack_nak_sync_rcved = false;
                _multiframe_map[ii].crc = 0;
                _multiframe_map[ii].crc_rcved = false;
                return true;
            }
    }
    return false;
}

Uint16 multiframe_map_remove_frame_by_id(Uint16 frame_id)
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        if(!_multiframe_map[ii].empty)
            if(_multiframe_map[ii].frame.msg_id == frame_id)
            {
                _multiframe_map[ii].empty = true;
                _multiframe_map[ii].start_pos_in_queue = 0;
                _multiframe_map[ii].ordered_recieved = 0;
                _multiframe_map[ii].frame.msg_id = 0;
                _multiframe_map[ii].ack_nak_sync_rcved = false;
                _multiframe_map[ii].crc = 0;
                _multiframe_map[ii].crc_rcved = false;
                return true;
            }
    }
    return false;
}

static _multiframe_map_entry_t * _get_multiframe_from_map_by_id(Uint16 frame_id)
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        if(!_multiframe_map[ii].empty)
            if(_multiframe_map[ii].frame.msg_id == frame_id)
            {
                return &(_multiframe_map[ii]);
            }
    }
    return NULL;
}

static _multiframe_map_entry_t * _get_multiframe_from_map(cli_frame_data_t frame)
{
    Uint16 ii;
    for(ii = 0; ii < _MULTIFRAME_MAP_SIZE; ii++)
    {
        if(!_multiframe_map[ii].empty)
            if(_multiframe_map[ii].frame.msg_id == frame.msg_id)
            {
                return &(_multiframe_map[ii]);
            }
    }
    return NULL;
}

static void _expected_frame_map_init()
{
    Uint16 ii;
    for(ii = 0; ii < _EXPECTED_FRAME_MAP_SIZE; ii++)
    {
        _expected_frame_map[ii].empty = true;
        _expected_frame_map[ii].frame.msg_id = 0x00FF;
    }
}

static Uint16 _expected_frame_map_exist(cli_frame_data_t frame)
{
    Uint16 ii;
    for(ii = 0; ii < _EXPECTED_FRAME_MAP_SIZE; ii++)
    {
        if(!_expected_frame_map[ii].empty)
            if(_expected_frame_map[ii].frame.msg_id == frame.msg_id)
                return true;
    }
    return false;
}

Uint16 expected_frame_map_add(cli_frame_data_t * frame)
{
    Uint16 ii;
    for(ii = 0; ii < _EXPECTED_FRAME_MAP_SIZE; ii++)
    {
        if(_expected_frame_map[ii].empty)
        {
            _expected_frame_map[ii].frame = *frame;
            _expected_frame_map[ii].empty = false;;
            return true;
        }
    }
    return false;
}

Uint16 expected_frame_map_remove(cli_frame_data_t * frame)
{
    Uint16 ii;
    for(ii = 0; ii < _EXPECTED_FRAME_MAP_SIZE; ii++)
    {
        if(!_expected_frame_map[ii].empty)
            if(_expected_frame_map[ii].frame.msg_id == frame->msg_id)
            {
                _expected_frame_map[ii].empty = true;
                return true;
            }
    }
    return false;
}

void ecana_clear_send_frame_buffer()
{
    transFrameId.all = 0;
}

static inline Uint16 _ecana_raw_frame_enqueue(raw_frame_data_t * frame)
{
    if (((raw_frame_cq.head + 1) % raw_frame_cq.my_size) == raw_frame_cq.tail)
        return false;
    raw_frame_cq.queue[raw_frame_cq.head] = * frame;
    raw_frame_cq.head = (raw_frame_cq.head + 1) % raw_frame_cq.my_size;
    if (raw_frame_cq.tail < 0)
        raw_frame_cq.tail = (raw_frame_cq.head + 1) % raw_frame_cq.my_size;
    raw_frame_cq.size = (raw_frame_cq.head + raw_frame_cq.my_size - raw_frame_cq.tail + 1) % raw_frame_cq.my_size;
    return true;
}

static inline Uint16 _ecana_raw_frame_dequeue(raw_frame_data_t * frame)
{
    if ((raw_frame_cq.tail < 0) || (raw_frame_cq.tail == raw_frame_cq.head))
    {
        raw_frame_cq.err = EM_CIRCLEQ_EMPTY;
        return false;
    }
    if (frame)
    {
        *frame = raw_frame_cq.queue[raw_frame_cq.tail];
        raw_frame_cq.tail = (raw_frame_cq.tail + 1) % raw_frame_cq.my_size;
        return true;
    }
    return false;
}

static inline Uint16 _ecana_raw_frame_queue_is_empty()
{
    return (raw_frame_cq.tail < 0) || (raw_frame_cq.head == raw_frame_cq.tail);
}

Uint16 ecana_rcv_frame_enqueue(cli_frame_data_t * frame)
{
    if (((recv_frame_cq.head + 1) % recv_frame_cq.my_size) == recv_frame_cq.tail)
        return false;
    recv_frame_cq.queue[recv_frame_cq.head] = * frame;
    recv_frame_cq.head = (recv_frame_cq.head + 1) % recv_frame_cq.my_size;
    if (recv_frame_cq.tail < 0)
    	recv_frame_cq.tail = (recv_frame_cq.head + 1) % recv_frame_cq.my_size;
    recv_frame_cq.size = (recv_frame_cq.head + recv_frame_cq.my_size - recv_frame_cq.tail + 1) % recv_frame_cq.my_size;
    return true;
}

Uint16 ecana_rcv_frame_dequeue(cli_frame_data_t * frame)
{
    if ((recv_frame_cq.tail < 0) || (recv_frame_cq.tail == recv_frame_cq.head))
    {
    	recv_frame_cq.err = EM_CIRCLEQ_EMPTY;
        return false;
    }
    if (frame)
    {
        *frame = recv_frame_cq.queue[recv_frame_cq.tail];
        recv_frame_cq.tail = (recv_frame_cq.tail + 1) % recv_frame_cq.my_size;
        return true;
    }
    return false;
}

Uint16 ecana_rcv_frame_queue_is_empty()
{
    return (recv_frame_cq.tail < 0) || (recv_frame_cq.head == recv_frame_cq.tail);
}

/******************************************
 *******************************************
 **   END ERDOS MILLER'S BACKGROUND IP   ***
 *******************************************
 ******************************************/

