################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/adc.c \
../Drivers/adc_internal.c \
../Drivers/bootload.c \
../Drivers/can.c \
../Drivers/cli.c \
../Drivers/crc.c \
../Drivers/ecana.c \
../Drivers/emlib.c \
../Drivers/fuse.c \
../Drivers/globals.c \
../Drivers/pinout.c \
../Drivers/pwm.c \
../Drivers/usb.c \
../Drivers/usb_bulk_structs.c \
../Drivers/util.c 

C_DEPS += \
./Drivers/adc.d \
./Drivers/adc_internal.d \
./Drivers/bootload.d \
./Drivers/can.d \
./Drivers/cli.d \
./Drivers/crc.d \
./Drivers/ecana.d \
./Drivers/emlib.d \
./Drivers/fuse.d \
./Drivers/globals.d \
./Drivers/pinout.d \
./Drivers/pwm.d \
./Drivers/usb.d \
./Drivers/usb_bulk_structs.d \
./Drivers/util.d 

OBJS += \
./Drivers/adc.obj \
./Drivers/adc_internal.obj \
./Drivers/bootload.obj \
./Drivers/can.obj \
./Drivers/cli.obj \
./Drivers/crc.obj \
./Drivers/ecana.obj \
./Drivers/emlib.obj \
./Drivers/fuse.obj \
./Drivers/globals.obj \
./Drivers/pinout.obj \
./Drivers/pwm.obj \
./Drivers/usb.obj \
./Drivers/usb_bulk_structs.obj \
./Drivers/util.obj 

OBJS__QUOTED += \
"Drivers\adc.obj" \
"Drivers\adc_internal.obj" \
"Drivers\bootload.obj" \
"Drivers\can.obj" \
"Drivers\cli.obj" \
"Drivers\crc.obj" \
"Drivers\ecana.obj" \
"Drivers\emlib.obj" \
"Drivers\fuse.obj" \
"Drivers\globals.obj" \
"Drivers\pinout.obj" \
"Drivers\pwm.obj" \
"Drivers\usb.obj" \
"Drivers\usb_bulk_structs.obj" \
"Drivers\util.obj" 

C_DEPS__QUOTED += \
"Drivers\adc.d" \
"Drivers\adc_internal.d" \
"Drivers\bootload.d" \
"Drivers\can.d" \
"Drivers\cli.d" \
"Drivers\crc.d" \
"Drivers\ecana.d" \
"Drivers\emlib.d" \
"Drivers\fuse.d" \
"Drivers\globals.d" \
"Drivers\pinout.d" \
"Drivers\pwm.d" \
"Drivers\usb.d" \
"Drivers\usb_bulk_structs.d" \
"Drivers\util.d" 

C_SRCS__QUOTED += \
"../Drivers/adc.c" \
"../Drivers/adc_internal.c" \
"../Drivers/bootload.c" \
"../Drivers/can.c" \
"../Drivers/cli.c" \
"../Drivers/crc.c" \
"../Drivers/ecana.c" \
"../Drivers/emlib.c" \
"../Drivers/fuse.c" \
"../Drivers/globals.c" \
"../Drivers/pinout.c" \
"../Drivers/pwm.c" \
"../Drivers/usb.c" \
"../Drivers/usb_bulk_structs.c" \
"../Drivers/util.c" 


